build:
	cargo build

ci:
	cargo watch -s "just dev"

clean:
	cargo clean

dev: lint build
	cargo test -- --nocapture
	export RUSTDOCFLAGS="DOCUMENT_PRIVATE_ITEMS=1"
	cargo doc

doc:
	cargo doc

lint:
	cargo check

loop +ACTION:
	cargo watch -s "just {{ACTION}}"

run FILE: dev
	cargo run --package cosmonaut_cli --bin cosmo -- -vv lib/assets/{{FILE}}.cosmos

test: lint build
	cargo test

trace:
	just lib/trace
	just cli/trace
