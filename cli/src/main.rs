#![feature(external_doc)]
#![doc(include = "../README.md")]
#![cfg_attr(feature = "trace", feature(custom_attribute, plugin))]
#![cfg_attr(feature = "trace", plugin(flamer))]

#[macro_use]
extern crate clap;
extern crate cosmonaut;
#[cfg(feature = "trace")]
extern crate flame;
#[macro_use]
extern crate log;
extern crate simplelog;
extern crate tap;

use clap::App;
use cosmonaut::Dictionary;
use simplelog::*;
use std::{
	fs::File,
	io::{
		self,
		Read,
	},
	process::exit,
};

fn main() {
	#[cfg(feature = "trace")]
	::flame::start("Init");

	let yaml = load_yaml!("cli.yml");
	let app = App::from_yaml(yaml)
		.author(crate_authors!("\n"))
		.version(crate_version!());
	let args = app.get_matches();

	TermLogger::init(match args.occurrences_of("verbosity") {
		0 => LevelFilter::Error,
		1 => LevelFilter::Warn,
		2 => LevelFilter::Info,
		3 => LevelFilter::Debug,
		_ => LevelFilter::Trace,
	}, Config::default()).unwrap_or_else(|_| {
		eprintln!("Failed to set up the logger!");
	});

	#[cfg(feature = "trace")]
	::flame::end("Init");

	let dict = if let Some(paths) = args.values_of_os("paths") {
		#[cfg(feature = "trace")]
		::flame::start("load");
		let mut files: Vec<File> = paths.map(File::open)
			.filter_map(|r| match r {
				Ok(f) => Some(f),
				Err(e) => {
					error!("{:?}", e);
					None
				},
			})
			.collect::<Vec<File>>();
		let mut read: Vec<&mut Read> = files.iter_mut()
			.map(|f| f as &mut Read)
			.collect();
		#[cfg(feature = "trace")]
		::flame::end("load");
		parse(&mut read).unwrap_or_else(|_| {
			error!("Failed to read a file into memory for parsing!");
			exit(1);
		})
	}
	else {
		info!("Reading COSMOS text from stdin!");
		let stdin = io::stdin();
		let mut stdin_l = stdin.lock();
		parse(&mut [&mut stdin_l as &mut Read]).unwrap_or_else(|_| {
			error!("Failed to read from stdin");
			exit(1);
		})
	};
	render(&dict);

	#[cfg(feature = "trace")]
	::flame::dump_html(&mut File::create("flames.html").unwrap()).unwrap();
}

#[cfg_attr(feature = "trace", flame)]
fn parse(sources: &mut [&mut Read]) -> io::Result<Dictionary> {
	let mut out = Dictionary::new();
	//  TODO: Parallelize
	for source in sources {
		let mut s = String::new();
		source.read_to_string(&mut s)?;
		out.parse(&s).map_err(|e| {
			error!("The parser encountered an error");
			io::Error::new(io::ErrorKind::InvalidData, e)
		})?;
	}
	Ok(out)
}

#[cfg_attr(feature = "trace", flame)]
fn render(dict: &Dictionary) {
	print!("{}", dict);
	print!("{}", dict.stats());
}
