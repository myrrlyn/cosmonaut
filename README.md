# Cosmonaut Project

[![Gitlab CI Status][build]][repo]

This project aims to provide a set of tools for working with the [COSMOS][0]
data definition language. It is primarily driven by my experiences and use cases
discovered at my job.

This project is a workspace of multiple crates, which are designed to work
together but *may* be used independently. Each crate has its own README and
documentation providing more information about what it does and how to use it.

I have not yet decided how to publish this on [`crates.io`][1], since workspaces
do not support packaging all their child crates into a single package. I intend
for this project to be distributed as a workspace unit first and foremost, with
the crates available individually as needed. It seems the most likely solution
is to create a façade crate, `cosmonaut`, that re-exports all the API surfaces
and applications of the other crates, but the project does not yet merit this.

## Licensing

Cosmonaut is in spirit, if not in letter, a derivative work of the COSMOS
project copyrighted by Ball Aerospace and licensed under the GPLv3. I have
elected to license this project under the LGPLv3, and copyright it under my name
and status as an employee of Space Dynamics Laboratory.

**THIS IS NOT AN OFFICIAL PROJECT OR PRODUCT OF SPACE DYNAMICS LABORATORY.** The
company’s involvement is currently limited solely to the fact that I am
developing this as a personal project in my capacity as an employee, over and
above my primary duties.

## Current Progress

- A parser that builds a dictionary of messages from input text.

    TODO: Make the parser able to pull from a stream rather than requiring an
    in-memory string.

- A CLI app that can use the parser API to parse, process, and render COSMOS
    text.

    TODO: Make it useful.

## Future Goals

- A linter that uses the parser to identify problems in the source text.

- A binary serializer/deserializer interface that can map binary messages to
    parsed definitions, and back.

- A code generator that ports the ser/des interface to other languages natively,
    or as FFI bindings to the Rust ser/des library.

- A network server that can read COSMOS configuration files and provide the
    back end for a COSMOS application.

- A graphical front end, either web or native, that can mimic the functionality
    of the reference COSMOS application framework.

## Rust Version

The workspace will be pinned via `rust-toolchain` to the oldest version of Rust
that can compile it. Currently, this is `nightly-2018-03-16`.

[0]: http://cosmosrb.com/
[1]: https://crates.io/
[build]: https://gitlab.com/myrrlyn/cosmonaut/badges/master/build.svg
[repo]: https://gitlab.com/myrrlyn/cosmonaut
