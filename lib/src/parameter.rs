#![doc(include = "../doc/cosmos/parameter.md")]

use super::{
	ParseResult,
	decorators::*,
	endian::Endian,
	kind::{
		Kind,
		KindVal,
		Variant,
	},
	parse_utils::*,
	traits::{
		Bodied,
		Described,
		Keyword,
		Named,
		Nameable,
		Parsed,
		Selectable,
		Stateful,
	},
};
use nom::types::CompleteStr;
use std::{
	fmt::{
		self,
		Display,
		Formatter,
	},
	ops::Index,
};

#[doc(include = "../doc/cosmos/parameter.md")]
#[derive(Clone, Debug, Default)]
pub struct Parameter {
	/// The `Parameter`'s name
	pub name: String,
	/// The `Parameter`'s description
	pub description: Option<String>,
	/// The `Parameter`'s variant (`Normal`, `Ident`, or `Array`).
	pub variant: Variant,
	/// The kind of the interior data.
	pub kind: Kind,
	/// The endianness of the field (defaults to owner's endianness).
	pub endian: Option<Endian>,
	/// The offset of the field within the owning `Command` packet.
	///
	/// If `None`, then the offset is calculated when the owner compiles its
	/// binary representation.
	pub offset: Option<i64>,
	/// The `Parameter`'s width in bits.
	pub width: u64,
	/// On array items, the width of each element in bits.
	/// (Ignored on non-array items)
	pub item_width: u64,
	/// The `Parameter`'s default (or identifying, for `Ident`) value.
	pub default: Option<KindVal>,
	/// The maximum allowable value.
	pub maximum: Option<KindVal>,
	/// The minimum allowable value.
	pub minimum: Option<KindVal>,
	/// Miscellaneous information about the `Parameter`.
	pub body: <Parameter as Bodied>::Body,
}

#[doc(include = "../doc/cosmos/element_body.md")]
#[derive(Clone, Debug, Default)]
pub struct Body {
	/// A set of named states for the `Parameter`.
	pub states: StateSet<Parameter>,
	/// A set of transformations to be applied to the data value before the
	/// packet is transmitted.
	pub poly_transforms: Vec<SegmentedPolyConversion<Parameter>>,
	/// An explicit description, found in `DESCRIPTION "text"`.
	pub description: Option<Description>,
	/// A set of meta-information describing the `Parameter` structure.
	pub meta: MetaSet,
	/// A unit of measurement contextualizing the data value.
	pub unit: Option<Unit>,
	/// A flag indicating that the parameter must be explicitly set by the
	/// client.
	pub required: Required,
	/// An explicit minimum value for the `Parameter`, overriding that set in
	/// the initial declaration.
	pub minimum: Option<Minimum>,
	/// An explicit maximum value for the `Parameter`, overriding that set in
	/// the initial declaration.
	pub maximum: Option<Maximum>,
	/// An explicit default value for the `Parameter`, overriding that set in
	/// the initial declaration.
	pub default: Option<Default>,
}

#[doc(include = "../doc/cosmos/patch.md")]
#[derive(Clone, Debug, Default)]
pub struct Patch {
	/// The name of the `Parameter` to edit.
	pub name: String,
	/// The patch body.
	pub body: Body,
}

impl Parameter {
	/// Accesses the set of named states.
	pub fn states(&self) -> &[State<Self>] {
		&self.body.states
	}
}

impl Bodied for Parameter {
	type Body = Body;

	#[cfg_attr(feature = "trace", flame)]
	fn apply(&mut self, body: <Self as Bodied>::Body) {
		let Body {
			mut states,
			poly_transforms,
			description,
			meta,
			unit,
			required,
			minimum,
			maximum,
			default,
		} = body;

		let kind = self.kind;
		states.iter_mut().for_each(|s| s.cast_in_place(kind));

		//  Conditionally overlay the body elements onto self
		self.body.states.extend(states);
		self.body.poly_transforms.extend(poly_transforms);
		self.body.meta.extend(meta);
		if let Some(Description { text }) = description {
			self.describe(text);
		}
		if unit.is_some() {
			self.body.unit = unit;
		}
		if *required {
			self.body.required = required;
		}
		if let Some(Minimum(val)) = minimum {
			self.minimum = Some(val.cast(kind));
		}
		if let Some(Maximum(val)) = maximum {
			self.maximum = Some(val.cast(kind));
		}
		if let Some(Default(val)) = default {
			self.default = Some(val.cast(kind));
		}
	}
}

impl Described for Parameter {
	#[cfg_attr(feature = "trace", flame)]
	fn description(&self) -> Option<&str> {
		self.description.as_ref().map(|s| s as &str)
	}

	#[cfg_attr(feature = "trace", flame)]
	fn describe(&mut self, description: String) {
		self.description = Some(description);
	}
}

impl Display for Parameter {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(64);
		write!(out, "{}{} {}",
			self.variant, <Self as Keyword>::KEYWORD, self.name
		)?;
		if let Some(ref o) = self.offset {
			write!(out, " {}", o)?;
		}
		write!(out, " {} {}", match self.variant {
			Variant::Array => self.item_width,
			_ => self.width,
		}, self.kind)?;
		if self.variant != Variant::Array {
			//  These *cannot* be None, Clippy.
			#[cfg_attr(feature = "cargo-clippy", allow(option_unwrap_used))]
			match self.kind {
				Kind::Int | Kind::Uint | Kind::Float | Kind::Derived => {
					write!(out, " {} {}",
						self.minimum.as_ref().unwrap(),
						self.maximum.as_ref().unwrap()
					)?
				},
				_ => {},
			}
		}
		if let Some(ref d) = self.default {
			write!(out, " {}", d)?;
		}
		if let Some(d) = self.description() {
			write!(out, " {:?}", d)?;
		}
		if let Some(e) = self.endian {
			write!(out, " {}", e)?;
		}
		writeln!(out)?;
		write!(out, "{}", self.body)?;
		write!(fmt, "{}", out)
	}
}

impl Index<usize> for Parameter {
	type Output = State<Self>;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: usize) -> &Self::Output {
		&self.body.states[idx]
	}
}

impl<'a> Index<&'a str> for Parameter {
	type Output = State<Self>;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: &'a str) -> &Self::Output {
		self.state(idx).unwrap_or_else(|| panic!(
			"Parameter `{}` has no state named `{}`", self.name(), idx
		))
	}
}

impl Keyword for Parameter {
	const KEYWORD: &'static str = "PARAMETER";
}

impl Named for Parameter {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name as &str
	}
}

impl Nameable for Parameter {
	#[cfg_attr(feature = "trace", flame)]
	fn set_name(&mut self, name: String) {
		self.name = name;
	}
}

impl Parsed for Parameter {
	/// Parses a `COMMAND PARAMETER` item from a stream of COSMOS text.
	#[cfg_attr(feature = "trace", flame)]
	//  Parameters are complex. Bite me, clippy.
	#[cfg_attr(feature = "cargo-clippy", allow(cyclomatic_complexity))]
	//  Repeated rebinding of text occurs because the parser is a sequential
	//  muncher. This is going to require manipulations that are hard to do in
	//  nom macros, otherwise I'd just do_parse! it.
	#[cfg_attr(feature = "cargo-clippy", allow(shadow_reuse))]
	#[cfg_attr(feature = "cargo-clippy", allow(shadow_same))]
	#[cfg_attr(feature = "cargo-clippy", allow(shadow_unrelated))]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		let mut out = Self::default();
		let (text, (append, variant)) = Variant::parse::<Self>(text)?;
		out.variant = variant;
		let (mut text, name) = word(text)?;
		out.name = name.to_string();
		if !append {
			let (rem, offset) = ws!(text, inum)?;
			out.offset = Some(offset);
			text = rem;
		}
		let (text, width) = ws!(text, unum)?;
		out.width = width;
		let (text, kind) = Kind::parse(text)?;
		out.kind = kind;
		//  TODO: On Kind::Derived, warn if offset is Some(nonzero).
		//  This is where the keywords begin to diverge.
		let (text, min, max, def) = match (out.variant, out.kind) {
			//  For arrays, move the width parsed above into item_width,
			//  because this format is garbage.
			(Variant::Array, _) => {
				out.item_width = width;
				let (rem, width) = ws!(text, unum)?;
				out.width = width;
				(rem, None, None, None)
			},
			//  Normal and Ident Parameters both have (min, max) on the numerics
			//  and default on all kinds.
			(_, Kind::Int) |
			(_, Kind::Uint) |
			(_, Kind::Float) |
			(_, Kind::Derived) => {
				ws!(text, tuple!(KindVal::parse, KindVal::parse, KindVal::parse))
					.map(|(rem, (min, max, def))| (rem,
						Some(min.cast(out.kind)),
						Some(max.cast(out.kind)),
						Some(def.cast(out.kind)),
					))?
			},
			(_, Kind::Text) => {
				dquote_escape(text).map(|(rem, def)| (
					rem, None, None, Some(KindVal::from(String::from(def)))
				))?
			},
			(_, Kind::Block) => unimplemented!("Block handler"),
		};
		out.minimum = min;
		out.maximum = max;
		out.default = def;

		//  Grab some optional trailing items
		let mut text = text;
		if let Ok((rem, desc)) = dquote_escape(text) {
			out.description = Some(desc.to_string());
			text = rem;
		}
		if let Ok((rem, end)) = Endian::parse(text) {
			out.endian = Some(end);
			text = rem;
		}
		//  `PARAMETER` lines do not require children elements.
		if let Ok((rem, body)) = <Self as Bodied>::Body::parse(text) {
			out.apply(body);
			text = rem;
		}

		Ok((text, out))
	}
}

impl Selectable for Parameter {
	type Patch = Patch;

	#[cfg_attr(feature = "trace", flame)]
	fn patch(&mut self, patch: Self::Patch) {
		if self.name() == patch.name() {
			self.apply(patch.body);
		}
	}
}

impl Stateful for Parameter {
	type Extra = Hazard;

	fn state(&self, name: &str) -> Option<&State<Self>> {
		self.body.states.get(name)
	}
}

impl Display for Body {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		let Body {
			ref states,
			ref poly_transforms,
			ref description,
			ref meta,
			ref unit,
			ref required,
			ref minimum,
			ref maximum,
			ref default,
		} = *self;

		if let Some(ref d) = *description {
			writeln!(out, "\t{}", d)?;
		}
		for state in states {
			writeln!(out, "\t{}", state)?;
		}
		for poly in poly_transforms {
			writeln!(out, "\t{}", poly)?;
		}
		if let Some(ref u) = *unit {
			writeln!(out, "\t{}", u)?;
		}
		if **required {
			writeln!(out, "\t{}", required)?;
		}
		if let Some(ref m) = *minimum {
			writeln!(out, "\t{}", m)?;
		}
		if let Some(ref m) = *maximum {
			writeln!(out, "\t{}", m)?;
		}
		if let Some(ref d) = *default {
			writeln!(out, "\t{}", d)?;
		}
		for m in meta {
			writeln!(out, "\t{}", m)?;
		}
		write!(fmt, "{}", out)
	}
}

impl Parsed for Body {
	/// Parse the children elements of a `PARAMETER` element.
	///
	/// This parser is infallible. `PARAMETER` elements are not required to have
	/// children, in which case this parser does nothing and returns success.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(mut text: CompleteStr) -> ParseResult<Self> {
		let mut out = Self::default();
		loop {
			text = if let Ok((rem, s)) = State::<Parameter>::parse(text) {
				out.states.insert(s);
				rem
			}
			else if let Ok((rem, d)) = Description::parse(text) {
				out.description = Some(d);
				rem
			}
			else if let Ok((rem, m)) = Meta::parse(text) {
				out.meta.insert(m);
				rem
			}
			else if let Ok((rem, u)) = Unit::parse(text) {
				out.unit = Some(u);
				rem
			}
			else if let Ok((rem, p)) = PolyConversion::<Parameter>::parse(text) {
				out.poly_transforms.push(p.into());
				rem
			}
			else if let Ok((rem, s)) = SegmentedPolyConversion::<Parameter>::parse(text) {
				out.poly_transforms.push(s);
				rem
			}
			else if let Ok((rem, d)) = Default::parse(text) {
				out.default = Some(d);
				rem
			}
			else if let Ok((rem, m)) = Maximum::parse(text) {
				out.maximum = Some(m);
				rem
			}
			else if let Ok((rem, m)) = Minimum::parse(text) {
				out.minimum = Some(m);
				rem
			}
			else if let Ok((rem, r)) = Required::parse(text) {
				out.required = r;
				rem
			}
			else {
				break;
			};
		}
		Ok((text, out))
	}
}

impl Display for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(32);
		writeln!(out, "{}{} {}",
			<Parameter as Selectable>::KEYWORD,
			<Parameter as Keyword>::KEYWORD,
			self.name
		)?;
		write!(out, "{}", self.body)?;
		write!(fmt, "{}", out)
	}
}

impl Named for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name
	}
}

impl Nameable for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn set_name(&mut self, name: String) {
		self.name = name;
	}
}

impl Parsed for Patch {
	/// Parses a `SELECT_PARAMETER` grammar.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		do_parse!(CompleteStr::from(text.trim_left()),
			name: map!(word, |w| w.to_string()) >>
			body: call!(<<Parameter as Bodied>::Body as Parsed>::parse) >>
			(Self {
				name,
				body,
			})
		)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let text = Cs(r#"
PARAMETER CCSDSVER 0 3 UINT 0 0 0 "COSMOS PRIMARY HEADER VERSION NUMBER"
SELECT_PARAMETER CCSDSVER
  MAXIMUM_VALUE 5
		"#);

		let (rem, p) = Parameter::parse(text).unwrap();
		assert_matches!(p.offset, Some(0));
		assert_eq!(p.width, 3);
		assert_matches!(p.kind, Kind::Uint);
		assert_matches!(p.minimum, Some(KindVal::Uint(0)));
		assert_matches!(p.maximum, Some(KindVal::Uint(0)));
		assert_matches!(p.default, Some(KindVal::Uint(0)));
		assert_matches!(p.description(), Some("COSMOS PRIMARY HEADER VERSION NUMBER"));

		let (rem, s) = Parameter::select(rem).unwrap();
		assert_matches!(s.body.maximum, Some(Maximum(KindVal::Int(5))));
		let mut p = p;
		p.patch(s);
		assert_matches!(p.maximum, Some(KindVal::Uint(5)));
		assert!(rem.trim().is_empty());
	}

	#[test]
	fn render() {
		//  Note that our Display impls always emit canonicalized, strictly
		//  correct, strings that will frequently NOT match the reference files
		//  in assets/.
		//
		//  This is correct behavior.
		let text = Cs(r#"
PARAMETER MODE 80 8 UINT 0 1 0 "DATA COLLECTION MODE"
	STATE "DIAG" 1 HAZARDOUS "Diagnostic mode consumes extra power"
	STATE "NORMAL" 0
SELECT_PARAMETER MODE
	DEFAULT_VALUE 1
		"#);

		let (rem, param) = Parameter::parse(text).unwrap();
		let (rem, patch) = Parameter::select(rem).unwrap();
		assert!(rem.trim().is_empty());
		assert_eq!(text.trim(), format!("{}{}", param, patch).trim());
	}

	#[test]
	fn lines() {
		let text = Cs(r#"
PARAMETER MODE 80 8 UINT 0 1 0 "DATA COLLECTION MODE"
	STATE "DIAG" 1 HAZARDOUS "Diagnostic mode consumes extra power"
	STATE "NORMAL" 0
HAZARDOUS "This is not on the parameter"
		"#);

		let (haz, param) = Parameter::parse(text).unwrap();
		let s: &State<Parameter> = &param[1];
		assert!(!s.has_extra());

		assert_eq!(haz.trim_right(), "\nHAZARDOUS \"This is not on the parameter\"");
	}
}
