#![doc(include = "../doc/cosmos/macros.md")]

#![cfg_attr(feature = "cargo-clippy", allow(stutter))]

use super::{
	ParseResult,
	parse_utils::{
		first_line_only,
		unum,
	},
	traits::{
		Described,
		Keyword,
		Nameable,
		Parsed,
	},
};
use nom::types::CompleteStr;
use std::{
	default::Default,
	fmt::{
		self,
		Debug,
		Display,
		Formatter,
	},
	ops::Deref,
};
use tap::TapResultOps;

#[doc(include = "../doc/cosmos/macros.md")]
#[derive(Clone, Debug, Default)]
#[cfg_attr(feature = "cargo-clippy", allow(stutter))]
pub struct Macro<T: Clone + Debug + Default> {
	/// The starting value of the macro expansion loop.
	pub start: u64,
	/// The stopping value of the macro expansion loop.
	pub stop: u64,
	/// The macro text to be processed.
	pub text: String,
	/// The values created by processing the macro and then parsing the output.
	pub inner: Vec<T>,
}

impl<T: Clone + Debug + Default> Macro<T> {
	/// The terminating keyword.
	const TERMINATOR: &'static str = "MACRO_APPEND_END";
}

impl<T: Clone + Debug + Default + Described + Nameable + Parsed> Macro<T> {
	/// Parses a COSMOS macro invocation, returning the processed result of the
	/// macro.
	#[cfg_attr(feature = "trace", flame)]
	#[cfg_attr(feature = "cargo-clippy", allow(shadow_reuse))]
	#[cfg_attr(feature = "cargo-clippy", allow(shadow_unrelated))]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		let (rem, _) = <Self as Keyword>::seek(text)?;
		let (rem, (start, stop)) = first_line_only(rem, |line| {
			ws!(line, tuple!(unum, unum))
		}).tap_err(|_| {
			error!("The MACRO keyword block requires start and stop values");
		})?;
		let (out, src) = terminated!(rem,
			take_until!(Self::TERMINATOR),
			tag!(Self::TERMINATOR)
		).tap_err(|_| {
			error!("The MACRO keyword block requires inner text");
		})?;

		let inner: CompleteStr = src.trim_right().into();
		//  Parse the macro's inner text once.
		//  TODO: It is an error for APPEND_MACRO to contain non-APPEND fields.
		let (rem, base) = many1!(inner, <T as Parsed>::parse).tap_err(|_| {
			error!("The MACRO keyword block must not have unparseable inner text");
		})?;

		//  If the parser terminates without eating all the macro text, abort
		//  using the start of the macro text as context. Making the abort
		//  status index into the interior of the macro text is too complex for
		//  now.
		if !rem.trim().is_empty() {
			use nom::{
				Err::Error,
				ErrorKind::Custom,
			};
			error!("The MACRO keyword block must not have unparseable inner text");
			return Err(Error(error_position!(src, Custom('m' as u32))));
		}
		let text = inner.to_string();
		//  Single-allocate the expansion buffer.
		#[cfg_attr(feature = "cargo-clippy", allow(cast_possible_truncation))]
		let mut inner = Vec::with_capacity(base.len() * (stop + 1 - start) as usize);
		#[cfg(feature = "devel")]
		let _g = ::flame::start_guard("Unrolling macro");
		//  Now clone the parsed structures and tweak their text.
		for n in start ..= stop {
			let num = n.to_string();
			#[cfg(feature = "devel")]
			::flame::start("Clone");
			let mut group = base.clone();
			#[cfg(feature = "devel")]
			::flame::end("Clone");
			#[cfg(feature = "devel")]
			::flame::start("Modify");
			for field in &mut group {
				let name = field.name().replace("#x", &num);
				field.set_name(name);
				let desc = field.description().map(|d| d.replace("#x", &num));
				if let Some(d) = desc {
					field.describe(d);
				}
			}
			#[cfg(feature = "devel")]
			::flame::end("Modify");
			inner.extend(group);
		}
		Ok((out.trim_left().into(), Self {
			start,
			stop,
			text,
			inner,
		}))
	}

	/// Discards the macro metadata, leaving only the collection of expanded
	/// elements.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap(self) -> Vec<T> {
		self.inner
	}
}

impl<T: Clone + Debug + Default> Deref for Macro<T> {
	type Target = [T];

	#[cfg_attr(feature = "trace", flame)]
	fn deref(&self) -> &Self::Target {
		&self.inner
	}
}

impl<T: Clone + Debug + Default> Display for Macro<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		writeln!(out, "{} {} {}", <Self as Keyword>::KEYWORD, self.start, self.stop)?;
		writeln!(out, "{}", self.text.trim_right())?;
		write!(out, "{}", Self::TERMINATOR)?;
		write!(fmt, "{}", out)
	}
}

impl<T: Clone + Debug + Default> Keyword for Macro<T> {
	const KEYWORD: &'static str = "MACRO_APPEND_START";
}

#[cfg(test)]
mod tests {
	use super::*;
	use item::Item;
	use traits::Named;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn expansion() {
		let source = r#"
MACRO_APPEND_START 1 10
	APPEND_ITEM SETTING_#x 16 UINT "SETTING #x"
MACRO_APPEND_END
ITEM ANOTHER 160 UINT "Not macroed"
		"#.trim();

		let expanded = r#"
APPEND_ITEM SETTING_1 16 UINT "SETTING 1"
APPEND_ITEM SETTING_2 16 UINT "SETTING 2"
APPEND_ITEM SETTING_3 16 UINT "SETTING 3"
APPEND_ITEM SETTING_4 16 UINT "SETTING 4"
APPEND_ITEM SETTING_5 16 UINT "SETTING 5"
APPEND_ITEM SETTING_6 16 UINT "SETTING 6"
APPEND_ITEM SETTING_7 16 UINT "SETTING 7"
APPEND_ITEM SETTING_8 16 UINT "SETTING 8"
APPEND_ITEM SETTING_9 16 UINT "SETTING 9"
APPEND_ITEM SETTING_10 16 UINT "SETTING 10"
		"#.trim();

		let (rest, mac) = Macro::<Item>::parse(Cs(source)).unwrap();
		assert_eq!(rest.0, "ITEM ANOTHER 160 UINT \"Not macroed\"");

		let text: &String = &mac.text;
		assert_eq!(text.trim_right(), "\tAPPEND_ITEM SETTING_#x 16 UINT \"SETTING #x\"");

		let inner: &Vec<Item> = &mac.inner;
		let i0: &Item = &inner[0];
		assert_eq!(i0.name(), "SETTING_1");
		assert_matches!(i0.description(), Some("SETTING 1"));

		let mut expand = String::new();
		for item in inner {
			use std::fmt::Write;
			write!(expand, "{}", item).unwrap();
		}
		assert_eq!(expand.trim(), expanded);
	}

	#[test]
	fn render() {
		let text = r#"
MACRO_APPEND_START 1 10
	APPEND_ITEM SETTING_#x 16 UINT "SETTING #x"
MACRO_APPEND_END
		"#.trim();

		assert_eq!(text, format!("{}", Macro::<Item>::parse(Cs(text)).unwrap().1).trim());
	}
}
