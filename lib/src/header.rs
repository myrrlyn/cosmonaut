#![doc(include = "../doc/cosmos/header.md")]

use super::{
	ParseResult,
	endian::Endian,
	parse_utils::{
		dquote_escape,
		first_line_only,
		word,
	},
	traits::{
		Described,
		Named,
		Targeted,
	},
};
use nom::types::CompleteStr;
use std::fmt::{
	self,
	Display,
	Formatter,
};
use string_cache::DefaultAtom as Atom;
use tap::TapResultOps;

#[doc(include = "../doc/cosmos/header.md")]
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Header {
	/// The target system or scope for which the message is used.
	pub target: Atom,
	/// The message name.
	pub name: Atom,
	/// The endianness of the message.
	pub endian: Endian,
	/// A human-readable summary of the message block’s purpose and behavior.
	pub description: Option<String>,
}

impl Header {
	/// Parses a message header from a COSMOS text stream.
	///
	/// This function returns a `Header` structure and the remainder of the
	/// stream on success, or the remainder of the stream after an error on
	/// failure. A partially-successful parse consumes the valid text.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		first_line_only(text, |line| do_parse!(line,
			target: map!(word, |w| Atom::from(&w as &str)) >>
			name: map!(word, |w| Atom::from(&w as &str)) >>
			endian: call!(Endian::parse) >>
			description: opt!(map!(dquote_escape, String::from)) >>
			(Self {
				target,
				name,
				endian,
				description,
			})
		).tap_err(|_| {
			error!("The `Header` structure __must not fail__ to parse!");
		}))
	}
}

impl Described for Header {
	#[cfg_attr(feature = "trace", flame)]
	fn description(&self) -> Option<&str> {
		self.description.as_ref().map(|s| s as &str)
	}

	#[cfg_attr(feature = "trace", flame)]
	fn describe(&mut self, description: String) {
		self.description = Some(description);
	}
}

impl Display for Header {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut s = String::with_capacity(80);
		write!(s, "{} {} {}", self.target, self.name, self.endian)?;
		if let Some(d) = self.description() {
			write!(s, " {:?}", d)?;
		}
		write!(f, "{}", s)
	}
}

impl Named for Header {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name
	}
}

impl Targeted for Header {
	#[cfg_attr(feature = "trace", flame)]
	fn target(&self) -> &str {
		&self.target
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let with_desc = Cs("TARGET NAME BIG_ENDIAN \"Description\"");
		let without_desc = Cs("TARGET NAME LITTLE_ENDIAN");
		match Header::parse(with_desc) {
			Ok((r, h @ Header {
				description: Some(_),
				endian: Endian::Big,
				..
			})) => {
				assert_eq!(&h.name as &str, "NAME");
				assert_eq!(&h.target as &str, "TARGET");
				assert_matches!(
					h.description.as_ref().map(|s| s as &str),
					Some("Description")
				);
				assert!(r.is_empty());
			},
			e => panic!("Error in the header parser: {:?}", e),
		}
		match Header::parse(without_desc) {
			Ok((r, h @ Header {
				description: None,
				endian: Endian::Little,
				..
			})) => {
				assert_eq!(&h.name as &str, "NAME");
				assert_eq!(&h.target as &str, "TARGET");
				assert!(r.is_empty());
			},
			e => panic!("Error in the header parser: {:?}", e),
		}
	}
}
