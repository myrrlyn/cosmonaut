/*! An enum over all COSMOS text root blocks.

This is the simplest way to handle alternating over the four structures.
!*/

use super::{
	ParseResult,
	command::Command,
	parse_utils::span,
	telemetry::Telemetry,
	traits::{
		Named,
		Keyword,
		Parsed,
		Selectable,
		Targeted,
	},
};
use nom::types::CompleteStr;

/// The root COSMOS structure type.
#[derive(Clone, Debug)]
pub enum Block {
	/// A `Command` packet.
	Cmd(Command),
	/// A `Telemetry` packet.
	Tlm(Telemetry),
	/// A `SELECT_COMMAND` patch element.
	SelectCmd(<Command as Selectable>::Patch),
	/// A `SELECT_TELEMETRY` patch element.
	SelectTlm(<Telemetry as Selectable>::Patch),
}

impl Block {
	/// Advances through source text until one of the block parsers can begin.
	///
	/// This advances by token, and will not attempt a parse on keywords found
	/// inside quoted strings or comments.
	#[cfg_attr(feature = "trace", flame)]
	pub fn seek_to(text: CompleteStr) -> ParseResult<CompleteStr> {
		use nom::{
			Err::Error,
			ErrorKind::Custom,
		};
		let mut cursor = text;
		while let Ok((rem, _)) = span(cursor) {
			if alt!(cursor,
				call!(<Command as Keyword>::seek) |
				call!(<Telemetry as Keyword>::seek) |
				call!(<Command as Selectable>::seek) |
				call!(<Telemetry as Selectable>::seek)
			).is_ok() {
				let rlen = text.len() - cursor.len();
				return Ok((cursor, text[.. rlen].into()));
			}
			cursor = rem;
		}
		Err(Error(error_position!(text, Custom('b' as u32))))
	}
}

impl Named for Block {
	fn name(&self) -> &str {
		match *self {
			Block::Cmd(ref c) => c.name(),
			Block::Tlm(ref t) => t.name(),
			Block::SelectCmd(ref s) => s.name(),
			Block::SelectTlm(ref s) => s.name(),
		}
	}
}

impl Parsed for Block {
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		alt!(text,
			Command::parse => { |c| Block::Cmd(c) } |
			Telemetry::parse => { |t| Block::Tlm(t) } |
			Command::select => { |c| Block::SelectCmd(c) } |
			Telemetry::select => { |t| Block::SelectTlm(t) }
		)
	}
}

impl Targeted for Block {
	fn target(&self) -> &str {
		match *self {
			Block::Cmd(ref c) => c.target(),
			Block::Tlm(ref t) => t.target(),
			Block::SelectCmd(ref s) => s.target(),
			Block::SelectTlm(ref s) => s.target(),
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn skip_bad_text() {
		let text = r#"
This is some explanatory text because with the addition of a valid span
tokenizer, it is now technically possible for the COSMOS text to be written in a
literate style, with bare comments. It is even possible to use quoted strings in
order to prevent the keyword matcher from finding false positives on keywords
like "COMMAND" or 'TELEMETRY' or 'SELECT_COMMAND' or "SELECT_TELEMETRY".

TELEMETRY FOO BAR BIG_ENDIAN "Demonstrates the fast-forward mechanism"
  ITEM QUUX 0 8 UINT
		"#.trim();

		let (rest, skip) = Block::seek_to(text.into()).unwrap();
		assert_eq!(skip, text[.. 384].into());
		let (rem, tlm) = Block::parse(rest).unwrap();
		assert!(rem.trim().is_empty());
		if let Block::Tlm(tlm) = tlm {
			assert_eq!(tlm.items().len(), 1);
		}
		else {
			panic!("Didn’t parse a Telemetry from TELEMETRY???");
		}
	}
}
