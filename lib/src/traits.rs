#![doc(include = "../doc/cosmos/traits.md")]

use super::{
	ParseResult,
	decorators::State,
	parse_utils::span,
};
use nom::types::CompleteStr;

/// A marker trait indicating that an object carries a data body.
pub trait Bodied {
	/// The type of the object's data body elements.
	///
	/// This structure must be capable of holding all the parse results of valid
	/// grammars for a structure's logical children. Typically, this will be a
	/// structure rather than a vector of enumerations.
	type Body: Clone + Parsed + Sized;

	/// Applies a `Body` structure to self, moving any necessary items out of
	/// the `Body` and into `self`.
	fn apply(&mut self, body: Self::Body);
}

/// A trait for accessing the textual description of an object.
pub trait Described {
	/// Retrieves the object's description, if it has one.
	fn description(&self) -> Option<&str>;

	/// Sets the object's description.
	fn describe(&mut self, description: String);
}

/// A marker trait for structures that have COSMOS keywords.
pub trait Keyword {
	/// The base keyword on which the structure's parser seeks.
	///
	/// Some keywords may be preceded by other modifiers, for which the relevant
	/// parsers must account.
	const KEYWORD: &'static str;

	/// Determines if the text begins with the appropriate keyword.
	#[cfg_attr(feature = "trace", flame)]
	fn seek(text: CompleteStr) -> ParseResult<CompleteStr> {
		tag!(CompleteStr::from(text.trim_left()), Self::KEYWORD)
	}

	/// Seeks to the next site of the keyword, returning the intervening text.
	///
	/// If this function succeeds, the return cursor will have the keyword as
	/// the first word in the remnant, so Keyword::seek(remnant) will find it.
	#[cfg_attr(feature = "trace", flame)]
	fn seek_to(text: CompleteStr) -> ParseResult<CompleteStr> {
		use nom::{
			Err::Error,
			ErrorKind::Custom
		};
		let mut cursor = text;

		//  Scan through the text, taking a single atom at a time and testing it
		//  against the keyword. This will handle double- and single- quoted
		//  strings correctly.
		while let Ok((rem, span)) = span(cursor) {
			let t = <Self as Keyword>::seek(span);
			if t.is_ok() {
				let rlen = text.len() - cursor.len();
				return Ok((text[rlen ..].into(), text[.. rlen].into()));
			}
			cursor = rem;
		}
		Err(Error(error_position!(text, Custom('s' as u32))))
	}
}

/// A trait for accessing the name of an object.
pub trait Named {
	/// Retrieves the object's name.
	fn name(&self) -> &str;
}

/// A trait for setting the name of an object.
pub trait Nameable: Named {
	/// Sets the object's name.
	fn set_name(&mut self, name: String);
}

/// A marker trait for types which may be parsed.
///
/// This trait contains one method, which returns a heap-allocated instance of
/// the parsed data structure on success. This trait is not recomended for
/// general use; it exists specifically for internal use to power some of the
/// more esoteric generic code.
///
/// Specifically, it is implemented by `Item` and `Parameter` because they are
/// the target types of `Command` and `Telemetry` implementations of `Bodied`,
/// and there are parsers which are generic over `Bodied` that require access to
/// the parse implementation of `Bodied::Body`.
///
/// When using traits as type parameters, the inherent methods on the concrete
/// type are no longer accessible, *even if* the concrete type is known and all
/// possible concrete types are known to have an inherent method of a fixed
/// signature. So this trait provides a means of accessing some parse
/// implementation, even through generic code operating indirectly.
///
/// Trait methods must return a fixed-size object, and thus CANNOT return `Self`
/// directly because each implementor may have a different size. As a result,
/// the parse method here returns a pointer to the parsed structure, and the
/// implementation for `Item` and `Parameter` just calls the respective inherent
/// method and boxes the result.
///
/// ----------------------------------------------------------------------------
///
/// If and when Rust stabilizes the ability to return an object *directly into*
/// a collection, this trait can be refactored to do that, and skip the `Box`ing
/// of the parsed structure: simply be called with a mutable reference to a
/// receiver, and the trait method can move the parsed object directly without
/// an intermediate heap allocation that will (probably) be moved around upon
/// receipt.
///
/// The parsers that mandate using this trait, for instance, immediately unbox
/// the returned item and then move it back from the stack into the heap, only
/// this time inside a collection type. When the collection types are able to
/// receive directly, this trait can parse on the stack and then do a single
/// move to the heap: into the receiving collection.
///
/// This trait does not need to be implemented on all parseable types; only the
/// ones which need to have their parse method knowable after type erasure.
/// Most types are not in this situation, and so do not need it.
pub trait Parsed: Sized {
	/// Parses some source text.
	fn parse(text: CompleteStr) -> ParseResult<Self>;
}

/// A trait for permitting selection and editing of elements defined elsewhere.
///
/// This requires that implementing types are `Bodied`, since `SELECT_TYPE` and
/// `TYPE` elements are followed by the exact same body grammars.
pub trait Selectable: Bodied + Keyword {
	/// The keyword for beginning a selection.
	const KEYWORD: &'static str = "SELECT_";

	/// The type that holds patch data collected from the `SELECT` grammar.
	type Patch: Parsed;

	/// Determines if the source text starts with the correct `SELECT` keyword.
	#[cfg_attr(feature = "trace", flame)]
	fn seek(text: CompleteStr) -> ParseResult<CompleteStr> {
		let trimmed = CompleteStr::from(text.trim_left());
		let klen = <Self as Selectable>::KEYWORD.len() + <Self as Keyword>::KEYWORD.len();
		do_parse!(trimmed,
			tag!(<Self as Selectable>::KEYWORD) >>
			tag!(<Self as Keyword>::KEYWORD) >>
			(trimmed[.. klen].into())
		)
	}

	/// Processes the `SELECT` grammar.
	#[cfg_attr(feature = "trace", flame)]
	fn select(text: CompleteStr) -> ParseResult<Self::Patch> {
		do_parse!(text,
			call!(<Self as Selectable>::seek) >>
			patch: call!(<<Self as Selectable>::Patch as Parsed>::parse) >>
			(patch)
		)
	}

	/// Overlays a `SELECT` patch onto self.
	fn patch(&mut self, patch: Self::Patch);
}

/// A marker trait for elements that accept `State` decorators.
pub trait Stateful {
	/// `State` decorators can have a decorator themselves; this is the type of
	/// that decoration.
	type Extra;

	/// Accessor for a named `State` in the `Stateful` implementor.
	fn state(&self, name: &str) -> Option<&State<Self>>;
}

/// A marker trait for elements that have a direct association with a target.
///
/// This trait should only be implemented by `Command` and `Telemetry`, and
/// their respective `Selectable::Patch` types.
pub trait Targeted {
	/// The target namespace to which the element belongs.
	fn target(&self) -> &str;
}
