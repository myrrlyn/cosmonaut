/*! # Dictionary

This is the root export type of the parser library. It holds collections of
targets, which collect `Command` and `Telemetry` blocks, and has a public method
for parsing text into a `Dictionary` instance.
!*/

use super::{
	block::Block,
	target::Target,
	traits::{
		Named,
		Parsed,
		Selectable,
		Targeted,
	},
};
use nom::types::CompleteStr;
use std::{
	collections::HashMap,
	convert::From,
	fmt::{
		self,
		Display,
		Formatter,
	},
	iter::IntoIterator,
	ops::Index,
};
use string_cache::DefaultAtom as Atom;
use tap::{
	TapOps,
	TapOptionOps,
};

/// A collection of parsed data structures, to be used by a client.
#[derive(Clone, Debug, Default)]
pub struct Dictionary {
	/// The set of target namespaces
	pub targets: HashMap<Atom, Target>,
}

impl Dictionary {
	/// Constructs a blank dictionary.
	pub fn new() -> Self {
		<Self as Default>::default()
	}

	/// Retrieves a named target from the set.
	pub fn target(&self, name: &str) -> Option<&Target> {
		self.targets.get(&Atom::from(name))
	}

	/// Parses a source of COSMOS text into structures, and adds them to the
	/// dictionary.
	pub fn parse<'a>(&mut self, source: &'a str) -> Result<(), &'a str> {
		let mut cursor = CompleteStr::from(source);
		let mut hits = 0;
		loop {
			match Block::parse(cursor) {
				Ok((rem, blk)) => {
					let (t, n) = (blk.target().into(), blk.name().into());
					let tgt = self.targets.entry(t).or_insert_with(Target::default);
					match blk {
						Block::Cmd(c) => {
							tgt.commands.insert(n, c).tap_some(|c| {
								warn!("Replacing pre-existing COMMAND at {}/{}", c.target(), c.name());
							});
						},
						Block::Tlm(t) => {
							tgt.telemetry.insert(n, t).tap_some(|t| {
								warn!("Replacing pre-existing TELEMETRY at {}/{}", t.target(), t.name());
							});
						},
						Block::SelectCmd(sc) => {
							tgt.select_cmd.entry(n).or_insert_with(Vec::new).push(sc);
						},
						Block::SelectTlm(st) => {
							tgt.select_tlm.entry(n).or_insert_with(Vec::new).push(st);
						},
					};
					hits += 1;
					cursor = rem;
				},
				Err(_) => {
					if cursor.trim().is_empty() {
						break;
					}
					warn!("Encountered invalid text; attempting to fast-forward");
					if let Ok((rem, _)) = Block::seek_to(cursor) {
						cursor = rem;
					}
					else {
						warn!("No valid text remained in the file; exiting");
						break;
					}
				},
			}
		}
		if hits == 0 {
			warn!("Unable to parse this file");
		}
		else if hits == 1 {
			info!("Parsed 1 structure from this file");
		}
		else {
			info!("Parsed {} structures from this file", hits);
		}
		//  Once the parsing loop has ended, all pending patches need to be
		//  attempted for application. Any patches that do not have a suitable
		//  element to modify are left as-is. Based off of real-world data sets,
		//  it is presumed that there will be fewer patches than full packets,
		//  so this crawls the list of patches and attempts to find packets,
		//  rather than crawling the list of packets and attempting to find
		//  patches.
		self.apply_patches();
		Ok(())
	}

	/// Attempts to apply all SELECT_* blocks to the current block collection.
	fn apply_patches(&mut self) {
		for (ref target_name, ref mut target) in &mut self.targets {
			//  Extract the names of all patch sets that have a matching command.
			let mut names = target.select_cmd
				.keys()
				.filter(|k| target.commands.contains_key(k))
				.map(|k| k.clone())
				.collect::<Vec<_>>();
			//  Pull those patch sets out and apply them to the packet.
			for name in names {
				//  Note: .remove moves, .get_mut borrows mutably
				if let (Some(mut patches), Some(packet)) = (
					target.select_cmd.remove(&name),
					target.commands.get_mut(&name),
				) {
					for patch in patches {
						packet.patch(patch);
					}
				}
				else {
					error!("INCONSISTENT STATE! A PATCHSET OR COMMAND AT {}/{} WAS LOST!", target_name, name);
				}
			}
			//  Extract the names of all patch sets that have a matching telemetry.
			let mut names = target.select_tlm
				.keys()
				.filter(|k| target.telemetry.contains_key(k))
				.map(|k| k.clone())
				.collect::<Vec<_>>();
			for name in names {
				if let (Some(mut patches), Some(packet)) = (
					target.select_tlm.remove(&name),
					target.telemetry.get_mut(&name),
				) {
					for patch in patches {
						packet.patch(patch);
					}
				}
				else {
					error!("INCONSISTENT STATE! A PATCHSET OR COMMAND AT {}/{} WAS LOST", target_name, name);
				}
			}
		}
	}

	/// Gets the statistics of the Dictionary.
	pub fn stats(&self) -> String {
		use std::fmt::Write;
		let msg = "string building shouldn't fail";
		let mut out = String::new();
		let targets = self.targets.keys().collect::<Vec<_>>().tap(|v| v.sort());

		writeln!(out, "Targets: {}", self.targets.keys().count()).expect(msg);
		for _ in (0 .. out.trim().len()).into_iter() {
			write!(out, "-").expect(msg);
		}
		writeln!(out).expect(msg);

		for key in targets {
			let target = &self.targets[key];
			let name = &key as &str;
			let cmds = target.all_cmd().keys().collect::<Vec<_>>();
			let tlms = target.all_tlm().keys().collect::<Vec<_>>();
			let pkts = cmds.len() + tlms.len();
			writeln!(out, "Target {} with {} packet(s):", name, pkts).expect(msg);

			for key in cmds.tap(|v| v.sort_unstable()) {
				let cmd = target.cmd(key).expect("infallible");
				let name = cmd.name();
				let params = cmd.parameters().len();
				writeln!(out, " CMD | {} with {} parameter(s)", name, params).expect(msg);
			}
			for key in tlms.tap(|v| v.sort_unstable()) {
				let tlm = target.tlm(key).expect("infallible");
				let name = tlm.name();
				let items = tlm.items().len();
				writeln!(out, " TLM | {} with {} item(s)", name, items).expect(msg);
			}
		}
		out
	}
}

impl Display for Dictionary {
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		for target in self.targets.values() {
			write!(f, "{}", target)?;
		}
		Ok(())
	}
}

impl<'a> Index<&'a str> for Dictionary {
	type Output = Target;
	fn index(&self, idx: &'a str) -> &Self::Output {
		self.target(idx)
			.expect(&format!("Selected dictionary has no target {}", idx))
	}
}

impl<'a> IntoIterator for &'a Dictionary {
	type Item = (&'a Atom, &'a Target);
	type IntoIter = <&'a HashMap<Atom, Target> as IntoIterator>::IntoIter;
	fn into_iter(self) -> Self::IntoIter {
		(&self.targets).into_iter()
	}
}
