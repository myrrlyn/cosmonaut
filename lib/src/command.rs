#![doc(include = "../doc/cosmos/command.md")]

use super::{
	ParseResult,
	cosmos_macro::Macro,
	decorators::{
		Description,
		DisableMessages,
		Disabled,
		Hazard,
		Hidden,
		Meta,
		MetaSet,
	},
	header::Header,
	parameter::Parameter,
	traits::{
		Bodied,
		Described,
		Keyword,
		Named,
		Parsed,
		Selectable,
		Targeted,
	},
};
use nom::types::CompleteStr;
use std::{
	fmt::{
		self,
		Display,
		Formatter,
	},
	ops::{
		Deref,
		Index,
	},
};
use string_cache::DefaultAtom as Atom;

#[doc(include = "../doc/cosmos/command.md")]
#[derive(Clone, Debug, Default)]
pub struct Command {
	/// The meta information describing the command.
	pub header: Header,
	/// The set of child elements, such as `PARAMETER`s, that compose the
	/// `COMMAND` element in text.
	pub body: <Command as Bodied>::Body,
}

#[doc(include = "../doc/cosmos/element_body.md")]
#[derive(Clone, Debug, Default)]
pub struct Body {
	/// A set of `PARAMETER` elements that make up the serialized body of the
	/// `Command`.
	pub parameters: Vec<Parameter>,
	/// A set of `SELECT_PARAMETER` patch elements that must be folded onto the
	/// parameter list.
	pub patches: Vec<<Parameter as Selectable>::Patch>,
	/// The contents of a `DESCRIPTION "text"` line. When the `Body` structure
	/// is applied to a `Command`, this must be overlaid on the description
	/// field in the `Command`'s `Header` structure.
	pub description: Option<Description>,
	/// An optional hazard warning parsed from a `HAZARDOUS` line.
	pub hazard: Option<Hazard>,
	/// A set of meta-information describing the `COMMAND` element.
	pub meta: MetaSet,
	/// A flag stating that use of the `Command` should not display messages in
	/// the client. This does not disable logging for the `Command`.
	pub disable_messages: DisableMessages,
	/// A flag stating that the `Command` should be hidden from client display,
	/// but not forbidden from use.
	pub hidden: Hidden,
	/// A flag stating that the `Command` should be forbidden from client use.
	pub disabled: Disabled,
}

#[doc(include = "../doc/cosmos/patch.md")]
#[derive(Clone, Debug, Default)]
pub struct Patch {
	/// The name of the `Command` to patch.
	///
	/// This name is resolved when the patch is applied, not when it is parsed.
	/// It is not an error for a `SELECT_COMMAND` block to be parsed before the
	/// `COMMAND` block that it selects.
	pub name: Atom,
	/// The target of the `Command` to patch.
	///
	/// As with `name`, this target is not resolved until the patch is applied.
	pub target: Atom,
	/// The edit body.
	pub body: <Command as Bodied>::Body,
}

impl Command {
	/// Borrows a named parameter.
	#[cfg_attr(feature = "trace", flame)]
	pub fn param<'a>(&self, name: &'a str) -> Option<&Parameter> {
		self.body.parameters.iter().find(|p| p.name() == name)
	}

	/// Borrows a named parameter, mutably.
	#[cfg_attr(feature = "trace", flame)]
	pub fn param_mut<'a>(&mut self, name: &'a str) -> Option<&mut Parameter> {
		self.body.parameters.iter_mut().find(|p| p.name() == name)
	}

	/// Borrows the set of parameters.
	///
	/// The parameter set cannot be borrowed mutably. Any changes to parameters
	/// must be performed by generating a suitable `Patch` structure and
	/// applying it to the `Command` instance.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parameters(&self) -> &[Parameter] {
		&self.body.parameters
	}
}

impl Bodied for Command {
	type Body = Body;

	#[cfg_attr(feature = "trace", flame)]
	fn apply(&mut self, body: <Self as Bodied>::Body) {
		use std::mem;
		//  Take apart the body for piece-wise application.
		let Body {
			parameters,
			patches,
			description,
			hazard,
			meta,
			disable_messages,
			hidden,
			disabled,
		} = body;
		self.body.parameters.extend(parameters);
		self.body.patches.extend(patches);
		self.body.meta.extend(meta);
		if let Some(Description { text }) = description {
			self.describe(text);
		}
		if self.body.hazard.is_none() && hazard.is_some() {
			self.body.hazard = hazard;
		}
		else if let Some(Hazard { text: Some(t) }) = hazard {
			self.body.hazard = Some(Hazard::from(t));
		}
		if *disable_messages {
			self.body.disable_messages = disable_messages;
		}
		if *hidden {
			self.body.hidden = hidden;
		}
		if *disabled {
			self.body.disabled = disabled;
		}
		//  Pull out all the SELECT_PARAMETER patches and attempt to
		//  apply them to existing parameters.
		let patches = mem::replace(&mut self.body.patches, Vec::new());
		for patch in patches {
			//  if let Some(ref mut p) = self.param_mut(patch.name())
			//  causes the borrow of self to extend over the `else` branch, and
			//  thus `else` cannot immutably access `self.name()`
			if self.param(patch.name()).is_some() {
				//  Get mutable access to the Parameter that the patch names,
				//  and apply the patch to it.
				self.param_mut(patch.name()).map(|p| p.patch(patch));
			}
			else {
				error!("COMMAND {:?} has no PARAMETER named {:?}", self.name(), patch.name());
				//  Put the patch back; a parameter by this name might appear.
				self.body.patches.push(patch);
			}
		}
	}
}

impl Deref for Command {
	type Target = [Parameter];
	fn deref(&self) -> &Self::Target {
		&self.body.parameters
	}
}

impl Described for Command {
	#[cfg_attr(feature = "trace", flame)]
	fn description(&self) -> Option<&str> {
		self.header.description()
	}

	#[cfg_attr(feature = "trace", flame)]
	fn describe(&mut self, description: String) {
		self.header.describe(description);
	}
}

impl Display for Command {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(80);
		writeln!(out, "{} {}", <Self as Keyword>::KEYWORD, self.header)?;
		writeln!(out, "{}", self.body)?;
		write!(fmt, "{}", out)
	}
}

impl Index<usize> for Command {
	type Output = Parameter;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: usize) -> &Self::Output {
		&self.body.parameters[idx]
	}
}

impl<'a> Index<&'a str> for Command {
	type Output = Parameter;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: &'a str) -> &Self::Output {
		self.param(idx)
			.expect(&format!("Command `{}` has no parameters named `{}`",
				self.name(), idx))
	}
}

impl Keyword for Command {
	const KEYWORD: &'static str = "COMMAND";
}

impl Named for Command {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		self.header.name()
	}
}

impl Parsed for Command {
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		let (rem, (h, b)) = do_parse!(text,
			call!(<Self as Keyword>::seek) >>
			h: call!(Header::parse) >>
			b: call!(<<Self as Bodied>::Body as Parsed>::parse) >>
			(h, b)
		)?;
		let mut out = Self::default();
		out.header = h;
		out.apply(b);
		Ok((rem, out))
	}
}

impl Selectable for Command {
	type Patch = Patch;

	#[cfg_attr(feature = "trace", flame)]
	fn patch(&mut self, patch: Self::Patch) {
		//  Verify that the patch is applicable.
		if self.target() == patch.target() && self.name() == patch.name() {
			self.apply(patch.body);
		}
	}
}

impl Targeted for Command {
	/// Gets the target namespace for this COMMAND.
	fn target(&self) -> &str {
		&self.header.target
	}
}

impl Display for Body {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(128);
		let Self {
			ref parameters,
			ref patches,
			ref meta,
			ref hazard,
			ref disable_messages,
			ref hidden,
			ref disabled,
			ref description,
		} = *self;
		for param in parameters {
			for line in format!("{}", param).lines() {
				writeln!(out, "\t{}", line)?;
			}
		}
		if let Some(ref d) = *description {
			writeln!(out, "\t{}", d)?;
		}
		if let Some(ref h) = *hazard {
			writeln!(out, "\t{}", h)?;
		}
		if **disable_messages {
			writeln!(out, "\t{}", disable_messages)?;
		}
		if **hidden {
			writeln!(out, "\t{}", hidden)?;
		}
		if **disabled {
			writeln!(out, "\t{}", disabled)?;
		}
		for m in meta {
			writeln!(out, "\t{}", m)?;
		}
		//  These are SELECT_PARAMETER, not SELECT_COMMAND.
		for patch in patches {
			for line in format!("{}", patch).lines() {
				writeln!(out, "\t{}", line)?;
			}
		}
		write!(fmt, "{}", out)
	}
}

impl Parsed for Body {
	/// Parses the children of a `COMMAND` element.
	///
	/// `COMMAND`s are required to have at least one child, and this parser will
	/// fail if none are found.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		let mut rem = text.trim_left().into();
		let mut out = Self::default();
		loop {
			rem = if let Ok((rem, p)) = Parameter::parse(rem) {
				out.parameters.push(p);
				rem
			}
			else if let Ok((rem, p)) = Parameter::select(rem) {
				out.patches.push(p);
				rem
			}
			else if let Ok((rem, mac)) = Macro::<Parameter>::parse(rem) {
				out.parameters.extend(mac.unwrap());
				rem
			}
			else if let Ok((rem, desc)) = Description::parse(rem) {
				out.description = Some(Description::from(desc));
				rem
			}
			else if let Ok((rem, h)) = Hazard::parse(rem) {
				out.hazard = Some(h);
				rem
			}
			else if let Ok((rem, m)) = Meta::parse(rem) {
				out.meta.insert(m);
				rem
			}
			else if let Ok((rem, dm)) = DisableMessages::parse(rem) {
				out.disable_messages = dm;
				rem
			}
			else if let Ok((rem, h)) = Hidden::parse(rem) {
				out.hidden = h;
				rem
			}
			else if let Ok((rem, d)) = Disabled::parse(rem) {
				out.disabled = d;
				rem
			}
			else {
				break;
			};
		}
		if rem.len() == text.trim_left().len() {
			use nom::{
				Err::Error,
				ErrorKind::Custom,
			};
			error!("COMMAND elements must have at least one child element");
			return Err(Error(error_position!(text, Custom('C' as u32))));
		}
		Ok((rem, out))
	}
}

impl Display for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		writeln!(out, "{}{} {} {}",
			<Command as Selectable>::KEYWORD,
			<Command as Keyword>::KEYWORD,
			self.target,
			self.name
		)?;
		writeln!(out, "{}", self.body)?;
		write!(fmt, "{}", out)
	}
}

impl Named for Patch {
	fn name(&self) -> &str {
		&self.name
	}
}

impl Parsed for Patch {
	/// Parses a `SELECT_COMMAND` element.
	///
	/// `SELECT_COMMAND` elements are required to have children, and this parser
	/// will fail if none are found.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		use super::parse_utils::word;
		do_parse!(CompleteStr::from(text.trim_left()),
			target: map!(word, |w| Atom::from(&w as &str)) >>
			name: map!(word, |w| Atom::from(&w as &str)) >>
			body: call!(<<Command as Bodied>::Body as Parsed>::parse) >>
			(Self {
				name,
				target,
				body,
			})
		)
	}
}

impl Targeted for Patch {
	fn target(&self) -> &str {
		&self.target
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let text = r#"
COMMAND TARGET COLLECT_DATA BIG_ENDIAN "Commands my target to collect data"
  HAZARDOUS "Data collection takes power"
  PARAMETER CCSDSVER 0 3 UINT 0 0 0 "COSMOS PRIMARY HEADER VERSION NUMBER"
SELECT_COMMAND TARGET COLLECT_DATA
  HIDDEN
		"#.trim();

		let (rem, c) = Command::parse(Cs(text)).unwrap();
		assert_eq!(c.body.parameters.len(), 1);
		assert!(c.body.hazard.is_some());
		//  By default, `Command` is not hidden.
		assert!(!*c.body.hidden);

		//  Get the SELECT_COMMAND patch
		let (rem, p) = <Command as Selectable>::select(rem).unwrap();
		assert!(*p.body.hidden);

		//  Apply the patch
		let mut c = c;
		c.patch(p);
		//  The `Command` is now hidden.
		assert!(*c.body.hidden);

		assert!(rem.trim().is_empty());
	}

	#[test]
	fn write() {
		let text = r#"
COMMAND TARGET COLLECT_DATA BIG_ENDIAN "Commands my target to collect data"
	PARAMETER CCSDSVER 0 3 UINT 0 0 0 "COSMOS PRIMARY HEADER VERSION NUMBER"
		"#.trim();

		let (_, c) = Command::parse(Cs(text)).unwrap();
		let rendered = format!("{}", c);
		assert_eq!(text, rendered.trim());

		let text = r#"
SELECT_COMMAND TARGET COLLECT_DATA
	HIDDEN
		"#.trim();

		let (_, p) = Command::select(Cs(text)).unwrap();
		let rendered = format!("{}", p);
		assert_eq!(text, rendered.trim());
	}

	#[test]
	fn traits() {
		let text = r#"
COMMAND TARGET COLLECT_DATA BIG_ENDIAN "Commands my target to collect data"
	HAZARDOUS "Data collection takes power"
	PARAMETER CCSDSVER 0 3 UINT 0 0 0 "CCSDS PRIMARY HEADER VERSION NUMBER"
		"#.trim();

		let (_, c) = Command::parse(Cs(text)).unwrap();
		assert_eq!(c.name(), "COLLECT_DATA");
		assert_eq!(c.description().unwrap(), "Commands my target to collect data");

		let text = r#"
First we have a fair amount of text that is not valid COSMOS, in order to test
the seek function.

COMMAND TARGET COLLECT_DATA BIG_ENDIAN "Commands my target to collect data"
	HAZARDOUS "Data collection takes power"
	PARAMETER CCSDSVER 0 3 UINT 0 0 0 "CCSDS PRIMARY HEADER VERSION NUMBER"
		"#.trim();

		assert!(Command::parse(Cs(text)).is_err());

		let (rem, skipped) = <Command as Keyword>::seek_to(Cs(text)).unwrap();
		assert_eq!(skipped.trim(), r#"
First we have a fair amount of text that is not valid COSMOS, in order to test
the seek function.
		"#.trim());
		assert_matches!(Command::parse(rem), Ok((_, Command { .. })));
	}
}
