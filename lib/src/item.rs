#![doc(include = "../doc/cosmos/item.md")]

use super::{
	ParseResult,
	decorators::*,
	endian::Endian,
	kind::*,
	parse_utils::*,
	traits::{
		Bodied,
		Described,
		Keyword,
		Named,
		Nameable,
		Parsed,
		Selectable,
		Stateful,
	},
};
use nom::types::CompleteStr;
use std::{
	fmt::{
		self,
		Display,
		Formatter,
	},
	ops::Index,
};

#[doc(include = "../doc/cosmos/item.md")]
#[derive(Clone, Debug, Default)]
pub struct Item {
	/// The `Item`'s name.
	pub name: String,
	/// The `Item`'s description.
	pub description: Option<String>,
	/// The `Item`'s variant (`Normal`, `Ident`, or `Array`).
	pub variant: Variant,
	/// The kind of the interior data.
	pub kind: Kind,
	/// The endianness of the field (defaults to owner's endianness).
	pub endian: Option<Endian>,
	/// The offset of the field within the owning `Telemetry` packet.
	///
	/// If `None`, then the offset is calculated when the owner compiles its
	/// binary representation.
	pub offset: Option<i64>,
	/// The `Item`'s width in bits.
	pub width: u64,
	/// On array items, the width of each element in bits.
	/// (Ignored on non-array items)
	pub item_width: u64,
	/// On `Ident` variants, this is the signature value; otherwise it is `None`
	pub default: Option<KindVal>,
	/// Miscellaneous information about the `Item`.
	pub body: <Item as Bodied>::Body,
}

#[doc(include = "../doc/cosmos/element_body.md")]
#[derive(Clone, Debug, Default)]
pub struct Body {
	/// A description override for the main Item.
	pub description: Option<Description>,
	/// A unit of measurement contextualizing the inner data.
	pub unit: Option<Unit>,
	/// A set of named values for the inner data.
	pub states: StateSet<Item>,
	/// A set of transforms to be applied during deserialization from binary.
	pub transforms: Vec<SegmentedPolyConversion<Item>>,
	/// Meta information about the item
	pub meta: MetaSet,
}

#[doc(include = "../doc/cosmos/patch.md")]
#[derive(Clone, Debug, Default)]
pub struct Patch {
	/// The name of the `Item` to edit.
	pub name: String,
	/// The patch body.
	pub body: <Item as Bodied>::Body,
}

impl Item {
	/// Accesses the set of named states.
	pub fn states(&self) -> &[State<Self>] {
		&self.body.states
	}
}

impl Bodied for Item {
	type Body = Body;

	#[cfg_attr(feature = "trace", flame)]
	fn apply(&mut self, body: Body) {
		let Body {
			description,
			unit,
			mut states,
			transforms,
			meta,
		} = body;

		//  Ensure that the patch states are all the correct Kind.
		let kind = self.kind;
		states.iter_mut().for_each(|s| s.cast_in_place(kind));
		self.body.states.extend(states);
		self.body.transforms.extend(transforms);
		self.body.meta.extend(meta);
		if let Some(Description { text }) = description {
			self.describe(text);
		}
		if unit.is_some() {
			self.body.unit = unit;
		}
	}
}

impl Described for Item {
	#[cfg_attr(feature = "trace", flame)]
	fn description(&self) -> Option<&str> {
		self.description.as_ref().map(|s| s as &str)
	}

	#[cfg_attr(feature = "trace", flame)]
	fn describe(&mut self, description: String) {
		self.description = Some(description);
	}
}

impl Display for Item {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(80);
		if self.offset.is_none() {
			write!(out, "APPEND_")?;
		}
		write!(out, "{}{} {}", self.variant, <Self as Keyword>::KEYWORD, self.name)?;
		if let Some(ref o) = self.offset {
			write!(out, " {}", o)?;
		}
		write!(out, " {} {}", self.width, self.kind)?;
		if let Some(ref d) = self.default {
			write!(out, " {}", d)?;
		}
		if let Some(d) = self.description() {
			write!(out, " {:?}", d)?;
		}
		if let Some(e) = self.endian {
			write!(out, " {}", e)?;
		}
		writeln!(out)?;
		write!(out, "{}", self.body)?;
		write!(fmt, "{}", out)
	}
}

impl Index<usize> for Item {
	type Output = State<Self>;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: usize) -> &Self::Output {
		&self.body.states[idx]
	}
}

impl<'a> Index<&'a str> for Item {
	type Output = State<Self>;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: &'a str) -> &Self::Output {
		self.state(idx).unwrap_or_else(|| panic!(
			"Item `{}` has no state named `{}`", self.name(), idx
		))
	}
}

impl Keyword for Item {
	const KEYWORD: &'static str = "ITEM";
}

impl Named for Item {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name as &str
	}
}

impl Nameable for Item {
	#[cfg_attr(feature = "trace", flame)]
	fn set_name(&mut self, name: String) {
		self.name = name;
	}
}

impl Parsed for Item {
	/// Parses a `TELEMETRY ITEM` field from a stream of COSMOS text.
	#[cfg_attr(feature = "trace", flame)]
	#[cfg_attr(feature = "cargo-clippy", allow(shadow_reuse))]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		let (text, (append, variant)) = Variant::parse::<Self>(text)?;
		let (mut text, name) = word(text)?;
		let mut out = Self::default();
		out.variant = variant;
		out.name = name.to_string();
		if !append {
			let (rem, offset) = ws!(text, inum)?;
			out.offset = Some(offset);
			text = rem;
		}
		let (text, width) = ws!(text, unum)?;
		let (mut text, kind) = Kind::parse(text)?;
		out.width = width;
		out.kind = kind;
		if let Variant::Ident = out.variant {
			let (rem, def) = KindVal::parse(text)
				.map(|(r, v)| (r, v.cast(out.kind)))?;
			out.default = Some(def);
			text = rem;
		}
		if let Ok((rem, desc)) = dquote_escape(text) {
			out.description = Some(desc.to_string());
			text = rem;
		}
		if let Ok((rem, end)) = Endian::parse(text) {
			out.endian = Some(end);
			text = rem;
		}
		if let Ok((rem, body)) = <Self as Bodied>::Body::parse(text) {
			out.apply(body);
			text = rem;
		}

		Ok((text, out))
	}
}

impl Selectable for Item {
	type Patch = Patch;

	#[cfg_attr(feature = "trace", flame)]
	fn patch(&mut self, patch: Patch) {
		if self.name() == patch.name() {
			self.apply(patch.body);
		}
	}
}

impl Stateful for Item {
	type Extra = Color;

	#[cfg_attr(feature = "trace", flame)]
	fn state(&self, name: &str) -> Option<&State<Self>> {
		self.body.states.get(name)
	}
}

impl Display for Body {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		let Self {
			ref description,
			ref unit,
			ref states,
			ref transforms,
			ref meta,
		} = *self;
		if let Some(ref d) = *description {
			writeln!(out, "\t{}", d)?;
		}
		for state in states {
			writeln!(out, "\t{}", state)?;
		}
		for transform in transforms {
			writeln!(out, "\t{}", transform)?;
		}
		if let Some(ref u) = *unit {
			writeln!(out, "\t{}", u)?;
		}
		for m in meta {
			writeln!(out, "\t{}", m)?;
		}
		write!(fmt, "{}", out)
	}
}

impl Parsed for Body {
	/// Parses the children elements of an `ITEM` element.
	///
	/// This parser is infallible. `ITEM` elements are not required to have
	/// children, in which case this parser does nothing and returns success.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(mut text: CompleteStr) -> ParseResult<Self> {
		let mut out = Self::default();
		loop {
			text = if let Ok((rem, s)) = State::<Item>::parse(text) {
				out.states.insert(s);
				rem
			}
			else if let Ok((rem, d)) = Description::parse(text) {
				out.description = Some(d);
				rem
			}
			else if let Ok((rem, u)) = Unit::parse(text) {
				out.unit = Some(u);
				rem
			}
			else if let Ok((rem, p)) = PolyConversion::<Item>::parse(text) {
				out.transforms.push(p.into());
				rem
			}
			else if let Ok((rem, s)) = SegmentedPolyConversion::<Item>::parse(text) {
				out.transforms.push(s);
				rem
			}
			else if let Ok((rem, m)) = Meta::parse(text) {
				out.meta.insert(m);
				rem
			}
			else {
				break;
			};
		}
		Ok((text, out))
	}
}

impl Display for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		writeln!(out, "{}{} {}",
			<Item as Selectable>::KEYWORD,
			<Item as Keyword>::KEYWORD,
			self.name
		)?;
		write!(out, "{}", self.body)?;
		write!(fmt, "{}", out)
	}
}

impl Named for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name as &str
	}
}

impl Parsed for Patch {
	/// Parses a `SELECT_ITEM` grammar.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		do_parse!(CompleteStr::from(text.trim_left()),
			name: map!(word, |w| w.to_string()) >>
			body: call!(<<Item as Bodied>::Body as Parsed>::parse) >>
			(Self {
				name,
				body,
			})
		)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let text = r#"
ID_ITEM CCSDSAPID 5 11 UINT 102 "COSMOS APPLICATION PROCESS ID"
		"#;

		let (_, i) = Item::parse(Cs(text)).unwrap();
		assert_eq!(i.name(), "CCSDSAPID");
		assert_matches!(i.offset, Some(5));
		assert_eq!(i.width, 11);
		assert_matches!(i.kind, Kind::Uint);
		assert_matches!(i.default, Some(KindVal::Uint(102)));
		assert_matches!(i.description(), Some("COSMOS APPLICATION PROCESS ID"));
		assert!(i.endian.is_none());
	}

	#[test]
	fn render() {
		let text = r#"
ID_ITEM CCSDSAPID 5 11 UINT 102 "COSMOS APPLICATION PROCESS ID"
		"#;

		assert_eq!(text.trim(), format!("{}", Item::parse(Cs(text)).unwrap().1).trim());
	}
}
