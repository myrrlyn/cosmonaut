/*! # Target Sets

The `Target` structure holds all of the packet structures parsed for a specific
target.
!*/

use super::{
	command::Command,
	traits::Selectable,
	telemetry::Telemetry,
};
use std::{
	collections::HashMap,
	fmt::{
		self,
		Display,
		Formatter,
	},
	ops::{
		Deref,
		DerefMut,
		Index,
	},
};
use string_cache::DefaultAtom as Atom;

/// A set of command and telemetry messages in a target namespace.
///
/// This structure also holds all patches in a namespace that are pending
/// application, but patches are not semantically interesting.
#[derive(Clone, Debug, Default)]
pub struct Target {
	/// The set of command messages in the target.
	pub commands: CommandSet,
	/// The set of telemetry messages in the target.
	pub telemetry: TelemetrySet,
	/// The set of command patches awaiting application.
	pub select_cmd: HashMap<Atom, Vec<<Command as Selectable>::Patch>>,
	/// The set of telemetry patches awaiting application.
	pub select_tlm: HashMap<Atom, Vec<<Telemetry as Selectable>::Patch>>,
}

/// Type wrapper over the Command set
#[derive(Clone, Debug, Default)]
pub struct CommandSet(pub HashMap<Atom, Command>);
/// Type wrapper over the Telemetry set
#[derive(Clone, Debug, Default)]
pub struct TelemetrySet(pub HashMap<Atom, Telemetry>);

impl Target {
	/// Selects a command message from the target.
	pub fn cmd(&self, name: &str) -> Option<&Command> {
		self.commands.get(&Atom::from(name))
	}

	/// Gets access to all commands in the target.
	pub fn all_cmd(&self) -> &CommandSet {
		&self.commands
	}

	/// Selects a telemetry message from the target.
	pub fn tlm(&self, name: &str) -> Option<&Telemetry> {
		self.telemetry.get(&Atom::from(name))
	}

	/// Gets access to all telemetry in the target.
	pub fn all_tlm(&self) -> &TelemetrySet {
		&self.telemetry
	}

	/// Checks if the target has patches not yet applied.
	pub fn patches_pending(&self) -> bool {
		!self.select_cmd.is_empty() || !self.select_tlm.is_empty()
	}
}

impl Display for Target {
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		for command in self.commands.values() {
			write!(f, "{}", command)?;
		}
		for telemetry in self.telemetry.values() {
			write!(f, "{}", telemetry)?;
		}
		for patches in self.select_cmd.values() {
			for patch in patches {
				write!(f, "{}", patch)?;
			}
		}
		for patches in self.select_tlm.values() {
			for patch in patches {
				write!(f, "{}", patch)?;
			}
		}
		Ok(())
	}
}

impl CommandSet {
	/// Gets a named command from the set.
	pub fn get(&self, k: &str) -> Option<&Command> {
		self.0.get(&Atom::from(k))
	}
}

impl TelemetrySet {
	/// Gets a named telemetry from the set.
	pub fn get(&self, k: &str) -> Option<&Telemetry> {
		self.0.get(&Atom::from(k))
	}
}

impl Deref for CommandSet {
	type Target = HashMap<Atom, Command>;
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl Deref for TelemetrySet {
	type Target = HashMap<Atom, Telemetry>;
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for CommandSet {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

impl DerefMut for TelemetrySet {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

impl<'a> Index<&'a str> for CommandSet {
	type Output = Command;
	fn index(&self, idx: &'a str) -> &Self::Output {
		&self.0[&Atom::from(idx)]
	}
}

impl<'a> Index<&'a str> for TelemetrySet {
	type Output = Telemetry;
	fn index(&self, idx: &'a str) -> &Self::Output {
		&self.0[&Atom::from(idx)]
	}
}
