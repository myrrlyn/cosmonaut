#![feature(external_doc)]

#![doc(include = "../doc/parser.md")]

#![deny(missing_docs)]
// #![deny(warnings)]

#![cfg_attr(feature = "cargo-clippy", deny(clippy))]
//  Debug formatting is used deliberately.
#![cfg_attr(feature = "cargo-clippy", allow(use_debug))]
#![cfg_attr(all(feature = "cargo-clippy", not(test)), warn(clippy_pedantic))]

#![cfg_attr(feature = "trace", feature(plugin, custom_attribute))]
#![cfg_attr(feature = "trace", plugin(flamer))]

#[cfg(test)]
#[macro_use]
extern crate assert_matches;

#[cfg(feature = "trace")]
extern crate flame;

#[macro_use]
extern crate log;

#[macro_use]
extern crate nom;

extern crate string_cache;
extern crate tap;

#[macro_use]
pub mod traits;

//  Implementation detail; this probably does not need to be in the API.
pub mod block;
pub mod command;
pub mod cosmos_macro;
pub mod decorators;
pub mod dictionary;
pub mod endian;
//  Implementation detail; this certainly does not need to be in the API.
pub mod header;
pub mod item;
pub mod kind;
pub mod parameter;
pub mod parse_utils;
pub mod target;
pub mod telemetry;

//  Re-export useful types from the modules; the module division is not
//  important for clients poking at the internals.
//
//  The traits do not need to be re-exported at the root level
pub use command::Command;
pub use cosmos_macro::Macro;
pub use decorators::*;
pub use dictionary::Dictionary;
pub use endian::Endian;
pub use item::Item;
pub use kind::{
	Kind,
	KindVal,
	Variant,
};
pub use parameter::Parameter;
pub use target::Target;
pub use telemetry::Telemetry;

use nom::{
	IResult,
	types::CompleteStr,
};

/// General return type for component parsers.
///
/// This curries `nom`’s result type to always use an input type of `&str`. The
/// remaining type parameters (output, error) are filled at the call site, as is
/// the lifetime of the input string.
pub type ParseResult<'a, T, E = u32> = IResult<CompleteStr<'a>, T, E>;
