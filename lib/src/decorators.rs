/*! Miscellaneous Field Decorators

These items may be appended to a field to modify it.
!*/

use super::{
	ParseResult,
	kind,
	parse_utils,
	traits,
};

//  When non_modrs_mods (#44660) stabilizes, these path attributes can be stripped
#[path = "decorators/color.rs"]
pub mod colors;
pub use self::colors::Color;
#[path = "decorators/conversion.rs"]
pub mod conversion;
pub use self::conversion::*;
#[path = "decorators/hazard.rs"]
pub mod hazard;
pub use self::hazard::Hazard;
#[path = "decorators/meta.rs"]
pub mod meta;
pub use self::meta::*;
#[path = "decorators/misc.rs"]
pub mod misc;
pub use self::misc::*;
#[path = "decorators/state.rs"]
pub mod state;
pub use self::state::*;
#[path = "decorators/unit.rs"]
pub mod unit;
pub use self::unit::Unit;
