/*! Color words and values
!*/

use super::{
	ParseResult,
	parse_utils::word,
	traits::Parsed,
};
use nom::types::CompleteStr;
use std::{
	cmp::{
		Ord,
		Ordering,
		PartialOrd,
	},
	fmt::{
		self,
		Display,
		Formatter,
	},
};

/// A set of named colors.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Color {
	/// Denotes very good, close to ideal.
	Blue,
	/// Denotes good, within normal conditions.
	Green,
	/// Denotes acceptable but not good conditions. Generally safe, but outside
	/// normal limits.
	Yellow,
	/// Denotes bad or unsafe conditions, outside safe limits.
	Red,
}

impl Display for Color {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		write!(fmt, "{}", match *self {
			Color::Red => "RED",
			Color::Blue => "BLUE",
			Color::Green => "GREEN",
			Color::Yellow => "YELLOW",
		})
	}
}

impl Ord for Color {
	/// This ordering is in order of specificity, not absolute range.
	///
	/// `Blue` is the most specific, and compares `Greater` than the rest. It is
	/// bordered on both sides by `Green`, and then by `Yellow`, and `Red` has
	/// the broadest range and compares `Less` than the rest.
	fn cmp(&self, rhs: &Self) -> Ordering {
		match (*self, *rhs) {
			(Color::Red, Color::Red) |
			(Color::Blue, Color::Blue) |
			(Color::Green, Color::Green) |
			(Color::Yellow, Color::Yellow) => Ordering::Equal,

			(Color::Green, Color::Blue) => Ordering::Less,
			(Color::Green, _) => Ordering::Greater,

			(Color::Yellow, Color::Red) => Ordering::Greater,
			(Color::Yellow, _) => Ordering::Less,

			(Color::Blue, _) => Ordering::Greater,
			(Color::Red, _) => Ordering::Less,
		}
	}
}

impl Parsed for Color {
	/// Parses a color word.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		use nom::{
			Err::Error,
			ErrorKind::Custom,
		};
		let (rem, name) = word(text)?;
		Ok((rem, match &name as &str {
			"RED" | "Red" | "red" => Color::Red,
			"BLUE" | "Blue" | "blue" => Color::Blue,
			"GREEN" | "Green" | "green" => Color::Green,
			"YELLOW" | "Yellow" | "yellow" => Color::Yellow,
			_ => {
				if !name.is_empty() {
					error!("Invalid color name: {}", name);
				}
				return Err(Error(error_position!(text, Custom('c' as u32))));
			},
		}))
	}
}

impl PartialOrd for Color {
	fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
		Some(Ord::cmp(self, rhs))
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn compare() {
		use self::Color::*;
		assert!(Blue > Green);
		assert!(Green > Yellow);
		assert!(Yellow > Red);
	}

	#[test]
	fn parse() {
		let text = "BLUE GREEN YELLOW RED";
		let (rem, b) = Color::parse(Cs(text)).unwrap();
		let (rem, g) = Color::parse(rem).unwrap();
		let (rem, y) = Color::parse(rem).unwrap();
		let (rem, r) = Color::parse(rem).unwrap();

		assert_matches!(b, Color::Blue);
		assert_matches!(g, Color::Green);
		assert_matches!(y, Color::Yellow);
		assert_matches!(r, Color::Red);
		assert!(rem.is_empty());
	}

	#[test]
	fn render() {
		let text = "BLUE GREEN YELLOW RED";
		let render = format!("{} {} {} {}", Color::Blue, Color::Green, Color::Yellow, Color::Red);
		assert_eq!(text, render);
	}
}
