/*! Hazard Warnings

A hazard warning may be attached to an entire Command block, or to one state of
a command's parameter, to indicate that using this item would be in some way
dangerous or worthy of extra thought.

Hazard warnings may include text to explain the danger.
!*/

use super::{
	ParseResult,
	parse_utils::*,
	traits::{
		Described,
		Keyword,
		Parsed,
	},
};
use nom::types::CompleteStr;
use std::{
	convert::From,
	fmt::{
		self,
		Display,
		Formatter,
	},
};
use tap::TapResultOps;

/// A marker that indicates a certain item may warrant special attention.
///
/// This can be applied to command blocks or to parameter states.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Hazard {
	/// A description of the hazard. Can be empty.
	pub text: Option<String>,
}

impl Described for Hazard {
	#[cfg_attr(feature = "trace", flame)]
	fn description(&self) -> Option<&str> {
		self.text.as_ref().map(|s| s as &str)
	}

	#[cfg_attr(feature = "trace", flame)]
	fn describe(&mut self, description: String) {
		self.text = Some(description);
	}
}

impl Display for Hazard {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut s = String::with_capacity(16);
		write!(s, "{}", <Self as Keyword>::KEYWORD)?;
		if let Some(ref t) = self.text {
			write!(s, " {:?}", t)?;
		}
		write!(f, "{}", s)
	}
}

impl<T: Into<String>> From<T> for Hazard {
	#[cfg_attr(feature = "trace", flame)]
	fn from(s: T) -> Self {
		Self {
			text: Some(s.into()),
		}
	}
}

impl Keyword for Hazard {
	const KEYWORD: &'static str = "HAZARDOUS";
}

impl Parsed for Hazard {
	/// Parse a hazard warning.
	///
	/// The warning consists of the keyword `HAZARDOUS`, which may be followed
	/// by an optional double-quoted string.
	///
	/// There is a bug in the COSMOS reference text: the spec mandates the word
	/// `HAZARDOUS`, but the example files use `HAZARD`. Therefore, the `HAZARD`
	/// keyword is accepted, but will emit a warning when encountered.
	///
	/// The implementation of [`Display`] emits only `HAZARDOUS`.
	///
	/// [`Display`]: #impl-Display
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		<Self as Keyword>::seek(text)
			//  Note: The HAZARD keyword appears to be a bug: the COSMOS
			//  documentation *explicitly* states that only HAZARDOUS is a valid
			//  keyword, yet their reference files include HAZARD as a keyword.
			.or_else(|_| {
				ws!(text, tag!("HAZARD")).tap_ok(|_| {
					warn!("`HAZARD` is a deprecated keyword; use `HAZARDOUS` instead");
				})
			})
			.and_then(|(rem, _)| dquote_escape(rem)
				.map(|(rem, esc)| (rem, String::from(esc)))
				.map(|(rem, h)| (rem, Self::from(h)))
				.or_else(|_| Ok((rem, Self::default())))
			)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let (_, h) = Hazard::parse(Cs("HAZARD")).unwrap();
		assert_matches!(h.description(), None);

		let (_, h) = Hazard::parse(Cs("HAZARDOUS")).unwrap();
		assert_matches!(h.description(), None);

		let (_, h) = Hazard::parse(Cs(r#"HAZARD "Because I said""#)).unwrap();
		assert_matches!(h.description(), Some("Because I said"));

		let (_, h) = Hazard::parse(Cs(r#"HAZARDOUS "Because""#)).unwrap();
		assert_matches!(h.description(), Some("Because"));
	}

	#[test]
	fn render() {
		let text = r#"
HAZARDOUS "Some warning"
HAZARDOUS
HAZARD "Some warning"
HAZARD
		"#.trim();

		let (rem, h1) = Hazard::parse(Cs(text)).unwrap();
		let (rem, h2) = Hazard::parse(rem).unwrap();
		let (rem, h3) = Hazard::parse(rem).unwrap();
		let (rem, h4) = Hazard::parse(rem).unwrap();

		assert_eq!(format!("{}", h1), &text[.. 24]);
		assert_eq!(format!("{}", h2), &text[.. 9]);
		assert_eq!(format!("{}", h3), &text[.. 24]);
		assert_eq!(format!("{}", h4), &text[.. 9]);
		assert!(rem.is_empty());
	}
}
