/*! Polynomial Transformation Functions

This decorator describes a polynomial transform that is applied to data values
as they cross the binary-packet serialization boundary. It consists of two
keywords, `POLY_READ_CONVERSION` and `POLY_WRITE_CONVERSION`, which both take a
list of numerical coefficients.

As a data value is serialized or deserialized, the following function is applied
to it:

<blockquote><math displaystyle="true"><semantics><mrow>
	<mi>y</mi> <mo>=</mo>
	<munderover>
		<mo>∑</mo><mi>𝑛=0</mi><mi>𝑘 - 1</mi>
	</munderover>
	<mi>𝑐𝑜𝑒𝑓𝑓𝑠</mi><mo>[</mo><mi>𝑛</mi><mo>]</mo>
	<mo>×</mo>
	<msup><mi>x</mi><mn>𝑛</mn></msup>
	<mspace />
	<mo>|</mo>
	<mspace />
	<mi>𝑘</mi><mo>=</mo><mi>𝑙𝑒𝑛</mi><mo>(</mo><mi>𝑐𝑜𝑒𝑓𝑓𝑠</mi><mo>)</mo>
</mrow></semantics></math></blockquote>

A `POLY_READ_CONVERSION` filter transforms deserialized data from a
`TELEMETRY ITEM` and places the output value into the client's memory. A
`POLY_WRITE_CONVERSION` filter transforms a value in a client's memory and
places the output value into a packet during serialization.

<style type="text/css">
math { font-size: 1.2em; }
</style>
!*/

use super::{
	ParseResult,
	parse_utils::fnum,
	traits::Keyword,
};
use nom::types::CompleteStr;
use std::{
	cmp::{
		Ordering,
		PartialEq,
		PartialOrd,
	},
	convert::From,
	default::Default,
	fmt::{
		self,
		Display,
		Formatter,
	},
	ops::{
		Deref,
		Index,
		IndexMut,
	},
};

/// A mathematical transformation to be applied to certain parameters.
///
/// Contains a vector of one or more floats which are the polynomial
/// coefficients.
///
/// The polynomial equation is:
///
/// <blockquote><math displaystyle="true"><semantics><mrow>
///   <mi>y</mi> <mo>=</mo>
///   <munderover>
///     <mo>∑</mo><mi>𝑛=0</mi><mi>𝑘 - 1</mi>
///   </munderover>
///   <mi>𝑐𝑜𝑒𝑓𝑓𝑠</mi><mo>[</mo><mi>𝑛</mi><mo>]</mo>
///   <mo>×</mo>
///   <msup><mi>x</mi><mn>𝑛</mn></msup>
///   <mo>|</mo>
///   <mi>𝑘</mi><mo>=</mo><mi>𝑙𝑒𝑛</mi><mo>(</mo><mi>𝑐𝑜𝑒𝑓𝑓𝑠</mi><mo>)</mo>
/// </mrow></semantics></math></blockquote>
/// <style type="text/css">
/// math { font-size: 1.2em; }
/// </style>
#[derive(Clone, Debug)]
#[cfg_attr(feature = "cargo-clippy", allow(stutter))]
pub struct PolyConversion<T> {
	/// The list of polynomial coefficients.
	pub coeffs: Vec<f64>,
	/// Get the compiler to not complain that I'm using a type variable on a
	/// struct that doesn't differentiate itself by type.
	_marker: ::std::marker::PhantomData<T>,
}

impl<T: Keyword> PolyConversion<T> {
	/// The polynomial conversion keywords are `POLY_*_CONVERSION`.
	const SUFFIX: &'static str = "_CONVERSION";

	/// Parses a polynomial conversion from a stream of COSMOS text.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		do_parse!(CompleteStr::from(text.trim_left()),
			tag!(&Self::tag() as &str) >>
			coeffs: many1!(ws!(fnum)) >>
			(Self::from(coeffs))
		)
	}

	/// Use the type parameter's keyword to get the actual middle word.
	#[cfg_attr(feature = "trace", flame)]
	fn mid() -> &'static str {
		let kw = T::KEYWORD;
		match kw {
			"COMMAND" | "PARAMETER" => "WRITE",
			"TELEMETRY" | "ITEM" => "READ",
			_ => panic!("`PolyConversion` is not implemented for {}", kw),
		}
	}

	/// Render the fully-formed keyword.
	#[cfg_attr(feature = "trace", flame)]
	fn tag() -> String {
		format!("{}{}{}", <Self as Keyword>::KEYWORD, Self::mid(), Self::SUFFIX)
	}

	/// Render the coefficients without their enclosing brackets.
	#[cfg_attr(feature = "trace", flame)]
	fn display_coeffs(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(self.len() * 4);
		for coeff in self.iter() {
			write!(out, " {:?}", coeff)?;
		}
		write!(fmt, "{}", out)
	}
}

impl<T> Default for PolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn default() -> Self {
		Self::from(&[1.0] as &[f64])
	}
}

impl<T> Deref for PolyConversion<T> {
	type Target = [f64];

	#[cfg_attr(feature = "trace", flame)]
	fn deref(&self) -> &Self::Target {
		&self.coeffs
	}
}

impl<T: Keyword> Display for PolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "{}", Self::tag())?;
		self.display_coeffs(f)
	}
}

impl<'a, T> From<&'a [f64]> for PolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn from(coeffs: &[f64]) -> Self {
		if coeffs.len() < 1 {
			return Self::from(vec![1.0]);
		}
		Self::from(Vec::from(coeffs))
	}
}

impl<T> From<Vec<f64>> for PolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn from(coeffs: Vec<f64>) -> Self {
		Self {
			coeffs: if coeffs.len() < 1 {
				vec![1.0]
			}
			else {
				coeffs
			},
			_marker: ::std::marker::PhantomData::<T>::default(),
		}
	}
}

impl<T> Index<usize> for PolyConversion<T> {
	type Output = f64;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: usize) -> &Self::Output {
		&self.coeffs[idx]
	}
}

impl<T> IndexMut<usize> for PolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
		&mut self.coeffs[idx]
	}
}

impl<T> Keyword for PolyConversion<T> {
	const KEYWORD: &'static str = "POLY_";
}

impl<T> PartialEq for PolyConversion<T> {
	fn eq(&self, rhs: &Self) -> bool {
		self.coeffs.len() == rhs.coeffs.len()
	}
}

impl<T> PartialOrd for PolyConversion<T> {
	/// This compares the polynomial order of two conversions. It does not
	/// compare the behavior of the curves.
	fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
		self.coeffs.len().partial_cmp(&rhs.coeffs.len())
	}
}

/// A segmented polynomial conversion transform.
///
/// This is a collection of `PolyConversion` transforms that have finite,
/// segmented domains.
///
/// Each segment has a lower bound, and is implicitly terminated at the higher
/// bound by the lower bound of another segment, or positive infinity if no
/// other segment has a lower bound greater than it. These bounds are used to
/// determine which segment is applied for transformation, and then the
/// transformation occurs as described for `PolyConversion`.
#[derive(Clone, Debug)]
#[cfg_attr(feature = "cargo-clippy", allow(stutter))]
pub struct SegmentedPolyConversion<T> {
	/// The lower bound of the domain to which this segment applies.
	///
	/// When this value is -∞, then the instance is equivalent to an unadorned
	/// `PolyConversion`, and displays as such.
	pub lower_bound: f64,
	/// The coefficients of the polynomial transformation.
	pub conversion: PolyConversion<T>,
}

impl<T: Keyword> SegmentedPolyConversion<T> {
	/// Parses a segmented polynomial conversion item.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		do_parse!(text,
			call!(<Self as Keyword>::seek) >>
			tag!(&PolyConversion::<T>::tag() as &str) >>
			lower_bound: fnum >>
			coeffs: many1!(ws!(fnum)) >>
			(Self::from((lower_bound, coeffs)))
		)
	}
}

impl<T> Deref for SegmentedPolyConversion<T> {
	type Target = PolyConversion<T>;
	#[cfg_attr(feature = "trace", flame)]
	fn deref(&self) -> &Self::Target {
		&self.conversion
	}
}

impl<T: Keyword> Display for SegmentedPolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		//  A segment beginning at -∞ is equivalent to an unsegmented conversion
		if self.lower_bound == ::std::f64::NEG_INFINITY {
			return write!(fmt, "{}", self.conversion);
		}
		write!(fmt, "{}{} {:?}",
			<Self as Keyword>::KEYWORD,
			PolyConversion::<T>::tag(),
			self.lower_bound
		)?;
		self.conversion.display_coeffs(fmt)
	}
}

impl<T> From<(f64, PolyConversion<T>)> for SegmentedPolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn from(t: (f64, PolyConversion<T>)) -> Self {
		Self {
			lower_bound: t.0,
			conversion: t.1,
		}
	}
}

impl<'a, T> From<(f64, &'a [f64])> for SegmentedPolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn from(t: (f64, &'a [f64])) -> Self {
		Self::from((t.0, PolyConversion::<T>::from(t.1)))
	}
}

impl<T> From<(f64, Vec<f64>)> for SegmentedPolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn from(t: (f64, Vec<f64>)) -> Self {
		Self::from((t.0, PolyConversion::<T>::from(t.1)))
	}
}

impl<T> From<PolyConversion<T>> for SegmentedPolyConversion<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn from(p: PolyConversion<T>) -> Self {
		Self::from((::std::f64::NEG_INFINITY, p))
	}
}

impl<T> Keyword for SegmentedPolyConversion<T> {
	const KEYWORD: &'static str = "SEG_";
}

#[cfg(test)]
mod tests {
	use super::*;
	use command::Command;
	use telemetry::Telemetry;
	use nom::types::CompleteStr as Cs;

	fn text() -> &'static str {
		r#"
POLY_WRITE_CONVERSION 1.0 0.5 4.0
POLY_READ_CONVERSION 1.0 2.0 0.25
SEG_POLY_WRITE_CONVERSION 10.0 2.0
SEG_POLY_READ_CONVERSION 10.0 0.5
		"#.trim()
	}

	#[test]
	fn parse() {
		let (rem, pwc) = PolyConversion::<Command>::parse(Cs(text())).unwrap();
		assert_eq!(&pwc as &[f64], &[1.0, 0.5, 4.0]);
		let (rem, prc) = PolyConversion::<Telemetry>::parse(rem).unwrap();
		assert_eq!(&prc as &[f64], &[1.0, 2.0, 0.25]);
		let (rem, spwc) = SegmentedPolyConversion::<Command>::parse(rem).unwrap();
		assert_eq!(&spwc as &[f64], &[2.0]);
		let (rem, sprc) = SegmentedPolyConversion::<Telemetry>::parse(rem).unwrap();
		assert_eq!(&sprc as &[f64], &[0.5]);

		assert!(rem.is_empty());
	}

	#[test]
	fn render() {
		use std::fmt::Write;
		let (rem, pwc) = PolyConversion::<Command>::parse(Cs(text())).unwrap();
		let (rem, prc) = PolyConversion::<Telemetry>::parse(rem).unwrap();
		let (rem, spwc) = SegmentedPolyConversion::<Command>::parse(rem).unwrap();
		let (_, sprc) = SegmentedPolyConversion::<Telemetry>::parse(rem).unwrap();

		let mut rendered = String::new();
		writeln!(rendered, "{}", pwc).unwrap();
		writeln!(rendered, "{}", prc).unwrap();
		writeln!(rendered, "{}", spwc).unwrap();
		writeln!(rendered, "{}", sprc).unwrap();
		assert_eq!(text(), rendered.trim());
	}

	#[test]
	fn normalize() {
		let text = "SEG_POLY_READ_CONVERSION -inf 10.0 0.5";

		let (_, sprc) = SegmentedPolyConversion::<Telemetry>::parse(Cs(text)).unwrap();
		let rendered = format!("{}", sprc);
		assert_eq!(rendered, "POLY_READ_CONVERSION 10.0 0.5");
	}
}
