/*! Miscellaneous Keywords

These keywords set flags on their parent when present.
!*/

use super::{
	ParseResult,
	kind::{
		Kind,
		KindVal,
	},
	parse_utils::{
		dquote_escape,
		first_line_only,
	},
	traits::{
		Described,
		Keyword,
		Parsed,
	},
};
use nom::types::CompleteStr;
use std::{
	convert::From,
	fmt::{
		self,
		Display,
		Formatter,
	},
	ops::{
		Deref,
		DerefMut,
	},
};

//  The bare keywords all have the exact same implementation: a newtype wrapper
//  over bool, that derefs to bool for use in conditionals, and renders as its
//  keyword or as nothing.
macro_rules! bareword {
	( $( ($name:ident, $keyword:expr, $doc:expr) ),+ ) => { $(
#[doc = $doc]
//  `bool` defaults to `false`.
#[derive(Clone, Debug, Default)]
pub struct $name(pub bool);

impl Deref for $name {
	type Target = bool;

	#[cfg_attr(feature = "trace", flame)]
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl Display for $name {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		if **self {
			write!(fmt, "{}", <Self as Keyword>::KEYWORD)?;
		}
		Ok(())
	}
}

impl From<bool> for $name {
	#[cfg_attr(feature = "trace", flame)]
	fn from(b: bool) -> $name {
		$name(b)
	}
}

impl Keyword for $name {
	const KEYWORD: &'static str = $keyword;
}

impl Parsed for $name {
	#[doc = "Parses the `"]
	#[doc = $keyword]
	#[doc = "` keyword, if present."]
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		map!(text, <Self as Keyword>::seek, |_| Self::from(true))
	}
}
	)+ };
	( $( ($name:ident, $keyword:expr, $doc:expr), )+ ) => {
		bareword!($(($name, $keyword, $doc)),+);
	};
}

//  These keywords all take one subsequent argument.
macro_rules! kindword {
	( $( ($name:ident, $keyword:expr, $doc:expr) ),+ ) => { $(
#[doc = $doc]
#[derive(Clone, Debug, Default)]
pub struct $name(pub KindVal);

impl $name {
	/// Unwraps the inner `KindVal` instance.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap(self) -> KindVal {
		self.0
	}

	/// Casts the inner value to a new kind.
	#[cfg_attr(feature = "trace", flame)]
	pub fn cast(self, into: Kind) -> Self {
		Self::from(self.unwrap().cast(into))
	}
}

impl Deref for $name {
	type Target = KindVal;

	#[cfg_attr(feature = "trace", flame)]
	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for $name {
	#[cfg_attr(feature = "trace", flame)]
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

impl Display for $name {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		write!(fmt, "{} {}", <Self as Keyword>::KEYWORD, **self)
	}
}

impl From<KindVal> for $name {
	#[cfg_attr(feature = "trace", flame)]
	fn from(v: KindVal) -> Self {
		$name(v)
	}
}

impl Keyword for $name {
	const KEYWORD: &'static str = $keyword;
}

impl Parsed for $name {
	#[doc = "Parses the `"]
	#[doc = $keyword]
	#[doc = "` keyword and value, if present."]
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		first_line_only(text, |line| do_parse!(line,
			call!(<Self as Keyword>::seek) >>
			v: call!(KindVal::parse) >>
			(Self::from(v))
		))
	}
}
	)+ };
	( $( ($name:ident, $keyword:expr, $doc:expr), )+ ) => {
		kindword!($(($name, $keyword, $doc)),+);
	};
}

bareword! {
	(AllowShort, "ALLOW_SHORT",
	"A flag that permits signature-matched telemetry messages that are shorter \
	than their defined length to be accepted. Missing bits are zero-filled."),
	(DisableMessages, "DISABLE_MESSAGES",
	"A flag that quiets its holder in client display, but does not prevent its \
	use or logging."),
	(Disabled, "DISABLED",
	"A flag that sets its holder to be disabled from client use."),
	(Hidden, "HIDDEN",
	"A flag that sets its holder to be hidden from client use."),
	(Required, "REQUIRED",
	"A flag that makes its holding parameter required to be explicitly set in \
	client programs.

When this keyword is added to a parameter, that parameter must be explicitly \
	built in client programs or scripts, and cannot be sent in its default, \
	implicit form."),
}

kindword! {
	(Minimum, "MINIMUM_VALUE", "Overrides a `Parameter`'s minimum value."),
	(Maximum, "MAXIMUM_VALUE", "Overrides a `Parameter`'s maximum value."),
	(Default, "DEFAULT_VALUE", "Overrides a `Parameter`'s default value."),
}

/// A human-readable description.
///
/// Given a set of tokens `DESCRIPTION "$text"`, this holds the value `$text`.
#[derive(Clone, Debug, Default)]
pub struct Description {
	/// The description text.
	pub text: String,
}

impl Description {
	/// Parses a `DESCRIPTION "$text"` line.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		first_line_only(text, |line| do_parse!(line,
			call!(<Self as Keyword>::seek) >>
			text: map!(dquote_escape, String::from) >>
			(Self {
				text,
			})
		))
	}

	/// Retrieves the inner text.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap(self) -> String {
		self.text
	}
}

impl Described for Description {
	fn describe(&mut self, text: String) {
		self.text = text;
	}

	fn description(&self) -> Option<&str> {
		Some(&self.text)
	}
}

impl Display for Description {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		write!(fmt, "{} {:?}", <Self as Keyword>::KEYWORD, self.text)
	}
}

impl Keyword for Description {
	const KEYWORD: &'static str = "DESCRIPTION";
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let text = r#"
DISABLE_MESSAGES
DISABLED
HIDDEN
REQUIRED

MINIMUM_VALUE -10
MAXIMUM_VALUE 10
DEFAULT_VALUE 0.0

DESCRIPTION "Sample text"
		"#;

		let (rem, dm) = DisableMessages::parse(Cs(text)).unwrap();
		assert_matches!(dm, DisableMessages(true));
		let (rem, d) = Disabled::parse(rem).unwrap();
		assert_matches!(d, Disabled(true));
		let (rem, h) = Hidden::parse(rem).unwrap();
		assert_matches!(h, Hidden(true));
		let (rem, r) = Required::parse(rem).unwrap();
		assert_matches!(r, Required(true));

		let (rem, min) = Minimum::parse(rem).unwrap();
		let (rem, max) = Maximum::parse(rem).unwrap();
		let (rem, def) = Default::parse(rem).unwrap();

		let (rem, desc) = Description::parse(rem).unwrap();

		assert!(rem.trim().is_empty());

		let mut rendered = String::with_capacity(text.len());

		use std::fmt::Write;

		writeln!(rendered, "{}", dm).unwrap();
		writeln!(rendered, "{}", d).unwrap();
		writeln!(rendered, "{}", h).unwrap();
		writeln!(rendered, "{}", r).unwrap();
		writeln!(rendered).unwrap();
		writeln!(rendered, "{}", min).unwrap();
		writeln!(rendered, "{}", max).unwrap();
		writeln!(rendered, "{}", def).unwrap();
		writeln!(rendered).unwrap();
		writeln!(rendered, "{}", desc).unwrap();

		assert_eq!(text.trim(), rendered.trim());

		assert_eq!(min.unwrap().unwrap_int(), -10_i64);
		assert_eq!(max.unwrap().unwrap_int(), 10_i64);
		assert!((def.unwrap().unwrap_float() - 0.0).abs() < ::std::f64::EPSILON);
	}
}
