#![doc(include = "../doc/cosmos/units.md")]

//  I'm not documenting the macro-generated constants; that's annoyingly hard.
#![allow(missing_docs)]

use super::{
	ParseResult,
	parse_utils::word,
	traits::{
		Keyword,
		Named,
	},
};
use nom::types::CompleteStr;
use std::{
	borrow::Cow,
	fmt::{
		self,
		Display,
		Formatter,
	},
};

/// A measurement unit contextualizing a field number.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Unit {
	/// The unit name.
	pub name: Cow<'static, str>,
	/// The unit abbreviation or symbol.
	pub abbrev: Cow<'static, str>,
}

impl Unit {
	/// Parse a `UNITS` group.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		do_parse!(text,
			call!(<Self as Keyword>::seek) >>
			name: map!(word, |w| w.to_string()) >>
			abbrev: map!(word, |w| w.to_string()) >>
			(Self {
				name: Cow::Owned(name),
				abbrev: Cow::Owned(abbrev),
			})
		)
	}
}

impl Display for Unit {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "{} {} {}", Self::KEYWORD, self.name, self.abbrev)
	}
}

impl Keyword for Unit {
	const KEYWORD: &'static str = "UNITS";
}

impl Named for Unit {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name
	}
}

//  It'd be really cool if an immutable String could be constructed that
//  pointed at &'static str, so I don't have to do this Cow nonsense or have to
//  use lazy_static
macro_rules! unit {
	($name:ident, $n:expr, $a:expr) => {
		pub const $name : Unit = Unit {
			name: Cow::Borrowed($n),
			abbrev: Cow::Borrowed($a),
		};
	};
}

// Time
unit!(NANOSECOND, "Nanosecond", "ns");
unit!(MICROSECOND, "Microsecond", "us");
unit!(MILLISECOND, "Millisecond", "ms");
unit!(SECOND, "Second", "s");
unit!(MINUTE, "Minute", "min");
unit!(HOUR, "Hour", "hr");
unit!(DAY, "Day", "day");
unit!(WEEK, "Week", "wk");
unit!(YEAR, "Year", "yr");

// Distance
unit!(MICROMETER, "Micrometer", "um");
unit!(MILLIMETER, "Millimeter", "mm");
unit!(CENTIMETER, "Centimeter", "cm");
unit!(METER, "Meter", "m");
unit!(KILOMETER, "Kilometer", "km");
unit!(MEGAMETER, "Megameter", "Mm");
unit!(GIGAMETER, "Gigameter", "Gm");

// Temperature
unit!(CELSIUS, "Degree Celsius", "C");
unit!(FAHRENHEIT, "Degree Fahrenheit", "F");
unit!(KELVIN, "Kelvin", "K");
unit!(RANKINE, "Degree Rankine", "R");

// Frequency
unit!(NANOHERTZ, "Nanohertz", "nHz");
unit!(MICRHERTZ, "Microhertz", "uHz");
unit!(MILLIHERTZ, "Millihertz", "mHz");
unit!(HERTZ, "Hertz", "Hz");
unit!(KILOHERTZ, "Kilohertz", "kHz");
unit!(MEGAHERTZ, "Megahertz", "MHz");
unit!(GIGAHERTZ, "Gigahertz", "GHz");

// Information
unit!(BIT, "Bit", "b");
unit!(BYTE, "Byte", "B");
unit!(KILOBIT, "Kilobit", "kb");
unit!(KIBIBIT, "Kibibit", "Kib");
unit!(KILOBYTE, "Kilobyte", "kB");
unit!(KIBIBYTE, "Kibibyte", "KiB");
unit!(MEGABIT, "Megabit", "Mb");
unit!(MEBIBIT, "Mebibit", "Mib");
unit!(MEGABYTE, "Megabyte", "MB");
unit!(MEBIBYTE, "Mebibyte", "MiB");
unit!(GIGABIT, "Gigabit", "Gb");
unit!(GIBIBIT, "Gibibit", "Gib");
unit!(GIGABYTE, "Gigabyte", "GB");
unit!(GIBIBYTE, "Gibibyte", "GiB");

// Data rate
unit!(BAUD, "Baud", "Bd");
unit!(KILOBAUD, "Kilobaud", "kBd");
unit!(MEGABAUD, "Megabaud", "MBd");
unit!(GIGABAUD, "Gigabaud", "GBd");

// Power
unit!(DECIBEL, "Decibel", "dB");
unit!(DECIBEL_M, "Decibel-milliwatt", "dBm");
unit!(BEL, "Bel", "B");
unit!(BEL_W, "Bel-Watt", "BW");

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		match Unit::parse(Cs("UNITS Celsius C")) {
			Ok((_, Unit {
				name: n,
				abbrev: a,
			})) => {
				assert_eq!(n, "Celsius");
				assert_eq!(a, "C");
			},
			e => panic!("{:#?}", e),
		}
	}

	#[test]
	fn render() {
		let text = "UNITS Celsius C";
		assert_eq!(text, format!("{}", Unit::parse(Cs(text)).unwrap().1));
	}
}
