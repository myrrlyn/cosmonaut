/*! Meta Information

The `META` keyword is used to embed information on an element that is not
present in the binary message, but may be read and used by the client working
with the data structures.
!*/

use super::{
	ParseResult,
	kind::KindVal,
	parse_utils::{
		first_line_only,
		word,
	},
	traits::{
		Keyword,
		Named,
		Parsed,
	},
};
use nom::types::CompleteStr;
use std::{
	fmt::{
		self,
		Display,
		Formatter,
	},
	iter::IntoIterator,
	ops::{
		Deref,
		DerefMut,
		Index,
	},
};
use string_cache::DefaultAtom as Atom;

/// Meta information for an element.
#[derive(Clone, Debug, Default)]
pub struct Meta {
	/// The meta value's name.
	pub name: Atom,
	/// The meta value's contents.
	pub values: Vec<KindVal>,
}

/// Set of named meta information.
#[derive(Clone, Debug, Default)]
pub struct MetaSet(pub Vec<Meta>);

impl Deref for Meta {
	type Target = [KindVal];

	fn deref(&self) -> &Self::Target {
		&self.values
	}
}

impl Display for Meta {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(<Self as Keyword>::KEYWORD.len() + self.name().len() + 1);
		write!(out, "{} {}", <Self as Keyword>::KEYWORD, self.name())?;
		for v in self {
			write!(out, " {}", v)?;
		}
		write!(fmt, "{}", out)
	}
}

impl<'a> IntoIterator for &'a Meta {
	type Item = &'a KindVal;
	type IntoIter = <&'a Vec<KindVal> as IntoIterator>::IntoIter;

	fn into_iter(self) -> Self::IntoIter {
		(&self.values).into_iter()
	}
}

impl Keyword for Meta {
	const KEYWORD: &'static str = "META";
}

impl Named for Meta {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name
	}
}

impl Parsed for Meta {
	/// Parses a `META NAME [values]+` line.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		first_line_only(text, |line| do_parse!(line,
			call!(<Self as Keyword>::seek) >>
			name: map!(word, |w| Atom::from(&w as &str)) >>
			values: many1!(KindVal::parse) >>
			(Self {
				name,
				values,
			})
		))
	}
}

impl MetaSet {
	/// Inserts a new Meta attribute.
	///
	/// MetaSet is sorted, so this function finds the correct slot by binary
	/// search, which is O(log(n)), before inserting and shifting elements after
	/// it.
	pub fn insert(&mut self, m: Meta) {
		match self.find_slot(m.name()) {
			//  If a Meta with this name is already present, replace it
			Ok(slot) => self.0[slot] = m,
			//  Else, insert it
			Err(slot) => self.0.insert(slot, m),
		};
	}

	/// Adds the contents of another set to self.
	///
	/// Rather than search and extend each time, this function uses Vec’s extend
	/// method and then sorts afterwards.
	pub fn extend(&mut self, other: Self) {
		self.0.extend(other.0);
		self.0.sort_unstable_by_key(|m| m.name.clone());
	}

	/// Attempts to access a Meta by name.
	pub fn get(&self, name: &str) -> Option<&Meta> {
		self.find_slot(name).ok().and_then(|i| self.0.get(i))
	}

	/// Finds the slot for a Meta item with the given name.
	pub fn find_slot(&self, s: &str) -> Result<usize, usize> {
		let a = Atom::from(s);
		self.0.binary_search_by_key(&a, |m| m.name.clone())
	}
}

impl Deref for MetaSet {
	type Target = [Meta];

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for MetaSet {
	fn deref_mut(&mut self) -> &mut <Self as Deref>::Target {
		&mut self.0
	}
}

impl Display for MetaSet {
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		for m in self {
			writeln!(out, "{}", m)?;
		}
		write!(fmt, "{}", out)
	}
}

impl<'a> Index<&'a str> for MetaSet {
	type Output = Meta;

	//  The storage is always sorted, so bsearch it instead of linear scanning.
	fn index(&self, idx: &'a str) -> &Self::Output {
		self.find_slot(idx).map(|i| &self.0[i]).unwrap_or_else(|_| {
			panic!("No META attribute named {}", idx);
		})
	}
}

impl<'a> IntoIterator for &'a MetaSet {
	type Item = &'a Meta;
	type IntoIter = <&'a Vec<Meta> as IntoIterator>::IntoIter;

	fn into_iter(self) -> Self::IntoIter {
		(&self.0).into_iter()
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let text = r#"META NAME "val0" 5 10.0 "multiple words""#;
		let (_, m) = Meta::parse(Cs(text)).unwrap();
		assert_eq!(m.name(), "NAME");
		assert_eq!(m.len(), 4);

		assert_eq!(text, format!("{}", m));
	}

	#[test]
	fn parse_bare_word() {
		let (_, m) = Meta::parse(Cs("META TEST bare words")).unwrap();
		assert_eq!(m.len(), 2);
		assert_eq!(m.deref(), &["bare".into(), "words".into()]);
		assert_eq!(r#"META TEST "bare" "words""#, format!("{}", m));
	}
}
