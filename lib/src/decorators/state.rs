/*! # Parameter State

This keyword allows a map between arbitrary strings and values. States are
enumerated after the field they describe, as follows:

```cosmos
COMMAND ONE POWER LITTLE_ENDIAN "Powers the machine on or off"
	PARAMETER PWR 0 8 UINT 0 1 1 "Power state"
		STATE ON 1
		STATE OFF 0 HAZARDOUS "Waking from network is risky"
TELEMETRY ONE HEALTH BIG_ENDIAN "Health information"
	ITEM DIAGNOSTIC 0 8 UINT 0 3 1 "Diagnostic information"
		STATE OFF 0
		STATE MIN 1 Red
		STATE DEF 2 Yellow
		STATE MAX 3 Green
```

The above block adds two named states, `"ON"` and `"OFF"`, to the `PWR`
parameter. When manipulating the `POWER` packet's `PWR` parameter, it is legal
to state that the `PWR` value is either `"ON"` or `"OFF"`, in addition to `0` or
`1`. It adds four named states to the `DIAGNOSTIC` item, which enumerate the
possible values and the colors in which to render them for the client.

State names not enumerated may not be set on the field, just as integral values
outside the allowed range may not be set.
!*/

use super::{
	ParseResult,
	kind::*,
	traits::{
		Keyword,
		Named,
		Parsed,
		Stateful,
	},
};
use nom::types::CompleteStr;
use std::{
	fmt::{
		self,
		Debug,
		Display,
		Formatter,
	},
	iter::IntoIterator,
	ops::{
		Deref,
		DerefMut,
		Index,
	},
};
use string_cache::DefaultAtom as Atom;

/// A mapping of a text name to a parameter value.
///
/// This mapping may have an optional hazard warning attached to it, which must
/// be displayed to the user when the hazardous state is selected. The hazard
/// warning may optionally include text that explains the nature of the hazard.
#[derive(Clone, Debug, Default)]
pub struct State<T: Stateful + ?Sized> {
	/// The user-facing name of the value.
	pub name: Atom,
	/// The actual kind and value of the state.
	pub value: KindVal,
	/// A hazard warning. When `Some()`, the state is considered hazardous. The
	/// contained text may be empty, in which case a default warning will be
	/// shown, or may contain specific hazard details.
	pub extra: Option<T::Extra>,
}

/// Set of named states.
#[derive(Clone, Debug, Default)]
pub struct StateSet<T: Stateful + ?Sized> where T::Extra: Clone + Debug {
	/// Storage for the State values.
	pub inner: Vec<State<T>>,
}

impl<T: Stateful> State<T> where T::Extra: Parsed {
	/// Parses a single state value from a line of COSMOS text.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		use super::parse_utils::*;
		//  Wrap in a guard to ensure that it doesn’t try to parse a subsequent
		//  line as the extra decorator, as that’s valid syntax that can occur.
		first_line_only(text, |line| do_parse!(line,
			call!(<Self as Keyword>::seek) >>
			name: alt!(
				dquote_escape => { Atom::from } |
				squote => { |s: CompleteStr| Atom::from(&s as &str) } |
				word => { |w: CompleteStr| Atom::from(&w as &str) }
			) >>
			value: call!(KindVal::parse) >>
			//  The Hazard parser is sometimes indecisive.
			extra: opt!(complete!(<<T as Stateful>::Extra as Parsed>::parse)) >>
			(Self {
				name,
				value,
				extra,
			})
		))
	}

	/// Recast the inner value to a new kind.
	#[cfg_attr(feature = "trace", flame)]
	pub fn cast(mut self, into: Kind) -> Self {
		self.cast_in_place(into);
		self
	}

	/// Recast the inner value to a new kind, without moving it.
	#[cfg_attr(feature = "trace", flame)]
	pub fn cast_in_place(&mut self, into: Kind) {
		self.value.cast_in_place(into);
	}

	/// Determines if the `State` has extra information.
	pub fn has_extra(&self) -> bool {
		self.extra.is_some()
	}

	/// Gets the extra information.
	pub fn extra(&self) -> Option<&T::Extra> {
		self.extra.as_ref()
	}
}

impl<T: Stateful> Display for State<T> where T::Extra: Display {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "STATE {:?} {}", &self.name as &str, self.value)?;
		if let Some(ref m) = self.extra {
			write!(f, " {}", m)?;
		}
		Ok(())
	}
}

impl<T: Stateful> Keyword for State<T> {
	const KEYWORD: &'static str = "STATE";
}

impl<T: Stateful> Named for State<T> {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name
	}
}

impl<T: Stateful> StateSet<T> where T::Extra: Clone + Debug {
	/// Inserts a new State attribute.
	///
	/// StateSet is sorted, so this function finds the correct slot by binary
	/// search, which is O(log(n)), before inserting and shifting elements after
	/// it.
	pub fn insert(&mut self, s: State<T>) {
		match self.find_slot(s.name()) {
			//  If a State with this name is already present, replace it
			Ok(slot) => self.inner[slot] = s,
			//  Else, insert it
			Err(slot) => self.inner.insert(slot, s),
		};
	}

	/// Adds the contents of another set to self.
	///
	/// Rather than search and extend each time, this function uses Vec’s extend
	/// method and then sorts afterwards.
	pub fn extend(&mut self, other: Self) {
		self.inner.extend(other.inner);
		self.inner.sort_unstable_by_key(|s| s.name.clone());
	}

	/// Attempts to access a State by name.
	pub fn get(&self, name: &str) -> Option<&State<T>> {
		self.find_slot(name).ok().and_then(|i| self.inner.get(i))
	}

	/// Finds the slot for a State with the given name.
	pub fn find_slot(&self, s: &str) -> Result<usize, usize> {
		let a = Atom::from(s);
		self.inner.binary_search_by_key(&a, |s| s.name.clone())
	}
}

impl<T: Stateful> Deref for StateSet<T> where T::Extra: Clone + Debug {
	type Target = [State<T>];

	fn deref(&self) -> &Self::Target {
		&self.inner
	}
}

impl<T: Stateful> DerefMut for StateSet<T> where T::Extra: Clone + Debug {
	fn deref_mut(&mut self) -> &mut <Self as Deref>::Target {
		&mut self.inner
	}
}

impl<T: Stateful> Display for StateSet<T>
where T::Extra: Clone + Debug  + Display {
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		for s in self {
			writeln!(out, "{}", s)?;
		}
		write!(fmt, "{}", out)
	}
}

impl<T: Stateful> Index<usize> for StateSet<T> where T::Extra: Clone + Debug {
	type Output = State<T>;

	fn index(&self, idx: usize) -> &Self::Output {
		&self.inner[idx]
	}
}

impl<'a, T: Stateful> Index<&'a str> for StateSet<T>
where T::Extra: Clone + Debug {
	type Output = State<T>;

	//  Because we know that the storage is always sorted, bsearch it instead of
	//  doing a linear scan.
	fn index(&self, idx: &'a str) -> &Self::Output {
		self.find_slot(idx).map(|i| &self.inner[i]).unwrap_or_else(|_| {
			panic!("No STATE attribute named {}", idx);
		})
	}
}

impl<'a, T: Stateful> IntoIterator for &'a StateSet<T>
where T::Extra: Clone + Debug {
	type Item = &'a State<T>;
	type IntoIter = <&'a Vec<State<T>> as IntoIterator>::IntoIter;

	fn into_iter(self) -> Self::IntoIter {
		(&self.inner).into_iter()
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use parameter::Parameter;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		match State::<Parameter>::parse(Cs("STATE FALSE 0")) {
			Ok((_, State {
				name: n,
				value: KindVal::Int(0),
				extra: None,
			})) => {
				assert_eq!(&n as &str, "FALSE");
			},
			e => panic!("{:#?}", e),
		}

		match State::<Parameter>::parse(Cs(r#"STATE "Complex name" "Complex value""#)) {
			Ok((_, State {
				name: n,
				value: KindVal::Text(v),
				extra: None,
			})) => {
				assert_eq!(&n as &str, "Complex name");
				assert_eq!(v, "Complex value");
			},
			e => panic!("{:#?}", e),
		}

		match State::<Parameter>::parse(Cs(r#"STATE DANGER "Will Robinson" HAZARDOUS "Space is scary""#)) {
			Ok((_, State {
				name: n,
				value: KindVal::Text(v),
				extra: Some(h),
			})) => {
				assert_eq!(&n as &str, "DANGER");
				assert_eq!(v, "Will Robinson");
				assert_eq!(h.text.unwrap(), "Space is scary");
			},
			e => panic!("{:#?}", e),
		}

		match State::<Parameter>::parse(Cs("STATE CRYPTIC 5.00 HAZARDOUS")) {
			Ok((_, State {
				name: n,
				value: KindVal::Float(f),
				extra: Some(m),
			})) => {
				assert_eq!(&n as &str, "CRYPTIC");
				assert!((f - 5.00).abs() < ::std::f64::EPSILON);
				assert!(m.text.is_none());
			}
			e => panic!("{:#?}", e),
		}
	}
}
