/*! Parse utility functions

These parsers do not wrap any specific semantic elements, and are simply utility
components.

- `squote`, `dquote`, `escaped`, and `dquote_escape` are used for working with
    strings. `squote` extracts single-quote-delimited sequences, `dquote`
    extracts double-quote-delimited sequences that may have escape codes in them
    (including for double quotes), `escaped` transforms escape codes into the
    correct characters, and `dquote_escape` pipes the former into the latter.
- `inum`, `unum`, `fnum`, and `hnum` parse into numbers. `inum` parses a signed
    integer, `unum` parses an unsigned integer, `fnum` parses floating-point
    numbers, and `hnum` parses hexadecimal numbers. `unum` accepts `hnum` text
    as well.
- `word` simply breaks the first whitespace-separated item off the input.
- `block` parses long runs of bytes in hexadecimal notation.
- `get_line` breaks an input text in two at the first `\n` newline.
- `strip_leading_newlines` removes all `\n` or `\r\n` from the head of a string.
- `first_line_only` breaks input text using `get_line` and passes the first line
    to a provided function. It uses `strip_leading_newlins` before doing so to
    ensure that newlines are properly consumed when advancing the parse.
- `comment` is currently unused, as the COSMOS spec appears to not support them.
    It selects lines that match `/^#\s?(.*)$/`.
!*/

use super::ParseResult;
use nom::{
	Err::Error,
	ErrorKind::Custom,
	types::CompleteStr,
};
use std::borrow::Cow;

/// Parses a single text span.
///
/// This attempts, in order, to find a double-quoted string, a single-quoted
/// string, or a bare word. It does not perform any analysis other than pattern
/// match attempts.
#[cfg_attr(feature = "trace", flame)]
pub fn span(text: CompleteStr) -> ParseResult<CompleteStr> {
	alt!(CompleteStr::from(text.trim_left()),
		dquote |
		squote |
		word
	)
}

/// Parses a single word, separated by whitespace.
///
/// This parser makes no attempt to discern semantic meaning. This means that
/// the string `"hello, world"`, which is read by humans as a single unit
/// containing two words, two quotation marks, and one comma, is read by this
/// parser as two discrete words: `"hello,` and `world"`.
#[cfg_attr(feature = "trace", flame)]
//  It's not redundant, clippy.
#[cfg_attr(feature = "cargo-clippy", allow(redundant_closure))]
pub fn word(text: CompleteStr) -> ParseResult<CompleteStr> {
	let text = CompleteStr::from(text.trim_left());
	take_till1!(text, char::is_whitespace)
}

/// Parses an unsigned integral number in bases 10 or 16.
///
/// This method supports parsing keywords for the minimum and maximum values of
/// the four widths of unsigned integers: `{MIN,MAX}_UINT{8,16,32,64}`. It also
/// supports parsing hexadecimal literals of the form `/0x[0-9a-fA-F]+/`.
#[cfg_attr(feature = "trace", flame)]
#[cfg_attr(feature = "cargo-clippy", allow(cyclomatic_complexity))]
pub fn unum(text: CompleteStr) -> ParseResult<u64> {
	use std::{u8, u16, u32, u64};
	word(text).and_then(|(r, w)| w.parse::<u64>()
		//  The keywords naturally fail to parse in Rust's standard library.
		//  Therefore, if we have successfully read a word element but it's
		//  not a number Rust knows how to parse, attempt the keywords.
		.or_else(|_| alt!(w,
			tag!("MIN_UINT8") => { |_| u64::from(u8::min_value()) } |
			tag!("MAX_UINT8") => { |_| u64::from(u8::max_value()) } |
			tag!("MIN_UINT16") => { |_| u64::from(u16::min_value()) } |
			tag!("MAX_UINT16") => { |_| u64::from(u16::max_value()) } |
			tag!("MIN_UINT32") => { |_| u64::from(u32::min_value()) } |
			tag!("MAX_UINT32") => { |_| u64::from(u32::max_value()) } |
			tag!("MIN_UINT64") => { |_| u64::min_value() } |
			tag!("MAX_UINT64") => { |_| u64::max_value() } |
			//  Also try the hexadecimal literal parser
			hnum
		).map(|(_, u)| u))
		.map(|u| (r, u))
		.map_err(|_| Error(error_position!(w, Custom('u' as u32))))
	)
}

/// Parses a signed integral number in base 10.
///
/// This method supports parsing keywords for the minimum and maximum values of
/// the four widths of signed integers: `{MIN,MAX}_INT{8,16,32,64}`.
#[cfg_attr(feature = "trace", flame)]
#[cfg_attr(feature = "cargo-clippy", allow(cyclomatic_complexity))]
pub fn inum(text: CompleteStr) -> ParseResult<i64> {
	use std::{i8, i16, i32, i64};
	word(text).and_then(|(r, w)| w.parse::<i64>()
		.or_else(|_| alt!(text,
			tag!("MIN_INT8") => { |_| i64::from(i8::min_value()) } |
			tag!("MAX_INT8") => { |_| i64::from(i8::max_value()) } |
			tag!("MIN_INT16") => { |_| i64::from(i16::min_value()) } |
			tag!("MAX_INT16") => { |_| i64::from(i16::max_value()) } |
			tag!("MIN_INT32") => { |_| i64::from(i32::min_value()) } |
			tag!("MAX_INT32") => { |_| i64::from(i32::max_value()) } |
			tag!("MIN_INT64") => { |_| i64::min_value() } |
			tag!("MAX_INT64") => { |_| i64::max_value() }
		).map(|(_, i)| i))
		.map(|i| (r, i))
		.map_err(|_| Error(error_position!(r, Custom('i' as u32))))
	)
}

/// Parses a floating point number.
///
/// This is just a wrapper around Rust's floating-point parser.
#[cfg_attr(feature = "trace", flame)]
pub fn fnum(text: CompleteStr) -> ParseResult<f64> {
	word(text).and_then(|(r, w)| w.parse::<f64>()
		//  There are no named floats
		.map(|f| (r, f))
		.map_err(|_| Error(error_position!(r, Custom('f' as u32))))
	)
}

/// Parses a hexadecimal literal (`/0x[0-9a-fA-f]+/`).
#[cfg_attr(feature = "trace", flame)]
pub fn hnum(text: CompleteStr) -> ParseResult<u64> {
	preceded!(text, ws!(tag!("0x")), word).and_then(|(r, w)| {
		u64::from_str_radix(&w, 16)
			.map(|x| (r, x))
			.map_err(|_| Error(error_position!(r, Custom('x' as u32))))
	})
}

/// Parses a single-quote-delimited string.
///
/// Single-quote strings do not process escape characters, and thus cannot have
/// single quotes inside their contained text. They run from the first `'` to
/// the next `'`.
#[cfg_attr(feature = "trace", flame)]
pub fn squote(text: CompleteStr) -> ParseResult<CompleteStr> {
	let text = CompleteStr::from(text.trim_left());
	delimited!(text, tag!("'"), take_until!("'"), tag!("'"))
}

/// Parses a double-quote-delimited string.
///
/// This parser permits backslash escapes, including the sequence `\"`, to be
/// present in the input string. The parser performs no transformation on the
/// text, and returns the contents between the opening and closing `"`
/// characters as the parsed substring and the remainder of the stream AFTER the
/// closing `"` character.
///
/// If the first non-whitespace character of the stream is not `"`, then the
/// parser has been invoked in error and exits immediately.
#[cfg_attr(feature = "trace", flame)]
pub fn dquote(text: CompleteStr) -> ParseResult<CompleteStr> {
	use nom::{
		Err::{Error, Incomplete},
		ErrorKind::Custom,
		Needed::Unknown,
	};
	let text = text.trim_left();
	//  Abort if the given string is empty
	if text.is_empty() {
		return Err(Error(error_position!(text.into(), Custom('"' as u32))));
	}
	let mut iter = text.char_indices();
	//  Abort if the first character is not "
	if let Some((_, '"')) = iter.next() {
	}
	else {
		return Err(Error(error_position!(text.into(), Custom('"' as u32))));
	}
	while let Some((i, c)) = iter.next() {
		//  If the current character is `"`, then return the inner substring
		if c == '"' {
			return Ok((text[i + 1 ..].into(), text[1 ..i].into()));
		}
		//  If the current character is `\`, skip the next character.
		//  Validity of the backslash sequence is beyond the scope of this
		//  parser.
		if c == '\\' {
			iter.next();
		}
	}
	//  If control flow reaches here without having encountered an unescaped
	//  ", return Incomplete
	Err(Incomplete(Unknown))
}

/// Scans a string and transforms any escape sequences into the intended
/// characters.
///
/// This only allocates if the source contains escape sequences; a source string
/// without any is left untouched.
///
/// The escape sequences are:
///
/// - `\"` => `U+22`
/// - `\\` => `U+5C`
/// - `\a` => `U+07`
/// - `\b` => `U+08`
/// - `\f` => `U+0C`
/// - `\n` => `U+0A`
/// - `\r` => `U+0D`
/// - `\t` => `U+09`
/// - `\v` => `U+0B`
/// - `\xNN` => `U+NN`
///
/// TODO: `\uN{4...6}` => `U+N{4...6}`
#[cfg_attr(feature = "trace", flame)]
pub fn escaped(text: CompleteStr) -> ParseResult<Cow<str>> {
	use nom::{
		Err::Incomplete,
		Needed::Size,
	};
	//  Scan the string once to see if the substitution crawl can be skipped.
	//  While this is an O(n) scan, it can potentially be optimized into a fast
	//  searcher (especially when SIMD stabilizes) because it's a simple byte
	//  equality test. The unhappy path (an escape sequence is discovered) will
	//  require an O(n) scan that has complex logic at each cycle that cannot be
	//  optimized.
	if text.chars().all(|c| c != '\\') {
		//  No escapes; borrow the input again and exit.
		return Ok(("".into(), Cow::Borrowed(&text)));
	}
	//  Escapes are present; crawl the string again and swap escapes for their
	//  denoted value.
	let mut out = String::with_capacity(text.len());
	//  We cannot use a for-loop here because we require access to the iterator,
	//  and that is not named inside for-loops. We use char_indices instead of
	//  just chars in order to provide context-aware error messages in specific
	//  places (\x and \u).
	let mut iter = text.char_indices();
	while let Some((_, c)) = iter.next() {
		//  Not a backslash; push and move forward.
		if c != '\\' {
			out.push(c);
			continue;
		}
		//  Backslash! The next character(s) in the stream are escape codes.
		//  This is the part where we manually advance the iterator within the
		//  loop body; this is not doable within for-loops since the iterator is
		//  not accessible.
		out.push(match iter.next() {
			Some((_, '\\')) => '\\',
			Some((_, '"')) => '"',
			Some((_, 'n')) => '\n',
			Some((_, 'r')) => '\r',
			Some((_, 't')) => '\t',
			//  Rust char type doesn't have these escapes but C does, and
			//  _technically_ these strings can have all C escapes.
			Some((_, 'a')) => '\x07',
			Some((_, 'b')) => '\x08',
			Some((_, 'f')) => '\x0C',
			Some((_, 'v')) => '\x0B',
			//  For extra fun, allow escaped hexadecimal codes.
			Some((i, 'x')) => match (iter.next(), iter.next()) {
				//  Grab the next two characters in the stream
				(Some((_, a)), Some((_, b))) => {
					//  Generates an error at the appropriate spot in the stream
					let err = |n| Error(error_position!(text[n ..].into(), Custom('x' as u32)));
					let (_, a) = hex2num(a).map_err(|_| err(i + 1))?;
					let (_, b) = hex2num(b).map_err(|_| err(i + 2))?;
					hex2byte((a, b)) as char
				},
				//  If there was only one, then the stream needs another.
				(Some(_), None) => return Err(Incomplete(Size(1))),
				//  If there were none, then the stream needs two more.
				(None, None) => return Err(Incomplete(Size(2))),
				//  This is completely impossible when using iterators.
				(None, Some(_)) => unreachable!("Iterators don't do this"),
			},
			//  TODO: Implement `\u` escapes
			_ => return Err(Error(error_position!(text, Custom('\\' as u32)))),
		});
	}
	Ok(("".into(), out.into()))
}

/// Recognizes a double-quote-delimited string and attempts to escape its text.
///
/// This just pipes `dquote` into `escaped`, returning the escaped string if
/// successful or the raw `dquote` interior if the escape failed.
#[cfg_attr(feature = "trace", flame)]
pub fn dquote_escape(text: CompleteStr) -> ParseResult<Cow<str>> {
	let (rem, d) = dquote(text)?;
	//  If dquote succeeds, try to escape its interior.
	Ok((rem, match escaped(d) {
		//  escaped's remnant is immaterial; just return the new string
		Ok((_, esc)) => esc,
		//  If escaped fails, give up and just return the dquote interior with
		//  backslash sequences intact.
		_ => Cow::Borrowed(&d),
	}))
}

/// Strips a leading newline from some text.
///
/// This function returns the newline that it removed, in case the caller is
/// interested. It only matches on `\n` and `\r\n` sequences, and does not yet
/// match on other Unicode line breaks.
///
/// This always returns `Ok`, and is safe to unwrap.
///
/// # Examples
///
/// ```rust
/// # extern crate cosmonaut;
/// # use cosmonaut::parse_utils::strip_leading_newlines;
/// # fn main() {
/// let (rem, crlf) = strip_leading_newlines("\r\nhello\r\n".into()).unwrap();
/// # let (rem, crlf) = (rem.0, crlf.0);
/// assert_eq!(rem, "hello\r\n");
/// assert_eq!(crlf, "\r\n");
/// # }
/// ```
#[cfg_attr(feature = "trace", flame)]
pub fn strip_leading_newlines(text: CompleteStr) -> ParseResult<CompleteStr> {
	//  Set output cursors to separate leading newlines from actual text,
	//  starting before the 0th element in the text.
	let (mut contents, mut newlines) = (text, CompleteStr::from(""));
	loop {
		//  If the first element is a '\n', then advance both cursors by one.
		//  This means shrinking contents from the left and growing newlines to
		//  the right.
		if contents.starts_with('\n') {
			contents = contents[1 ..].into();
			newlines = text[..= newlines.len()].into();
		}
		//  If the first element is "\r\n", do the same as above but for two.
		else if contents.starts_with("\r\n") {
			contents = contents[2 ..].into();
			newlines = text[.. newlines.len() + 2].into();
		}
		//  If not, the search has ended, return the split text.
		else {
			return Ok((contents, newlines));
		}
	}
}

/// Pulls one line out of a text stream.
///
/// This will always return successfully: if there is at least one newline in
/// the source text, this will split the stream at that newline; if there are no
/// newlines, it will return the full text and empty-string.
///
/// It breaks on `"\n"` only.
///
/// As this always returns `Ok`, it is safe to unwrap.
///
/// # Usage
///
/// ```rust
/// # extern crate cosmonaut;
/// # use cosmonaut::parse_utils::get_line;
/// # fn main() {
/// let (rest, first) = get_line("hello\nworld".into()).unwrap();
/// # let (rest, first) = (rest.0, first.0);
/// assert_eq!(first, "hello\n");
/// assert_eq!(rest, "world");
///
/// let (rest, first) = get_line("hello, world".into()).unwrap();
/// # let (rest, first) = (rest.0, first.0);
/// assert_eq!(first, "hello, world");
/// assert_eq!(rest, "");
/// # }
#[cfg_attr(feature = "trace", flame)]
pub fn get_line(text: CompleteStr) -> ParseResult<CompleteStr> {
	#[cfg_attr(feature = "cargo-clippy", allow(range_plus_one))]
	//  Split the left-trimmed text at the first newline, returning all of it
	//  if there is not one.
	match text.find('\n') {
		//  .find returns the index of '\n', and splitting on it puts '\n' in
		//  the rem part, not the val part. Add one in order to have it in the
		//  val part.
		Some(n) => Ok((text[n + 1 ..].into(), text[..= n].into())),
		None => Ok(("".into(), text)),
	}
}

/// Applies a parser to only the first logical line of a text stream.
///
/// This function is useful for ensuring that a parser only consumes one logical
/// line of the source text, even if it would be valid for the parser to cross a
/// newline and consume part of the following lines.
///
/// # Behavior
///
/// This takes a source text stream, strips all leading newlines (that is,
/// newline tokens `\n` and `\r\n` that are not preceded by any other text,
/// including whitespace), then splits the remainder at the first line break (at
/// present, only `U+000A`) and passes the front of the split to the given
/// function. It then recombines any of the text that the function did not
/// consume (including trailing whitespace) with the back half of the split, and
/// emits that as its remnant item.
///
/// If this function is called with the text
/// <code>"<u>Some parseable text\n</u>Some more text"</code> and a function
/// `op(&str) -> ParseResult<T>`, then the underlined part is passed into `op`.
/// If `op` returns `Ok(("\n", r))`, then the full return item is
/// `Ok(("Some more text", r))`. If `op` returns `Ok(("text\n", r))`, then the
/// full return item is `Ok(("text\nSome more text", r))`.
///
/// This behavior also holds for `\r\n` newlines. Other newlines are not yet
/// handled.
///
/// # Usage
///
/// Suppose we have the COSMOS text:
///
/// ```cosmos
/// COMMAND TARGET NAME BIG_ENDIAN
///   PARAMETER NAME 0 8 UINT 0 1 0
///     STATE "Off" 0
///   HAZARDOUS
/// ```
///
/// Normally, the parsers in this library are whitespace-agnostic. However, this
/// means that in the example above, the `State` parser will ordinarily consume
/// both of the last two lines, yielding
/// `State { name: Off, val: 0, hazard: Some(_) }`, when in truth, the hazard
/// warning belongs to the command block, not to the state.
///
/// Parsers which must be aware of this distinction can use this function to
/// pass only the first line of text to a child parser, and still be guaranteed
/// to receive back all unused text (even from within the first line).
///
/// # Examples
///
/// This example will split some text into individual words using the `word`
/// parser from this module. `word` will skip over ALL whitespace, including
/// newlines, so calling it repeatedly on the source string
///
/// ```rust,no-run
/// let text = r#"hello world
///  won't parse
/// "#;
/// ```
///
/// > Note the space at the front of ` won't`. This is to demonstrate that the
/// > breaks occur at newlines, and arbitrary whitespace is not stripped.
///
/// will yield `["hello", "world", "won't", "parse"]` with no remnant, which is
/// not the effect we want. Wrapping the repetition of `word` in
/// `first_line_only` will yield `["hello", "world"]` with `"\n won't parse\n"`
/// as the remnant.
///
/// ```rust
/// # extern crate cosmonaut;
/// # use cosmonaut::parse_utils::first_line_only;
/// # use cosmonaut::ParseResult;
/// use cosmonaut::parse_utils::word;
/// # #[macro_use] extern crate nom;
/// # fn main() {
/// # let text = "hello world\n won't parse\n";
///
/// //  type inference isn't having a good time here
/// # let text: ::nom::types::CompleteStr = text.into();
/// let res: ParseResult<_> = many1!(text, word);
/// let all = res.unwrap().1;
/// # let all: Vec<&str> = all.into_iter().map(|v| v.0).collect();
/// assert_eq!(all, &["hello", "world", "won't", "parse"]);
///
/// let (rem, val) = first_line_only(text.into(), |line| many1!(line, word)).unwrap();
/// # let (rem, val): (&str, Vec<&str>) = (rem.0, val.into_iter().map(|v| v.0).collect());
/// assert_eq!(val, &["hello", "world"]);
/// assert_eq!(rem, "\n won't parse\n");
/// # }
/// ```
//  The lifetimes must be explicitly annotated in order to prove to the compiler
//  that all references are from the same object. The input text is split, and
//  part of it goes into the `op` function, which returns part of the input text
//  that is then reunified with the rest of the input text that was not passed.
//  Lifetime elision rules cause the compiler to assume that the references
//  entering and leaving `op` are decoupled from the lifetimes entering and
//  leaving this function.
#[cfg_attr(feature = "trace", flame)]
pub fn first_line_only<'a, F, T>(text: CompleteStr<'a>, op: F) -> ParseResult<'a, T>
where F: Fn(CompleteStr<'a>) -> ParseResult<'a, T> {
	//  Strip leading newlines.
	let (text, _) = strip_leading_newlines(text).expect("infallible");
	//  Peel off the first line. We can drop the reference to the remaining text
	//  since we still have a handle to the entire input.
	let (_, line) = get_line(text).unwrap();
	//  rem here refers to all text in the first line that was not consumed.
	let (rem, out) = op(line)?;
	//  This parser must return `((rem + rest), out)` so as to not drop text. We
	//  know the distance from start to newline (`line.len()`) and the distance
	//  backward from newline to unconsumed text (`rem.len()`), so we can index
	//  into `text` at that point for the full remnant.
	let len = line.len() - rem.len();
	Ok((text[len ..].into(), out))
}

/// Parses a comment.
///
/// COSMOS comments span from `#` to the end of the line or the text. They do
/// not preserve the first whitespace after `#`, but do preserve subsequent
/// leading whitespace, and do not affect the text after the line terminator.
///
/// I have not seen comments in COSMOS text yet, so it is possible that they do
/// not exist and this function would be an incompatible extension. I am leaving
/// it implemented in the event that the spec maintainers come to their senses.
#[cfg_attr(feature = "cargo-clippy", allow(shadow_reuse))]
#[cfg_attr(feature = "trace", flame)]
pub fn comment(text: CompleteStr) -> ParseResult<CompleteStr> {
	//  Does the text start with a #?
	let (rem, _) = tag!(CompleteStr::from(text.trim_left()), "#")?;
	//  Is there a single space or tab after the #? If so, skip it.
	let (rem, _) = alt!(rem, tag!(" ") | tag!("\t")).unwrap_or((rem, "".into()));
	//  Peel off the first line and strip any trailing whitespace from it.
	get_line(rem).map(|(r, c)| (r, c.trim_right().into()))
}

/// Parses a sequence matching `/'0x(hh)+'/` into a run of bytes.
///
/// # Usage
///
/// In COSMOS text, a block literal is represented by a single-quote-wrapped
/// series of hex digits, preceded by the token `0x`.
///
/// ```rust
/// # extern crate cosmonaut;
/// # use cosmonaut::parse_utils::block;
/// # fn main() {
/// let (_, b) = block("'0x0123456789abcdef'".into()).unwrap();
/// assert_eq!(b, &[0x01u8, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef]);
/// # }
/// ```
#[cfg_attr(feature = "trace", flame)]
pub fn block(text: CompleteStr) -> ParseResult<Vec<u8>> {
	//  Turns out function currying is in fact useful to do.
	fn helper<'a, F, R>(text: CompleteStr<'a>, op: F) -> ParseResult<'a, R>
	where F: Fn(CompleteStr<'a>) -> ParseResult<'a, R> {
		delimited!(text, tag!("'0x"), op, tag!("'"))
	}
	//  precondition: there are 5 or more characters.
	if text.len() <= 4 {
		return Err(Error(error_position!(text, Custom('b' as u32))));
	}
	//  happy path: an even number of digits between `'0x` and `'`.
	if text.len() % 2 == 0 {
		helper(text, |t| many1!(t, map!(tuple!(hex2num_s, hex2num_s), hex2byte)))
	}
	//  sad path: an odd number of digits.
	else {
		let (rem, all) = helper(text, |t| is_not!(t, "'"))?;
		let mut out = Vec::with_capacity((all.len() / 2) + 1);
		let (run, head) = hex2num_s(all)?;
		out.push(head);
		let error_gen = |_| Error(error_position!(run, Custom('b' as u32)));
		let mut chars = run.chars();
		while let Some(hi) = chars.next() {
			let lo = chars.next().expect("parse_utils/block/529 is infallible");
			let hi = hex2num(hi).map_err(&error_gen)?.1;
			let lo = hex2num(lo).map_err(&error_gen)?.1;
			out.push(hex2byte((hi, lo)));
		}
		Ok((rem, out))
	}
}

/// Parses a hexadecimal digit into a byte number.
#[cfg_attr(feature = "trace", flame)]
fn hex2num(c: char) -> ::nom::IResult<char, u8> {
	use nom::Context::Code;
	if c > 0xFF as char {
		return Err(Error(Code(c, Custom('x' as u32))));
	}
	let u: u8 = c as u8;
	match u {
		b'A' ... b'F' => Ok((c, u - b'A' + 10)),
		b'a' ... b'f' => Ok((c, u - b'a' + 10)),
		b'0' ... b'9' => Ok((c, u - b'0')),
		_ => Err(Error(Code(c, Custom('x' as u32)))),
	}
}

/// Parses a hexadecimal digit out of a string.
#[cfg_attr(feature = "trace", flame)]
fn hex2num_s(text: CompleteStr) -> ParseResult<u8> {
	use nom::{
		Err::Incomplete,
		Needed::Size,
	};
	if let Some(c) = text.chars().next() {
		hex2num(c)
			.map(|(_, o)| (text[1 ..].into(), o))
			//  The error returned by hex2num has no context.
			.map_err(|_| Error(error_position!(text, Custom('x' as u32))))
	}
	else {
		Err(Incomplete(Size(1)))
	}
}

/// Builds a full byte out of two bytes parsed from hex literals.
///
/// Given two bytes, high and low, the high byte is shifted up 4 bits and the
/// low byte has its high 4 bits cleared. The two are then or'd together to
/// produce the output.
fn hex2byte((h, l): (u8, u8)) -> u8 {
	(h << 4) | (l & 0x0F)
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn word() {
		use super::word;
		assert_matches!(
			word(Cs("hello world")),
			Ok((Cs(" world"), Cs("hello")))
		);
		assert_matches!(
			word(Cs("   hello   world   ")),
			Ok((Cs("   world   "), Cs("hello")))
		);

		let (rem, hello) = word(Cs("   hello   world   ")).unwrap();
		assert_eq!(hello.0, "hello");
		assert_eq!(rem.0, "   world   ");
		let (rem, world) = word(rem).unwrap();
		assert_eq!(world.0, "world");
		assert_eq!(rem.0, "   ");

		//  word() must fail on empty string
		assert!(word("  ".into()).is_err());
	}

	#[test]
	fn squote() {
		use super::squote;
		assert_matches!(
			squote(Cs("'hello world' is a single token")),
			Ok((Cs(" is a single token"), Cs("hello world")))
		);
		assert_matches!(
			squote(Cs("   'hello world'   loses left whitespace")),
			Ok((Cs("   loses left whitespace"), Cs("hello world")))
		);
	}

	#[test]
	fn dquote() {
		use super::dquote;
		assert_matches!(
			dquote(Cs(r#""hello, world""#)),
			Ok((Cs(""), Cs("hello, world")))
		);
		assert_matches!(
			dquote(Cs(r#""hello \"world\"""#)),
			Ok((Cs(""), Cs(r#"hello \"world\""#)))
		);
		assert_matches!(
			dquote(Cs(r#"   "hello \"world\"" is a token"#)),
			Ok((Cs(" is a token"), Cs(r#"hello \"world\""#)))
		);
	}

	#[test]
	fn dquote_errors() {
		use super::dquote;
		use nom::{
			Err::{
				Error,
				Incomplete,
			},
			Needed::Unknown,
		};
		assert_matches!(dquote(Cs("hello")), Err(Error(_)));
		assert_matches!(dquote(Cs("\"non-terminated")), Err(Incomplete(Unknown)));
	}

	#[test]
	fn escaped() {
		use super::escaped;
		use std::borrow::Cow;
		assert_matches!(escaped(Cs("unescaped")), Ok((Cs(""), Cow::Borrowed("unescaped"))));
		//  escaped() is not a whitespace stripper
		assert_matches!(
			escaped(Cs("   padded unescaped   ")),
			Ok((Cs(""), Cow::Borrowed("   padded unescaped   ")))
		);
		match super::escaped(Cs(r#"\\hello\t\"world\"\r\n"#)) {
			Ok((Cs(""), Cow::Owned(s))) => assert_eq!(s, "\\hello\t\"world\"\r\n"),
			e => panic!("Error parsing an escaped string: {:?}", e),
		}
		match super::escaped(Cs(r#"\a\b\f\v"#)) {
			Ok((Cs(""), Cow::Owned(s))) => {
				assert_eq!(s, "\x07\x08\x0C\x0B");
			},
			e => panic!("Error escaping the C escapes: {:?}", e),
		}
	}

	#[test]
	fn comment() {
		use super::comment;
		assert_matches!(
			comment(Cs("# comment text")),
			Ok((Cs(""), Cs("comment text")))
		);
		assert_matches!(
			comment(Cs("# comment\n")),
			Ok((Cs(""), Cs("comment")))
		);
		assert_matches!(
			comment(Cs("# comment\r\nNext line")),
			Ok((Cs("Next line"), Cs("comment")))
		);
		assert_matches!(
			comment(Cs("#\tcomment\n    Next line")),
			Ok((Cs("    Next line"), Cs("comment")))
		);
	}

	#[test]
	fn number() {
		use nom::Err::Error;
		assert_matches!(inum(Cs("12345")), Ok((Cs(""), 12_345_i64)));
		assert_matches!(unum(Cs("12345")), Ok((Cs(""), 12_345_u64)));
		assert_matches!(unum(Cs("abcd")), Err(Error(_)));
	}

	#[test]
	fn hex() {
		assert_matches!(hnum(Cs("0xabcd")), Ok((Cs(""), 0xABCD)));
		assert_matches!(hnum(Cs("abcd")), Err(::nom::Err::Error(_)));
	}

	#[test]
	fn special_first_line() {
		use {
			item::Item,
			parameter::Parameter,
			decorators::*,
			traits::Named,
		};

		let text = Cs(r#"
STATE "Name" 0 UNIT NAME N
STATE "Other" 1
		"#);

		let (text, _) = strip_leading_newlines(text).expect("infallible");
		let (two, one) = get_line(text).unwrap();
		assert_eq!(one.trim(), r#"STATE "Name" 0 UNIT NAME N"#);
		assert_eq!(two.trim(), r#"STATE "Other" 1"#);

		// The State parser should provably halt at UNIT
		let (rem, s1) = State::<Parameter>::parse(one).unwrap();
		assert_eq!(s1.name(), "Name");
		assert_eq!(rem.trim(), "UNIT NAME N");

		let (rem, s1) = first_line_only(text, State::<Item>::parse).unwrap();
		assert_eq!(s1.name(), "Name");
		// The remainder should include both the unit and the second state.
		let rlines: Vec<_> = rem.lines()
			.map(|l| l.trim())
			.filter(|l| !l.is_empty())
			.collect();
		let lines = ["UNIT NAME N", r#"STATE "Other" 1"#];
		assert_eq!(rlines, lines);
	}

	#[test]
	fn special_first_line_with_whitespace() {
		type Void<'a> = ParseResult<'a, ()>;

		//  first_line_only strips leading blank lines, as these are not useful
		let text = Cs("\r\nSome source remnant\r\nsome more source");
		//                             ^      ^^^^^
		//                             14     212223

		fn check(input: Cs) {
			assert_eq!(input.0, "Some source remnant\r\n");
		}
		fn empty(input: Cs) -> Void {
			check(input);
			Ok(("".into(), ()))
		}
		fn lf(input: Cs) -> Void {
			check(input);
			Ok(("\n".into(), ()))
		}
		fn crlf(input: Cs) -> Void {
			check(input);
			Ok(("\r\n".into(), ()))
		}
		fn nonempty(input: Cs) -> Void {
			check(input);
			Ok(("remnant\r\n".into(), ()))
		}

		let (rem, _) = first_line_only(text, empty).unwrap();
		assert_eq!(rem, text[23 ..].into());

		//  Function arguments which give back a bare newline should not have
		//  that newline stripped from the total output

		let (rem, _) = first_line_only(text, lf).unwrap();
		assert_eq!(rem, text[22 ..].into());

		let (rem, _) = first_line_only(text, crlf).unwrap();
		assert_eq!(rem, text[21 ..].into());

		//  Function arguments which give back text should not have that text
		//  stripped or altered in any way

		let (rem, _) = first_line_only(text, nonempty).unwrap();
		assert_eq!(rem, text[14 ..].into());
	}

	#[test]
	fn block() {
		use super::block;

		let odd = "'0xabc'";
		let even = "'0xabcd'";

		let (_, o) = block(odd.into()).unwrap();
		assert_eq!(o, &[0xa, 0xbc]);

		let (_, e) = block(even.into()).unwrap();
		assert_eq!(e, &[0xab, 0xcd]);
	}
}
