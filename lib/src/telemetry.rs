#![doc(include = "../doc/cosmos/telemetry.md")]

use super::{
	ParseResult,
	cosmos_macro::Macro,
	decorators::{
		AllowShort,
		Description,
		Meta,
		MetaSet,
	},
	header::Header,
	item::Item,
	traits::{
		Bodied,
		Described,
		Keyword,
		Named,
		Parsed,
		Selectable,
		Targeted,
	},
};
use nom::types::CompleteStr;
use std::{
	fmt::{
		self,
		Display,
		Formatter,
	},
	ops::{
		Deref,
		Index,
	},
};

#[doc(include = "../doc/cosmos/telemetry.md")]
#[derive(Clone, Debug, Default)]
pub struct Telemetry {
	/// The meta information describing the telemetry.
	pub header: Header,
	/// The set of child elements, such as `ITEM`s, that compose the `TELEMETRY`
	/// element in text.
	pub body: <Telemetry as Bodied>::Body,
}

#[doc(include = "../doc/cosmos/element_body.md")]
#[derive(Clone, Debug, Default)]
pub struct Body {
	/// A set of `ITEM` elements that make up the serialized body of the
	/// `Telemetry`.
	pub items: Vec<Item>,
	/// A set of `SELECT_ITEM` patch elements that must be folded onto the item
	/// list.
	pub patches: Vec<<Item as Selectable>::Patch>,
	/// A human-readable description.
	pub description: Option<Description>,
	/// A set of meta-information describing the `TELEMETRY` element.
	pub meta: MetaSet,
	/// A flag stating that a message which matches signature fields but is
	/// shorter than the calculated length should be accepted and zero-padded.
	pub allow_short: AllowShort,
}

#[doc(include = "../doc/cosmos/patch.md")]
#[derive(Clone, Debug, Default)]
pub struct Patch {
	/// The name of the `Telemetry` to patch.
	pub name: String,
	/// The target of the `Telemetry` to patch.
	pub target: String,
	/// The edit body.
	pub body: <Telemetry as Bodied>::Body,
}

impl Telemetry {
	/// Borrows a named item.
	#[cfg_attr(feature = "trace", flame)]
	pub fn item<'a>(&self, name: &'a str) -> Option<&Item> {
		self.body.items.iter().find(|i| i.name() == name)
	}

	/// Borrows a named item, mutably.
	#[cfg_attr(feature = "trace", flame)]
	pub fn item_mut<'a>(&mut self, name: &'a str) -> Option<&mut Item> {
		self.body.items.iter_mut().find(|i| i.name() == name)
	}

	/// Borrows the set of items.
	///
	/// The item set cannot be borrowed mutably. Any changes to items must be
	/// performed by generating a suitable `Patch` structure and applying it to
	/// the `Telemetry` instance.
	#[cfg_attr(feature = "trace", flame)]
	pub fn items(&self) -> &[Item] {
		&self.body.items
	}
}

impl Bodied for Telemetry {
	type Body = Body;

	#[cfg_attr(feature = "trace", flame)]
	fn apply(&mut self, body: Self::Body) {
		use std::mem;
		//  Take apart the body for piece-wise application.
		let Body {
			items,
			patches,
			description,
			meta,
			allow_short,
		} = body;
		self.body.items.extend(items);
		self.body.patches.extend(patches);
		self.body.meta.extend(meta);
		if let Some(Description { text }) = description {
			self.describe(text);
		}
		if *allow_short {
			self.body.allow_short = allow_short;
		}
		//  Pull out all the SELECT_ITEM patches and attempt to apply them to
		//  existing items.
		let patches = mem::replace(&mut self.body.patches, Vec::new());
		for patch in patches {
			if self.item(patch.name()).is_some() {
				self.item_mut(patch.name()).map(|i| i.patch(patch));
			}
			else {
				error!("TELEMETRY {} has no ITEM named {}", self.name(), patch.name());
				//  Put the patch back in; an item by this name might appear.
				self.body.patches.push(patch);
			}
		}
	}
}

impl Deref for Telemetry {
	type Target = [Item];
	fn deref(&self) -> &Self::Target {
		&self.body.items
	}
}

impl Described for Telemetry {
	#[cfg_attr(feature = "trace", flame)]
	fn description(&self) -> Option<&str> {
		self.header.description()
	}

	#[cfg_attr(feature = "trace", flame)]
	fn describe(&mut self, description: String) {
		self.header.describe(description);
	}
}

impl Display for Telemetry {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::with_capacity(64);
		writeln!(out, "{} {}", <Self as Keyword>::KEYWORD, self.header)?;
		write!(out, "{}", self.body)?;
		writeln!(fmt, "{}", out)
	}
}

impl Index<usize> for Telemetry {
	type Output = Item;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: usize) -> &Self::Output {
		&self.body.items[idx]
	}
}

impl<'i> Index<&'i str> for Telemetry {
	type Output = Item;

	#[cfg_attr(feature = "trace", flame)]
	fn index(&self, idx: &'i str) -> &Self::Output {
		self.body.items.iter().find(|p| p.name() == idx).expect(&format!(
			"Telemetry `{}` has no items named `{}`",
			self.name(),
			idx
		))
	}
}

impl Keyword for Telemetry {
	const KEYWORD: &'static str = "TELEMETRY";
}

impl Named for Telemetry {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		self.header.name()
	}
}

impl Parsed for Telemetry {
	/// Parses a `TELEMETRY` element from a stream of COSMOS text.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		let (rem, (h, b)) = do_parse!(text,
			call!(<Self as Keyword>::seek) >>
			h: call!(Header::parse) >>
			b: call!(<<Self as Bodied>::Body as Parsed>::parse) >>
			(h, b)
		)?;
		let mut out = Self::default();
		out.header = h;
		out.apply(b);
		Ok((rem, out))
	}
}

impl Selectable for Telemetry {
	type Patch = Patch;

	#[cfg_attr(feature = "trace", flame)]
	fn patch(&mut self, patch: Self::Patch) {
		if self.target() == patch.target() && self.header.name() == patch.name() {
			self.apply(patch.body);
		}
	}
}

impl Targeted for Telemetry {
	fn target(&self) -> &str {
		&self.header.target
	}
}

impl Display for Body {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		let Self {
			ref items,
			ref patches,
			ref description,
			ref meta,
			ref allow_short,
		} = *self;
		for item in items {
			for line in format!("{}", item).lines() {
				writeln!(out, "\t{}", line)?;
			}
		}
		if let Some(ref d) = *description {
			writeln!(out, "\t{}", d)?;
		}
		if **allow_short {
			writeln!(out, "\t{}", self.allow_short)?;
		}
		for m in meta {
			writeln!(out, "\t{}", m)?;
		}
		for patch in patches {
			for line in format!("{}", patch).lines() {
				writeln!(out, "\t{}", line)?;
			}
		}
		write!(fmt, "{}", out)
	}
}

impl Parsed for Body {
	/// Parses the children of a `TELEMETRY` element.
	///
	/// `TELEMETRY`s are required to have at least one child, and this parser
	/// will fail if none are found.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		let mut out = Self::default();
		let mut rem = text.trim_left().into();
		loop {
			rem = if let Ok((rem, item)) = Item::parse(rem) {
				out.items.push(item);
				rem
			}
			else if let Ok((rem, patch)) = Item::select(rem) {
				out.patches.push(patch);
				rem
			}
			else if let Ok((rem, mac)) = Macro::<Item>::parse(rem) {
				out.items.extend(mac.unwrap());
				rem
			}
			else if let Ok((rem, desc)) = Description::parse(rem) {
				out.description = Some(desc);
				rem
			}
			else if let Ok((rem, meta)) = Meta::parse(rem) {
				out.meta.insert(meta);
				rem
			}
			else if let Ok((rem, short)) = AllowShort::parse(rem) {
				out.allow_short = short;
				rem
			}
			else {
				break;
			};
		}
		if rem.len() == text.trim_left().len() {
			use nom::{
				Err::Error,
				ErrorKind::Custom,
			};
			error!("TELEMETRY elements must have at least one child element");
			return Err(Error(error_position!(text, Custom('C' as u32))));
		}
		Ok((rem, out))
	}
}

impl Display for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		use std::fmt::Write;
		let mut out = String::new();
		writeln!(out, "{}{} {} {}",
			<Telemetry as Selectable>::KEYWORD,
			<Telemetry as Keyword>::KEYWORD,
			self.target,
			self.name
		)?;
		write!(out, "{}", self.body)?;
		writeln!(fmt, "{}", out)
	}
}

impl Named for Patch {
	#[cfg_attr(feature = "trace", flame)]
	fn name(&self) -> &str {
		&self.name as &str
	}
}

impl Parsed for Patch {
	/// Parses a `SELECT_TELEMETRY` element.
	///
	/// `SELECT_TELEMETRY` elements are required to have children, and this
	/// parser will fail if none are found.
	#[cfg_attr(feature = "trace", flame)]
	fn parse(text: CompleteStr) -> ParseResult<Self> {
		use super::parse_utils::word;
		do_parse!(CompleteStr::from(text.trim_left()),
			target: map!(word, |w| w.to_string()) >>
			name: map!(word, |w| w.to_string()) >>
			body: call!(<Telemetry as Bodied>::Body::parse) >>
			(Self {
				name,
				target,
				body,
			})
		)
	}
}

impl Targeted for Patch {
	fn target(&self) -> &str {
		&self.target
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		let text = r#"
TELEMETRY TARGET HS BIG_ENDIAN "Health and Status information"
  ITEM CCSDSVER 0 3 UINT "COSMOS PRIMARY HEADER VERSION NUMBER"
SELECT_TELEMETRY TARGET HS
  ALLOW_SHORT
		"#.trim();

		let (rem, t) = Telemetry::parse(Cs(text)).unwrap();
		assert_eq!(t.items().len(), 1);
		let (_, p) = Telemetry::select(rem).unwrap();
		assert_eq!(p.name(), "HS");
	}
}
