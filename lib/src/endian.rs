#![doc(include = "../doc/cosmos/endian.md")]

use super::{
	ParseResult,
	parse_utils::word,
};
use nom::types::CompleteStr;
use std::{
	default::Default,
	fmt::{
		self,
		Display,
		Formatter,
	},
};

#[doc(include = "../doc/cosmos/endian.md")]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Endian {
	/// The data is laid out in big-endian order (MSB first).
	Big,
	/// The data is laid out in little-endian order (LSB first).
	Little,
}

impl Endian {
	/// Parses the endianness of an item.
	///
	/// COSMOS Endianness is denoted by the tokens `BIG_ENDIAN` or
	/// `LITTLE_ENDIAN`.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		word(text).and_then(|(rem, val)| alt!(val,
			tag!("BIG_ENDIAN") => { |_| Endian::Big } |
			tag!("LITTLE_ENDIAN") => { |_| Endian::Little  }
		).map(|(_, e)| (rem, e)))
	}
}

impl Default for Endian {
	#[cfg_attr(feature = "trace", flame)]
	fn default() -> Self {
		Endian::Little
	}
}

impl Display for Endian {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "{}", match *self {
			Endian::Big => "BIG_ENDIAN",
			Endian::Little => "LITTLE_ENDIAN",
		})
	}
}

#[cfg(test)]
mod tests {
	use super::Endian;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn parse() {
		match Endian::parse(Cs("BIG_ENDIAN")) {
			Ok((_, Endian::Big)) => {},
			e => panic!("{:?}", e),
		}
		match Endian::parse(Cs("LITTLE_ENDIAN")) {
			Ok((_, Endian::Little)) => {},
			e => panic!("{:?}", e),
		}
	}
}
