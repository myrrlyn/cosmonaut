#![doc(include = "../doc/cosmos/kind.md")]

use super::{
	ParseResult,
	parse_utils::{
		fnum,
		inum,
		unum,
	},
	traits::Keyword,
};
use nom::types::CompleteStr;
use std::{
	convert::From,
	default::Default,
	fmt::{
		self,
		Display,
		Formatter,
	},
};

//  Enumerate the above kinds for public export.
/// A marker for the data kind contained in the field.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Kind {
	/// A signed integer.
	Int,
	/// An unsigned integer.
	Uint,
	/// A floating-point number.
	Float,
	/// A derived number (this cannot be directly instantiated).
	Derived,
	/// A string.
	Text,
	/// A blob.
	Block,
}

impl Kind {
	/// Parses the various data kind keywords.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		alt!(text,
			tag!("INT") => { |_| Kind::Int } |
			tag!("UINT") => { |_| Kind::Uint } |
			tag!("FLOAT") => { |_| Kind::Float } |
			tag!("DERIVED") => { |_| Kind::Derived } |
			tag!("STRING") => { |_| Kind::Text } |
			tag!("BLOCK") => { |_| Kind::Block }
		)
	}
}

impl Default for Kind {
	#[cfg_attr(feature = "trace", flame)]
	fn default() -> Self {
		Kind::Int
	}
}

impl Display for Kind {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "{}", match *self {
			Kind::Int => "INT",
			Kind::Uint => "UINT",
			Kind::Float => "FLOAT",
			Kind::Derived => "DERIVED",
			Kind::Text => "STRING",
			Kind::Block => "BLOCK",
		})
	}
}

/// A data element contained within a field.
//  Bite me, Clippy.
#[cfg_attr(feature = "cargo-clippy", allow(stutter))]
#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub enum KindVal {
	/// A signed integer.
	///
	/// May be any token which can parse as a signed decimal integer, as well as
	/// any permutation of `INT{8,16,32,64}_{MIN,MAX}`.
	Int(i64),
	/// An unsigned integer.
	///
	/// May be any token which can parse as an unsigned decimal integer, a
	/// hexadecimal number (`/0x($h)+/`), or any permutation of
	/// `UINT_{8,16,32,64}_{MIN,MAX}`.
	Uint(u64),
	/// A floating-point number.
	Float(f64),
	/// A derived number (this cannot be directly instantiated).
	Derived,
	/// A string.
	Text(String),
	/// A blob.
	Block(Vec<u8>),
}

impl KindVal {
	/// Parse some text as if it were a `KindVal` data atom.
	///
	/// This attempts the numeric parses first, in order of signed integer,
	/// unsigned integer, then floating point number; after this, it attempts
	/// various string parsers (double-quoted runs, single-quoted runs) and
	/// lastly attempts a single word.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse(text: CompleteStr) -> ParseResult<Self> {
		use super::parse_utils::*;
		alt!(text,
			inum => { |i| Self::from(i) } |
			unum => { |u| Self::from(u) } |
			fnum => { |f| Self::from(f) } |
			dquote_escape => { |d| Self::from(String::from(d)) } |
			squote => { |s| Self::from(s) } |
			word => { |w| Self::from(w) }
		)
	}

	/// Casts the inner value into the specified kind.
	///
	/// This panics when the cast cannot be performed.
	#[cfg_attr(feature = "trace", flame)]
	pub fn cast(self, into: Kind) -> Self {
		match into {
			Kind::Int => self.into_int(),
			Kind::Uint => self.into_uint(),
			Kind::Float => self.into_float(),
			Kind::Derived => panic!("These don't exist!"),
			Kind::Text => self.into_text(),
			Kind::Block => self.into_block(),
		}
	}

	/// Casts the inner value into the specified kind, without consuming it.
	///
	/// This panics when the cast cannot be performed.
	#[cfg_attr(feature = "trace", flame)]
	pub fn cast_in_place(&mut self, into: Kind) { unsafe {
		use std::ptr;
		//  DO NOT ATTEMPT ON DERIVED
		//  On a `Derived` value, `cast` will panic. We want to do that panic,
		//  since it's the correct behavior, but we do NOT want to panic with
		//  the referent in a state that is logically uninitialized ONLY FROM
		//  THE PERSPECTIVE OF THIS FUNCTION! That will result in the KindVal
		//  instance attempting to drop twice (once when the stack value here
		//  unwinds, once when the real owner unwinds), which is Not Ideal.
		//  Granted, it'll occur *during a panic*, which means there won't be
		//  much time for this to be a real problem before the environment
		//  finishes dying.
		//
		//  It's worth noting that the conversions into `Text` and `Block` will
		//  allocate, but if the allocation fails, the program immediately dies.
		//  Not unwinds: dies. OOM causes an instantaneous abort, and does not
		//  run any destructors.
		if let Kind::Derived = into {
			panic!("These don't exist!");
		}
		//  1.  Pull the value out from behind the reference and onto the stack.
		//      This function now """owns""" the value, and can freely move it
		//      through the cast function.
		//  2.  Cast the value, which we know won't panic because we already
		//      panicked if needed, and catch the new value kind. `cast`
		//      probably occurs in place after LLVM optimizations, but we're
		//      doing this to deal with ownership, not stack slots.
		//  3.  Push the value back out behind the reference. Since we know that
		//      this function will not panic once we enter the pull-change-push
		//      run, and we know that we had an &mut exclusive reference, we can
		//      prove that, short of a failure in `borrowck` that is well beyond
		//      our scope, that this function is perfectly safe.
		//
		//      It also MIGHT not incur any `memmove`s at all, since LLVM knows
		//      that the value is going directly back where it was. It will not
		//      be a register-only function, since `into_text` and `into_block`
		//      are considerably expensive. A numeric cast might do it, though.
		//      None of these optimizations are necessary; just observing.
		ptr::write(self, ptr::read(self).cast(into));
	} }

	/// Convert the inner value into a signed integer.
	///
	/// Unsigned integers and floats are clamped at the range boundaries of
	/// `isize`. `Text` is parsed as an integer. If the parse fails, or if self
	/// is a `Block`, then an error message is emitted and the value is returned
	/// untouched.
	#[cfg_attr(feature = "trace", flame)]
	fn into_int(self) -> Self {
		let hi = i64::max_value();
		let lo = i64::min_value();
		#[cfg_attr(feature = "cargo-clippy", allow(cast_possible_truncation))]
		//  It's not going to wrap, Clippy, because I explicitly check for that.
		#[cfg_attr(feature = "cargo-clippy", allow(cast_possible_wrap))]
		#[cfg_attr(feature = "cargo-clippy", allow(cast_precision_loss))]
		//  Doesn't happen.
		#[cfg_attr(feature = "cargo-clippy", allow(cast_sign_loss))]
		match self {
			KindVal::Int(i) => KindVal::Int(i),
			KindVal::Uint(u) => if u > hi as u64 {
				warn!("Provided value {} is too large; saturating at {}", u, hi);
				KindVal::Int(hi)
			}
			else {
				KindVal::Int(u as i64)
			},
			KindVal::Float(f) => if f > hi as f64 {
				warn!("Provided value {} is too large; saturating at {}", f, hi);
				KindVal::Int(hi)
			}
			else if f < lo as f64 {
				warn!("Provided value {} is too small; saturating at {}", f, lo);
				KindVal::Int(lo)
			}
			else {
				KindVal::Int(f.round() as i64)
			},
			KindVal::Derived => panic!("Derived values cannot be converted."),
			KindVal::Text(s) => if let Ok((_, i)) = inum(CompleteStr(&s)) {
				KindVal::Int(i)
			}
			else {
				error!("The string {:?} cannot be parsed as an integer!", s);
				KindVal::Text(s)
			},
			KindVal::Block(_) => {
				error!("Blocks cannot be converted!");
				self
			},
		}
	}

	/// Convert the inner value into an unsigned integer.
	///
	/// This saturates negative numbers at zero, overly large floating point
	/// numbers at `usize::MAX`. `Text` is parsed. On failure, or if self is a
	/// `Block`, an error message is logged and the value is returned unaltered.
	#[cfg_attr(feature = "trace", flame)]
	fn into_uint(self) -> Self {
		let hi = u64::max_value();
		//  I promise I know what I'm doing, Clippy.
		#[cfg_attr(feature = "cargo-clippy", allow(cast_possible_truncation))]
		#[cfg_attr(feature = "cargo-clippy", allow(cast_precision_loss))]
		#[cfg_attr(feature = "cargo-clippy", allow(cast_sign_loss))]
		match self {
			KindVal::Int(i) => if i < 0 {
				warn!("Provided value {} is too small; saturating at 0", i);
				KindVal::Uint(0)
			}
			else {
				KindVal::Uint(i as u64)
			},
			KindVal::Uint(u) => KindVal::Uint(u),
			KindVal::Float(f) => if f > hi as f64 {
				warn!("Provided value {} is too large; saturating at {}", f, hi);
				KindVal::Uint(hi)
			}
			else if f < 0.0 {
				warn!("Provided value {} is too small; saturating at 0", f);
				KindVal::Uint(0)
			}
			else {
				KindVal::Uint(f.round().abs() as u64)
			},
			KindVal::Derived => panic!("Derived values cannot be converted."),
			KindVal::Text(s) => if let Ok((_, u)) = unum(CompleteStr(&s)) {
				KindVal::Uint(u)
			}
			else {
				error!("The string {:?} cannot be parsed as an unsigned integer!", s);
				KindVal::Text(s)
			},
			KindVal::Block(_) => {
				error!("Blocks cannot be converted!");
				self
			},
		}
	}

	/// Convert the inner value into a floating point number.
	///
	/// Converts the integers into their floating point selves, parses a string,
	/// and does nothing on `Block`.
	#[cfg_attr(feature = "trace", flame)]
	fn into_float(self) -> Self {
		//  Caveat emptor.
		#[cfg_attr(feature = "cargo-clippy", allow(cast_precision_loss))]
		match self {
			KindVal::Int(i) => KindVal::Float(i as f64),
			KindVal::Uint(u) => KindVal::Float(u as f64),
			KindVal::Float(f) => KindVal::Float(f),
			KindVal::Derived => panic!("Derived values cannot be converted."),
			KindVal::Text(s) => if let Ok((_, f)) = fnum(CompleteStr(&s)) {
				KindVal::Float(f)
			}
			else {
				error!("The string {:?} cannot be parsed as a floating point number!", s);
				KindVal::Text(s)
			},
			KindVal::Block(_) => {
				error!("Blocks cannot be converted!");
				self
			},
		}
	}

	/// Convert the inner type into a text string.
	///
	/// Formats the numeric types using their `Display` impl (for the integers)
	/// or `Debug` impl (for floating point), and formats `Block` as
	/// `'0x{:02X}*'`.
	#[cfg_attr(feature = "trace", flame)]
	fn into_text(self) -> Self {
		match self {
			KindVal::Int(i) => KindVal::Text(format!("{}", i)),
			KindVal::Uint(u) => KindVal::Text(format!("{}", u)),
			KindVal::Float(f) => KindVal::Text(format!("{:?}", f)),
			KindVal::Derived => panic!("Derived values cannot be converted."),
			KindVal::Text(s) => KindVal::Text(s),
			KindVal::Block(b) => {
				use std::fmt::Write;
				let mut s = String::with_capacity(b.len() * 2 + 4);
				write!(s, "'0x").unwrap();
				for byte in b {
					write!(s, "{:02X}", byte).unwrap();
				}
				write!(s, "'").unwrap();
				KindVal::Text(s)
			},
		}
	}

	/// Convert the inner type into a binary run.
	///
	/// All incoming data is interpreted as raw bits and stored as a `Vec<u8>`.
	#[cfg_attr(feature = "trace", flame)]
	fn into_block(self) -> Self {
		//  We're gonna get terrible in here.
		//
		//  Transmute all the numerics into their raw bit representations. There
		//  is no endianness conversion, just straight up type erasure down to
		//  [u8; 8] for the numerics and [u8] for `Text`.
		use std::mem::transmute;
		match self {
			KindVal::Int(i) => unsafe {
				warn!("Transmuting {} to its bit pattern {:#0X}", i, i as u64);
				let b: [u8; 8] = transmute(i);
				KindVal::from(&b as &[u8])
			},
			KindVal::Uint(u) => unsafe {
				warn!("Transmuting {} to its bit pattern {:#0X}", u, u);
				let b: [u8; 8] = transmute(u);
				KindVal::from(&b as &[u8])
			},
			KindVal::Float(f) => unsafe {
				let x = f.to_bits();
				warn!("Transmuting FLOATING POINT VALUE {} to its bit pattern {:#0X}", f, x);
				let b: [u8; 8] = transmute(x);
				KindVal::from(&b as &[u8])
			},
			KindVal::Derived => panic!("Derived values cannot be converted."),
			//  String is just a flat wrapper around Vec<u8>. We don't even have
			//  to change the vec's bookkeeping.
			KindVal::Text(s) => KindVal::Block(unsafe { ::std::mem::transmute(s) }),
			KindVal::Block(b) => KindVal::Block(b),
		}
	}

	/// Unwraps the instance into a signed integer.
	///
	/// This method panics if `self` is not of variant `Int`.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap_int(self) -> i64 {
		match self {
			KindVal::Int(i) => i,
			_ => panic!("Failed to unwrap a signed integer!"),
		}
	}

	/// Unwraps the instance into an unsigned integer.
	///
	/// This method panics if `self` is not of variant `Uint`.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap_uint(self) -> u64 {
		match self {
			KindVal::Uint(u) => u,
			_ => panic!("Failed to unwrap an unsigned integer!"),
		}
	}

	/// Unwraps the instance into a floating-point value.
	///
	/// This method panics if `self` is not of variant `Float`.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap_float(self) -> f64 {
		match self {
			KindVal::Float(f) => f,
			_ => panic!("Failed to unwrap a floating-point value!"),
		}
	}

	/// Unwraps the instance into a text string.
	///
	/// This method panics if `self` is not of variant `Text`.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap_text(self) -> String {
		match self {
			KindVal::Text(s) => s,
			_ => panic!("Failed to unwrap a text string!"),
		}
	}

	/// Unwraps the instance into a binary sequence.
	///
	/// This method panics if `self` is not of variant `Block`.
	#[cfg_attr(feature = "trace", flame)]
	pub fn unwrap_block(self) -> Vec<u8> {
		match self {
			KindVal::Block(b) => b,
			_ => panic!("Failed to unwrap a binary sequence!"),
		}
	}
}

impl Default for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn default() -> Self {
		KindVal::Int(i64::default())
	}
}

impl Display for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		match *self {
			KindVal::Int(n) => write!(f, "{}", n),
			KindVal::Uint(n) => write!(f, "{}", n),
			KindVal::Float(n) => write!(f, "{:?}", n),
			KindVal::Derived => write!(f, ""),
			KindVal::Text(ref s) => write!(f, "{:?}", s),
			KindVal::Block(ref b) => {
				use std::fmt::Write;
				let mut s = String::with_capacity(b.len() * 2 + 2);
				write!(s, "'0x")?;
				for byte in b as &[u8] {
					write!(s, "{:02X}", byte)?;
				}
				write!(s, "'")?;
				write!(f, "{}", s)
			},
		}
	}
}

impl From<i64> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(i: i64) -> Self {
		KindVal::Int(i)
	}
}

impl From<u64> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(u: u64) -> Self {
		KindVal::Uint(u)
	}
}

impl From<f64> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(f: f64) -> Self {
		KindVal::Float(f)
	}
}

impl<'a> From<&'a str> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(s: &'a str) -> Self {
		KindVal::Text(s.into())
	}
}

impl<'a> From<CompleteStr<'a>> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(cs: CompleteStr<'a>) -> Self {
		KindVal::Text((&cs as &str).into())
	}
}

impl From<String> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(s: String) -> Self {
		KindVal::Text(s)
	}
}

impl<'a> From<&'a [u8]> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(b: &'a [u8]) -> Self {
		KindVal::Block(b.into())
	}
}

impl From<Vec<u8>> for KindVal {
	#[cfg_attr(feature = "trace", flame)]
	fn from(b: Vec<u8>) -> Self {
		KindVal::Block(b)
	}
}

//  Enumerate the above variants for export.
/// Field variants.
///
/// Each field can hold one data atom, a magic identifying atom, or an array of
/// multiple atoms.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Variant {
	/// A field holding one data element.
	Normal,
	/// A field holding an identifying magic number when serialized.
	Ident,
	/// A field holding multiple elements of the same kind.
	Array,
}

impl Variant {
	/// Parses the field keywords.
	///
	/// This function returns a `(bool, Variant)` tuple because the keywords can
	/// include an `APPEND_` prefix that does not affect the field variant
	/// directly, but does alter the control flow of the parent parser.
	///
	/// Field keywords are composed of the variant identifiers and the field
	/// direction keyword, which is provided by the specific call site.
	#[cfg_attr(feature = "trace", flame)]
	pub fn parse<K: Keyword>(text: CompleteStr) -> ParseResult<(bool, Self)> {
		ws!(text, terminated!(
			tuple!(Self::appended, Self::variant),
			tag!(K::KEYWORD)
		))
	}

	/// Detects the presence or absence of `APPEND_`.
	#[cfg_attr(feature = "trace", flame)]
	fn appended(text: CompleteStr) -> ParseResult<bool> {
		map!(text, opt!(tag!("APPEND_")), |o| o.is_some())
	}

	/// Parses the field variant type.
	#[cfg_attr(feature = "trace", flame)]
	fn variant(text: CompleteStr) -> ParseResult<Self> {
		alt!(text,
			tag!("ARRAY_") => { |_| Variant::Array } |
			tag!("ID_") => { |_| Variant::Ident } |
			tag!("") => { |_| Variant::Normal }
		)
	}
}

impl Default for Variant {
	#[cfg_attr(feature = "trace", flame)]
	fn default() -> Self {
		Variant::Normal
	}
}

impl Display for Variant {
	#[cfg_attr(feature = "trace", flame)]
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "{}", match *self {
			Variant::Normal => "",
			Variant::Ident => "ID_",
			Variant::Array => "ARRAY_",
		})
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use item::Item;
	use parameter::Parameter;
	use nom::types::CompleteStr as Cs;

	#[test]
	fn kind() {
		for &(kw, _k) in &[
			("INT", Kind::Int),
			("UINT", Kind::Uint),
			("FLOAT", Kind::Float),
			("DERIVED", Kind::Derived),
			("STRING", Kind::Text),
			("BLOCK", Kind::Block),
		] {
			assert_matches!(Kind::parse(Cs(kw)), Ok((_, _k)));
		}
	}

	#[test]
	fn kind_val() {
		assert_matches!(KindVal::from(0_i64), KindVal::Int(0));
		assert_matches!(KindVal::from(1_u64), KindVal::Uint(1));
		assert_matches!(KindVal::from(1.0), KindVal::Float(_));
		assert_matches!(KindVal::from("hello, world"), KindVal::Text(_));
		assert_matches!(KindVal::from(&[1u8, 2u8] as &[u8]), KindVal::Block(_));
	}

	#[test]
	fn variant() {
		for &(kw, _a, _v) in &[
			("ITEM", false, Variant::Normal),
			("ID_ITEM", false, Variant::Ident),
			("ARRAY_ITEM", false, Variant::Array),
			("APPEND_ITEM", true, Variant::Normal),
			("APPEND_ID_ITEM", true, Variant::Ident),
			("APPEND_ARRAY_ITEM", true, Variant::Array),
		] {
			assert_matches!(Variant::parse::<Item>(Cs(kw)), Ok((_, (_a, _v))));
		}

		for &(kw, _a, _v) in &[
			("PARAMETER", false, Variant::Normal),
			("ID_PARAMETER", false, Variant::Ident),
			("ARRAY_PARAMETER", false, Variant::Array),
			("APPEND_PARAMETER", true, Variant::Normal),
			("APPEND_ID_PARAMETER", true, Variant::Ident),
			("APPEND_ARRAY_PARAMETER", true, Variant::Array),
		] {
			assert_matches!(Variant::parse::<Parameter>(Cs(kw)), Ok((_a, _v)));
		}
	}
}
