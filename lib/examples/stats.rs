/*! Dictionary Stats

This program loads the COSMOS files from assets/, parses them, and prints them
with their statistics. It demonstrates a simple use of the Dictionary API to
load files, parse them, and process the Dictionary.

Run this example with `cargo run -p cosmonaut --example stats`.
!*/

extern crate cosmonaut;
use cosmonaut::{
	Dictionary,
};
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

fn main() {
	let mut dict = Dictionary::new();
	//  This pipeline loads the files and parses them into the dict.
	[
		"patch.cosmos",
		//"nonexistent.cosmos",
		"tlm.cosmos",
		"cmd.cosmos",
	].iter()
	//  Build absolute paths from the filenames using compile time information
	//  so that at runtime we can find the assets.
	.map(|n| PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("assets").join(n))
	//  Attempt to open each file; failures print the failed filename and drop
	//  their entry in the pipeline. Uncomment "//nonexistent.cosmos," above to
	//  see this!
	.filter_map(|n| match File::open(&n) {
		Ok(f) => Some(f),
		Err(_) => {
			eprintln!("Could not open file: {}", n.to_string_lossy());
			None
		},
	})
	//  The original filenames are now lost, so failures from here on cannot be
	//  named. This is avoidable, but requires restructuring the pipeline, and
	//  this example won’t fail.
	//
	//  Taking the File handle, we now allocate a String and read the File into
	//  it. This repository is encoded in UTF-8, so this will succeed!
	//  Production users may need to use locale-aware encoding conversions when
	//  loading their files.
	.map(|mut f| {
		let mut contents = String::new();
		match f.read_to_string(&mut contents) {
			Ok(_) => {},
			Err(_) => eprintln!("Could not read a file to string"),
		}
		contents
	})
	//  At this stage, the pipeline contains Strings; we now attempt to parse
	//  them, and warn on failure.
	.for_each(|txt| if let Err(_) = dict.parse(&txt) {
		eprintln!("Could not parse a file");
	});

	//  The Dictionary Display impl and stats() function both render terminating
	//  newlines, and can be printed directly without println!
	print!("{}{}", dict, dict.stats());
}
