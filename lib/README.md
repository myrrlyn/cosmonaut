# Cosmonaut Parser

The Cosmonaut parser crate builds COSMOS message structures from source text. It
has no built-in knowledge of any COSMOS frameworks, and is designed to be as
internally stateless as possible. The API provides a state-accumulating type for
the client, into which COSMOS text is parsed.

The parser exposes all of its structure types for client use, and clients can
choose to use the Dictionary API or use its components manually. The former is
recommended as the easiest strategy.

The parser is built on the [`nom`] library, and all parse functions within the
crate can be used with other `nom`-based projects if desired.

This crate ships with a book describing the COSMOS grammar and how it is parsed,
that can be built with [`mdBook`].

## Dictionary API

The recommended API is driven by the `Dictionary` type. The client builds a
blank dictionary, and then calls its `.parse` method with source text. The
`.parse` method expects an `&str`, which is UTF-8 text; files loaded in other
encodings must be converted by the client before being parsed.

The dictionary accumulates a set of named “targets”, which are channels
associated with a specific device or network, each of which contains command and
telemetry messages. The COSMOS grammar supports patching with the `SELECT_`
prefix, and parsed patches are applied as soon as a matching base structure is
discovered.

### Structure

The dictionary is a nested map of named targets, each of which has maps of named
structures. Each level implements `Index`, so known names can be reached with
the `[]` operator, but `.get()` is preferred as it returns `Option`.

The `Dictionary` can fetch specific `Target` structures by name, with

```rust
Dictionary::get(&self, name: &str) -> Option<&Target>;
Dictionary[&str] -> Target;
```

or it can iterate over all targets, yielding a tuple of the name (which is
borrowable as `&str`[^1]) and a reference to the `Target` collection.

### Target

A `Target` structure is not directly indexable, because it has two collections:
one of `Command` and one of `Telemetry`. (Patch structures are not directly
selectable, and are only seen in the public API when printing.) A `Command` or
`Telemetry` can be queried directly with

```rust
Target::cmd(&self, name: &str) -> Option<&Command>;
Target::tlm(&self, name: &str) -> Option<&Telemetry>;
```

The collections of all `Command`s or all `Telemetry`s can be acquired with

```rust
Target::all_cmd(&self) -> &C<K, Command>;
Target::all_tlm(&self) -> &T<K, Telemetry>;
```

where the return types `C` and `T` are some collection (the exact type is
subject to change, and not important) of `Command` or `Telemetry` with the
packet’s name being some type `K` which can be borrowed as `&str`[^1].

The collections `C` and `T` implement `Index<&str>` so that they can be queried
for a named packet with `[]`, or with `.get()` for a fallible lookup.

### Packet

`Command` and `Telemetry` are themselves composite structures, and carry some
metadata as well as their primary payload: a list of `Parameter` or `Item`
fields, respectively. `Command` and `Telemetry` dereference to slices of their
field types, which means they can index their fields by name or iterate over
them in sequence.

### Field

`Parameter` and `Item` fields are a collection of raw data, all of which is
exposed for viewing. They implement indexing of, but do not dereference to,
their list of `State` elements.

## Examples

### Parsing

This example will load and parse the COSMOS files supplied in the `assets/`
folder of this repository.

```rust
extern crate cosmonaut as parser;

fn parse() -> Dictionary
    //  Import the main type for named use.
    use parser::Dictionary;
    //  The source files are in the filesystem.
    use std::fs::File;
    use std::io::Read;

    let mut dict = Dictionary::new();
    for path in &[
        "assets/patch.cosmos",
        "assets/cmd.cosmos",
        "assets/tlm.cosmos",
    ] {
        let mut s = String::new();
        let mut f = File::open(path).expect("filesystem error");
        f.read_to_string(&mut s).expect("read error");
        dict.parse(&s).expect("parse error");
        print!("{}", dict.stats());
    }
    dict
}
```

The output from this command should be similar to:

```text
Targets: 1
----------
Target TARGET with 0 packet(s):

Targets: 1
----------
Target TARGET with 4 packet(s):
 CMD | COLLECT_DATA with 9 parameter(s)
 CMD | EXAMPLE with 8 parameter(s)
 CMD | NOOP with 8 parameter(s)
 CMD | SETTINGS with 7 parameter(s)

Targets: 1
----------
Target TARGET with 5 packet(s):
 CMD | COLLECT_DATA with 9 parameter(s)
 CMD | EXAMPLE with 8 parameter(s)
 CMD | NOOP with 8 parameter(s)
 CMD | SETTINGS with 7 parameter(s)
 TLM | HS with 19 item(s)
```

The first file loaded, `patch.cosmos` does not have any packet text but does
have patches, so after it is parsed the dictionary has a target with patch
structures but no packet structures.

After the second file, `cmd.cosmos`, parses, the four commands it contains are
inserted into the dictionary and the previously-parsed patches are applied.

After the third file, `tlm.cosmos`, parses, the telemetry it contains is
inserted into the dictionary and the previously-parsed patches are applied.

The dictionary can be printed as normalized COSMOS text, with

```rust
println!("{}", dict);
```

and in fact the `stats` example application demonstrates this by loading asset
COSMOS files, parsing them, and printing the parsed result and dictionary
information. Run it with `cargo run -p cosmonaut --example stats`!

### Querying

Given a dictionary, the client can query into it using index or accessor methods
to narrow the collection to specific elements.

This code will use the dictionary parsed above.

```rust
extern crate cosmonaut as parser;
fn parse() -> parser::Dictionary;

fn query() {
    use parser::{
        Dictionary,
        Target,
        Command,
        Telemetry,
        Parameter,
        Item,
        decorators::State,
    };

    let dict: Dictionary = parse();
    let target: &Target = dict["TARGET"];

    let noop: &Command = target.all_cmds()["NOOP"];
    let example: &Command = target.cmd("EXAMPLE").expect("no such command");
    assert_eq!(example.len(), 8);
    assert_eq!(noop.len(), 8);

    let example_header: &Parameter = example["HEADER"];
    let noop_dummy: &Parameter = noop.param("DUMMY").expect("no such param");

    //  Everything has an Index impl if you're willing to risk a panic
    let deep_query: &State<Parameter> = &dict
        ["TARGET"]      //  Dict      >-name-> Target
        .all_cmds()     //  Target    >------> [Command]
        ["SETTINGS"]    //  [Command] >-name-> Command
        ["OPCODE"]      //  Command   >-name-> Parameter
        ["DIAGNOSTIC"]  //  Parameter >-name-> State
    ;

    let _: &Telemetry = target.all_tlms()["HS"];
    let hs: &Telemetry = target.tlm("HS").expect("no such telemetry");
    assert_eq!(hs.len(), 19);

    let hs_mode: &Item = hs["MODE"];
    let hs_angle: &Item = hs.item("ANGLEDEG").expect("no such item");
}
```

## Internals

The parser exposes almost all of its types at the root level for client projects
to use as they wish, rather than requiring all access to be through the
Dictionary API. All exposed types implement an inherent `.parse` method, as well
as implement the `Parsed` trait (until `impl Trait` stabilizes).

### Major Types

The four major type families are `Command`, `Telemetry`, `Parameter`, and
`Item`. Each has two associated types as part of their implementations of the
`Bodied` and `Selectable` traits, which manage child elements and the `SELECT_`
keyword variants, respectively.

The `Kind` and `KindVal` enums manage field data type and values, respectively,
and are used as the carriers of all data in COSMOS text that does not have its
type statically enforced by the grammar. `KindVal` instances can have their data
type transformed using `.cast` or `.cast_in_place`, with the caveat that casting
may not be the behavior you expect, and is allowed to silently fail or panic.
The `kind` module has more documentation.

### Minor Types

`Header`, `Endian`, `Macro`, and the `decorators` module hold minor types that
are encountered in the grammar but are not particularly interesting on their
own.

### Parse Utility Functions

The `parse_utils` module holds a collection of leaf functions that are the base
processors of COSMOS text, and are the atoms of which structure parsing is
composed.

All parser functions in this library take a `nom::types::CompleteStr` source
handle, which is a wrapper over Rust `&str` to denote that the source text is
fully loaded. The parsers do not currently handle parsing from a stream,
although this restriction may be lifted in the future.

The return type of all parser functions is `cosmonaut::ParseResult<T>`,
which partially completes `nom`’s `nom::IResult` type to use `CompleteStr` as
the source type. Any use of these functions will require these two types to
handle their input and output.

### Traits

A number of traits are used to generalize parsing or to reduce the number of
concrete types that must be instantiated in order to cover the full set of
COSMOS grammar permutations. The most prominent is `Keyword`, which must be
implemented by any type corresponding to a keyword element in the grammar.

- `Named`, `Described`, and `Targeted` permit generalized querying of a
  structure for its meta information
- `Bodied` permits deduplicating child element logic between standard and
  `SELECT_`-variant structures;
- `Stateful` allows `Parameter` and `Item` to share logic for the `STATE`
  keyword
- `Parsed` is a placeholder trait until `impl Trait` stabilizes; it is used to
  return a `Box`ed structure from generic parsers in contexts where the return
  value is guaranteed to have only one type, but *which* specific type it is
  cannot be known statically.

See the interior of `cosmos_macro::Macro::parse` for an example – all types `T`
that can instantiate a `Macro` are guaranteed to have an inherent `.parse`
method, but because generic code cannot call inherent methods, the `T: Parsed`
bound is added and `<T as Parsed>::parse` is called to return a
`Box<T> as Box<Parsed>`. Because `Parsed::parse` specifies that it returns a
`Box<Self>` and not `Box<Parsed>`, it is sound to immeditately dereference
`Box<T as Parsed>` into `T` even though `T` cannot be named at the call site,
use an inherent method, nor return an immediate value of `T`.

## Library Details

I am using features which are still (for now) only available on the nightly
compiler: external documentation and nested include paths. Once these stabilize,
and both are approaching this, `cosmonaut` will cease using nightly features and
work on stable.

- [`feature(external_doc)`][external_doc] is unstable.
- [`feature(non_modrs_mods)`][non_modrs_mods] is unstable, but also unused – I
    am using `#[path]` annotations until this feature stabilizes.

Minimum Rust version: `nightly-2018-03-16`.

### Future Goals

- Make an iterator adapter that consumes source material, either an in-memory
    string or a stream source, and emits parsed items on demand.

- Make parse failures contain more contextual information.

    At present, failures simply unwind the parse tree, eventually exiting
    without having advanced the cursor at all if no alternate branch succeeded.
    This is not good for debugging flawed source text, though the unwind is
    mitigated by the fact that elements are generally granular enough that an
    error will generally cause an incomplete return rather than nothing.

- Comprehensive coverage of the COSMOS grammar, using the COSMOS documentation
    and source code, as well as samples pulled from production, for a guide.

[^1]: The dictionary uses string caching to reduce the number of allocations of
    identical text, and so the exact type of the name is not part of the public
    API. It will always be able to be cast `as &str`.

[`mdBook`]: https://github.com/rust-lang-nursery/mdBook
[`nom`]: https://docs.rs/nom
[external_doc]: https://doc.rust-lang.org/unstable-book/language-features/external-doc.html
[non_modrs_mods]: https://doc.rust-lang.org/unstable-book/language-features/non-modrs-mods.html
