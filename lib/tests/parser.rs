/*! Full integration tests of the COSMOS parser.
!*/

#[macro_use]
extern crate assert_matches;
extern crate cosmonaut;
extern crate nom;

use cosmonaut::{
	Dictionary,
	Target,
	command::Command,
	decorators::{
		SegmentedPolyConversion,
		State,
	},
	item::Item,
	kind::{
		KindVal,
		Variant,
	},
	parameter::Parameter,
	telemetry::Telemetry,
	traits::{
		Described,
		Named,
		Parsed,
	},
};
use nom::types::CompleteStr;
use std::{
	fs::File,
	io::{
		self,
		Read,
		Write,
	},
};

fn get(path: &str) -> io::Result<String> {
	let mut f = File::open(&format!("assets/{}.cosmos", path))?;
	let mut out = String::with_capacity(f.metadata()?.len() as usize);
	f.read_to_string(&mut out)?;
	Ok(out)
}

#[test]
fn parse_command_file() {
	let source = get("cmd").unwrap();
	let text: CompleteStr = (&source as &str).into();

	let (rem, c1) = Command::parse(text).unwrap();
	let (rem, c2) = Command::parse(rem).unwrap();
	let (rem, c3) = Command::parse(rem).unwrap();
	let (rem, c4) = Command::parse(rem).unwrap();

	//  <Command as Named>
	assert_eq!(c1.name(), "COLLECT_DATA");
	//  <Command as Described>
	assert_matches!(c1.description(), Some("Commands my target to collect data"));
	assert_eq!(c1.parameters().len(), 9);

	//  <Command as Index<usize>>
	let p: &Parameter = &c1[3];
	assert_matches!(p.variant, Variant::Ident);
	assert_matches!(p.default, Some(KindVal::Uint(100)));

	//  <Command as Index<&str>>
	let p: &Parameter = &c1["MODE"];
	let s: &State<Parameter> = &p["DIAG"];
	assert_matches!(s.value, KindVal::Uint(1));
	assert!(s.has_extra());
	assert_matches!(
		s.extra().unwrap().description(),
		Some("Diagnostic mode consumes extra power")
	);

	assert_eq!(c2.name(), "NOOP");

	assert_eq!(c3.name(), "SETTINGS");

	assert_eq!(c4.name(), "EXAMPLE");
	assert!(*c4["OPCODE"].body.required);

	assert_eq!(rem.trim().lines().count(), 0);
}

#[test]
fn parse_telemetry_file() {
	let source = get("tlm").unwrap();
	let text: CompleteStr = (&source as &str).into();

	let (rem, tlm) = Telemetry::parse(text).unwrap();

	assert_eq!(tlm.name(), "HS");
	assert_matches!(tlm.description(), Some("Health and Status for My Target"));

	let i: &Item = &tlm[3];
	assert_matches!(i.variant, Variant::Ident);
	assert_matches!(i.offset, Some(5));
	assert_eq!(i.width, 11);
	assert_matches!(i.default, Some(KindVal::Uint(102)));

	let i: &Item = &tlm["CCSDSSEQFLAGS"];
	assert_eq!(i.states().len(), 4);

	let i: &Item = &tlm["ANGLEDEG"];
	let t: &Vec<SegmentedPolyConversion<Item>> = &i.body.transforms;
	let p: &SegmentedPolyConversion<Item> = &t.get(0).unwrap();
	// Dereference-----\
	// as PolyConv<T>-\|
	// as [f64]------\||
	// and reborrow-vvvv
	let c: &[f64] = &***p;
	assert_eq!(c.len(), 2);
	assert_eq!(c[0], 0.0);
	assert_eq!(c[1], 57.295);

	//  Macro-expanded items
	let is: &[Item] = tlm.items();
	let m: &[Item] = &is[12 .. 17];
	assert_eq!(m[0].description(), Some("SETTING 1"));
	assert_eq!(m[1].description(), Some("SETTING 2"));
	assert_eq!(m[2].description(), Some("SETTING 3"));
	assert_eq!(m[3].description(), Some("SETTING 4"));
	assert_eq!(m[4].description(), Some("SETTING 5"));

	//  Not all decorators are implemented yet.
	//  This number should gradually approach 0.
	assert_eq!(rem.trim().lines().count(), 0);
}

#[test]
fn parse_into_dict() {
	let mut out = File::create("../target/target.txt").unwrap();
	let mut text = get("cmd").unwrap();
	text.push_str(&get("tlm").unwrap());

	let mut dict: Dictionary = Dictionary::new();

	let p = get("patch").unwrap();
	dict.parse(&p).unwrap();

	assert!(dict.target("TARGET").unwrap().patches_pending());

	write!(out, "{:#?}\n", dict.target("TARGET").unwrap()).unwrap();

	dict.parse(&text).unwrap();

	assert!(!dict.target("TARGET").unwrap().patches_pending());

	write!(out, "{:#?}\n", dict).unwrap();

	let tgt: &Target = dict.target("TARGET").unwrap();

	let cmds = tgt.all_cmd();

	let cmd: &Command = &cmds["NOOP"];

	assert_eq!(cmd.description(), Some("Does nothing"));
}
