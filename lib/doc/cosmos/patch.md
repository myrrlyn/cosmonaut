Patch Structures

# Design Overview

All four structural grammars (`COMMAND`, `TELEMETRY`, `PARAMETER`, and `ITEM`)
support a `SELECT_` variant grammar which, rather than building a new element,
selects an element defined elsewhere and edits it.

Because the `SELECT_` variants are all followed by the same grammar productions
as the main grammars, the edit is performed by collecting all the child
productions into the appropriate instance of `Bodied::Body` and waiting until
the patch is overlaid on the original structure.

Patches for `COMMAND` and `TELEMETRY` require a target/name tuple in order to
resolve the structure on which they will be overlaid. Patches for `ITEM` and
`PARAMETER` need only the name.

Application of multiple patches is performed in chronological order of
discovery, so the most recently parsed patch structure has highest precedence.

Application of a patch is not a replacement of the pre-existing body; the
patch's elements are evaluated individually and only applied if they exist. A
`None` in a patch cannot replace a `Some` in a target body.
