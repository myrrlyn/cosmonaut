COSMOS Endianness

COSMOS packets, and fields within them, must have their endianness types
explicitly set. Fields are permitted to be smaller than bytes, and to cross byte
boundaries, so it is generally assumed that the endianness descriptor refers to
the order of *bits in a field*, not the order of *bytes*.

This is an important distinction to make, since there are systems (for example,
the x86 processor) where BIT order is big-endian but BYTE order is
little-endian.

# Grammar Productions

An Endian token must be one of the following two strings:

- `BIG_ENDIAN`
- `LITTLE_ENDIAN`

# Effects on Binary Messages

For any field of width `n` and starting index `i` (units are bits, not bytes),
the two endian tokens mean the following layout in memory:

```text
Big-endian:    |n - 1|n - 2|n - 3|...|    2    |    1    |    0    |
Little-endian: |  0  |  1  |  2  |...|  n - 3  |  n - 2  |  n - 1  |
Address:       |  i  |i + 1|i + 2|...|i + n - 3|i + n - 2|i + n - 1|
```

That is, a field with starting index `i` that is `n` bits wide will run from bit
`i` to bit `i + n - 1` in memory. In big-endian layout, the most significant bit
(bit `n - 1`) is at the lowest-numbered address, and the least significant bit
(bit `0`) is at the highest-numbered address. In little-endian layout, the most
significant bit is at the highest-numbered address, and the least significant
bit is at the lowest-numbered address.
