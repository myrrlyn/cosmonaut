# COSMOS Internal Traits

These traits provide ways to annotate the various COSMOS internal data
structures with functionality that can easily be made uniform across the various
types.

The most-used trait, `Keyword`, is just a marker with a single associated
constant (the COSMOS keyword used to select a parser). It provides default
methods that can detect the keyword immediately, or advance through the text
until it finds an instance. The latter behavior is useful for resuming parsing
when encountering invalid text.

> Note: currently, the `seek_to` function will match on ANY instance of the
> keyword text, including when in an invalid token slot (such as inside a
> quote-enclosed string). This is probably not an actual problem, but I'll work
> on making the seek function able to treat quoted strings as atoms.

This trait is very useful for templating parsers which act on the same text, but
are used in different type contexts, so that the child parser can be invoked
with awareness of the keyword it needs to use for part of its work.

For example:

```rust
# #[macro_use]
# extern crate assert_matches;
extern crate cosmonaut;
use cosmonaut::{
    item::Item,
    kind::Variant,
    parameter::Parameter,
    traits::Keyword,
};

# fn main() {
# assert_matches!(Variant::parse::<Parameter>("APPEND_ID_PARAMETER".into()), Ok((_, (true, Variant::Ident))));
let (_, (a, v)) = Variant::parse::<Parameter>("APPEND_ID_PARAMETER".into()).unwrap();
println!("Append: {}, Variant: {}", a, v);
//> "Append: true, Variant: ID_"
# let rendered = format!("Append: {}, Variant: {}", a, v);
# assert_eq!(rendered, "Append: true, Variant: ID_");
# }
```
