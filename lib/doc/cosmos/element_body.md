Element Bodies

# Design Overview

All COSMOS text blocks consist of a header line followed by zero or more body
lines. The top-level structures `COMMAND` and `TELEMETRY` must have at least one
body line, but the component structures `PARAMETER` and `ITEM` do not require a
body.

The overall process of parsing body text and accumulating body items is common
to all the structures, and shared between the structures proper and their
`SELECT_` variants, so it is factored out into the [`Bodied`][0] trait.

Each major structure type implements this trait by declaring a `Bodied::Body`
type (which I have conventionally named `Body` as well) that holds the child
structures which can be parsed out of all the valid grammar productions
following the structure's own grammar.
