Units of Measurement

The COSMOS specification allows units to be attached to a parameter value so
that the interpreting application can provide context and/or additional
computational resources.

Units are attached as decorations below a parameter line, and are of the form

```cosmos
UNITS Name Symbol
```

Correct processing of each unit kind is left to the interpreting application.
COSMOS units are merely textual decoration, and carry no intrinsic awareness of
mathematical transformation or meaning.

# Provided Units

The following common units are provided by Cosmonaut:

## Time

- [nanosecond](constant.NANOSECOND.html)
- [microsecond](constant.MICROSECOND.html)
- [millisecond](constant.MILLISECOND.html)
- [second](constant.SECOND.html)
- [minute](constant.MINUTE.html)
- [hour](constant.HOUR.html)
- [day](constant.DAY.html)
- [week](constant.WEEK.html)
- [year](constant.YEAR.html)

## Distance

- [micrometer](constant.MICROMETER.html)
- [millimeter](constant.MILLIMETER.html)
- [centimeter](constant.CENTIMETER.html)
- [meter](constant.METER.html)
- [kilometer](constant.KILOMETER.html)
- [megameter](constant.MEGAMETER.html)
- [gigameter](constant.GIGAMETER.html)

## Temperature

- [Celsius](constant.CELSIUS.html)
- [Fahrenheit](constant.FAHRENHEIT.html)
- [Kelvin](constant.KELVIN.html)
- [Rankine](constant.RANKINE.html)

## Frequency

- [nanohertz](constant.NANOHERTZ.html)
- [microhertz](constant.MICROHERTZ.html)
- [millihertz](constant.MILLIHERTZ.html)
- [hertz](constant.HERTZ.html)
- [kilohertz](constant.KILOHERTZ.html)
- [megahertz](constant.MEGAHERTZ.html)
- [gigahertz](constant.GIGAHERTZ.html)

## Information

- [bit](constant.BIT.html)
- [byte](constant.BYTE.html)
- [kilobit](constant.KILOBIT.html)
- [kibibit](constant.KIBIBIT.html)
- [kilobyte](constant.KILOBYTE.html)
- [kibibyte](constant.KIBIBYTE.html)
- [megabit](constant.MEGABIT.html)
- [mebibit](constant.MEBIBIT.html)
- [megabyte](constant.MEGABYTE.html)
- [mebibyte](constant.MEBIBYTE.html)
- [gigabit](constant.GIGABIT.html)
- [gibibit](constant.GIBIBIT.html)
- [gigabyte](constant.GIGABYTE.html)
- [gibibyte](constant.GIBIBYTE.html)

## Data Rate

- [baud](constant.BAUD.html)
- [kilobaud](constant.KILOBAUD.html)
- [megabaud](constant.MEGABAUD.html)
- [gigabaud](constant.GIGABAUD.html)

## Power

- [decibel](constant.DECIBEL.html)
- [decibel-milliwatt](constant.DECIBEL_M.html)
- [Bel](constant.BEL.html)
- [Bel-Watt](constant.BEL_W.html)
