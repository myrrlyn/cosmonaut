# COSMOS Field Types

The fields in a COSMOS message have variations that affect their carried data
and the way they are processed.

## Data Types

COSMOS messages are composed of one or more fields which have statically-known
data types. Each field can hold one of:

- signed integer (`INT`)
- unsigned integer (`UINT`)
- floating-point number (`FLOAT`)
- a derived value (`DERIVED`) which is not present in the message`s byte form
- a text string (`STRING`)
- a binary sequence (`BLOCK`)

## Field Kinds

In addition to the above data payload, each field also has a meta type, which
can be one of:

- a normal, single, data element
- an identifier which is used to match on byte sequences and select the correct
    deserializer
- an array of multiple data elements

## Implementation Details

Rust supports dynamic typing with the [`Any`] trait, but this requires that the
actual type be behind a layer of indirection. It is probably possible to get an
`&Any` reference to objects on the stack, but since I don't know how to make
that work or limit the domain of types over which to operate, I elected to use
an enum.

This has the followup problem of requiring two discrete but coupled enums, one
to *name* the type and one to *hold* the type. The reason for this is that the
Item type does not hold any values, and thus requires a bare marker in addition
to the value-carrying enumeration used on Parameters.

[`Any`]: //doc.rust-lang.org/std/any/trait.Any.html
