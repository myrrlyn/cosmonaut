Block Header

A block header is the text immediately after the starting token for a `COMMAND`
or `TELEMETRY` block. It has the form:

```text
$target $packet $endian $description?
```

where `$target` and `$packet` are bare, single, words that are interpreted by
the [`word`][0] parser. Since `word` does not perform any semantic analysis of
the text on which it operates, the token `"first second"` will be parsed as
`($target: '"first', $packet: 'second")`.

The `$endian` element must be one of the two [`Endian`][1] tokens.

The `$description` element is an optional [double-quoted][2] string. The
reference documents only ever show or discuss double-quote, and I have not yet
seen any single-quote descriptions. At present, only double-quote is permitted,
but I may expand the parse here to include single-quote strings here as well.
Bare words are expressly NOT allowed.

[0]: ../parse_utils/fn.word.html
[1]: ../endian/struct.Endian.html
[2]: ../parse_utils/fn.dquote_escape.html
