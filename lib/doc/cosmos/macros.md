`MACRO_APPEND` Blocks

Repetitive elements can be generated programmatically rather than hand-written
by using the `MACRO_APPEND_START` and `MACRO_APPEND_END` keywords. These delimit
a set of one or more elements that can include a single replacement marker.

The grammar looks like this:

```text
MACRO_APPEND_START $start $stop
$elem+
MACRO_APPEND_END
```

where `$start` and `$stop` are values which can delineate a range. At present,
this means only integers. If I encounter values in use that are strings (since
the reference implementation is in Ruby, and Ruby supports ranges of strings,
this is likely *technically* possible even if undocumented), then I will have to
expand the range generator to support this.

The `$elem+` element must be at least one syntactically valid element which can
be repeated. This element may contain the token `#x` at any point within it.
This token will be replaced with the iteration cursor during macro expansion.
The element text after replacement must remain valid.

# Examples

```text
MACRO_APPEND_START 1 5
  APPEND_ITEM SETTING_#x 16 UINT "Setting ##x"
MACRO_APPEND_END
```

This parses into a `Macro<Item>` structure, whose expansion is equivalent to the
results of parsing the following text:

```text
  APPEND_ITEM SETTING_1 16 UINT "Setting #1"
  APPEND_ITEM SETTING_2 16 UINT "Setting #2"
  APPEND_ITEM SETTING_3 16 UINT "Setting #3"
  APPEND_ITEM SETTING_4 16 UINT "Setting #4"
  APPEND_ITEM SETTING_5 16 UINT "Setting #5"
```

The `#x` token is replaced by a linear scan that does not backtrack, so it is
impossible to escape `\#x` in a manner that will cause a literal `#x` to appear
in the expanded text.
