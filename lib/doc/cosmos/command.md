# `COMMAND` Blocks

`COMMAND` blocks describe control messages emitted by one node, generally human
operated, wishing to instruct another node to perform an action. These blocks
represent one of COSMOS’s two root-level elements, and are one of the four
root-level text structures in message definition files.

Each `COMMAND` block must have a block header carrying information about the
command message itself, followed by one or more `Parameter` blocks that describe
the semantic and binary construction of the resulting message packet. `COMMAND`
blocks may also have zero or more decorator elements.

## `COMMAND` Grammar

<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" contentScriptType="application/ecmascript" contentStyleType="text/css" height="173px" preserveAspectRatio="xMidyMid" style="width:100%;height:auto;" version="1.1" viewBox="0 0 823 173" width="823px" zoomAndPan="magnify"><defs><filter height="300%" id="f1ci30c4efdwim" width="300%" x="-1" y="-1"><feGaussianBlur result="blurOut" stdDeviation="2.0"/><feColorMatrix in="blurOut" result="blurOut2" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .4 0"/><feOffset dx="4.0" dy="4.0" in="blurOut2" result="blurOut3"/><feBlend in="SourceGraphic" in2="blurOut3" mode="normal"/></filter></defs><g><ellipse cx="16" cy="85.5" fill="#000000" filter="url(#f1ci30c4efdwim)" rx="10" ry="10" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f1ci30c4efdwim)" height="67.7969" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="78" x="661" y="51.5"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="661" x2="739" y1="79.1094" y2="79.1094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="32" x="684" y="71.0332">Child</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="58" x="666" y="96.5664">Parameter</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="43" x="666" y="111.6602">Modifier</text><ellipse cx="802" cy="85.5" fill="none" filter="url(#f1ci30c4efdwim)" rx="10" ry="10" style="stroke: #000000; stroke-width: 1.0;"/><ellipse cx="802.5" cy="86" fill="#000000" rx="6" ry="6" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f1ci30c4efdwim)" height="154.6094" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="495" x="130.5" y="8"/><rect fill="#FFFFFF" height="121" rx="12.5" ry="12.5" style="stroke: #FFFFFF; stroke-width: 1.0;" width="489" x="133.5" y="38.6094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="130.5" x2="625.5" y1="35.6094" y2="35.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="46" x="355" y="27.5332">Header</text><ellipse cx="151.5" cy="113.6094" fill="#000000" filter="url(#f1ci30c4efdwim)" rx="10" ry="10" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f1ci30c4efdwim)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="62" x="179.5" y="87.1094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="179.5" x2="241.5" y1="114.7188" y2="114.7188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="42" x="189.5" y="106.6426">Target</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="184.5" y="132.1758">word</text><rect fill="#FEFECE" filter="url(#f1ci30c4efdwim)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="56" x="259.5" y="87.1094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="259.5" x2="315.5" y1="114.7188" y2="114.7188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="36" x="269.5" y="106.6426">Name</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="264.5" y="132.1758">word</text><rect fill="#FEFECE" filter="url(#f1ci30c4efdwim)" height="67.7969" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="111" x="334" y="79.6094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="334" x2="445" y1="107.2188" y2="107.2188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="44" x="367.5" y="99.1426">Endian</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="72" x="339" y="124.6758">BIG_ENDIAN</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="91" x="339" y="139.7695">LITTLE_ENDIAN</text><ellipse cx="582.5" cy="113.6094" fill="none" filter="url(#f1ci30c4efdwim)" rx="10" ry="10" style="stroke: #000000; stroke-width: 1.0;"/><ellipse cx="583" cy="114.1094" fill="#000000" rx="6" ry="6" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f1ci30c4efdwim)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="91" x="463" y="87.1094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="463" x2="554" y1="114.7188" y2="114.7188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="71" x="473" y="106.6426">Description</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="32" x="468" y="132.1758">" text "</text><!--link *start*CHeader to Target--><path d="M161.6406,113.6094 C165.756,113.6094 169.8713,113.6094 173.9867,113.6094 " fill="none" id="*start*CHeader-Target" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="179.1995,113.6094,170.1995,109.6094,174.1995,113.6094,170.1995,117.6094,179.1995,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Target to Name--><path d="M241.781,113.6094 C245.883,113.6094 249.985,113.6094 254.087,113.6094 " fill="none" id="Target-Name" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="259.283,113.6094,250.283,109.6094,254.283,113.6094,250.283,117.6094,259.283,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Name to Endian--><path d="M315.789,113.6094 C320.135,113.6094 324.481,113.6094 328.827,113.6094 " fill="none" id="Name-Endian" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="333.929,113.6094,324.929,109.6094,328.929,113.6094,324.929,117.6094,333.929,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Endian to *end*CHeader--><path d="M428.572,79.5774 C439.219,72.2218 451.224,65.4914 463.5,61.6094 C502.062,49.4153 519.778,40.8704 554.5,61.6094 C567.748,69.5224 575.096,86.1034 578.903,98.4259 " fill="none" id="Endian-*end*CHeader" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="580.325,103.4086,581.7024,93.6565,578.9533,98.6004,574.0093,95.8513,580.325,103.4086" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Endian to Description--><path d="M445.281,113.6094 C449.333,113.6094 453.384,113.6094 457.436,113.6094 " fill="none" id="Endian-Description" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="462.568,113.6094,453.568,109.6094,457.568,113.6094,453.568,117.6094,462.568,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Description to *end*CHeader--><path d="M554.172,113.6094 C558.52,113.6094 562.868,113.6094 567.216,113.6094 " fill="none" id="Description-*end*CHeader" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="572.32,113.6094,563.32,109.6094,567.32,113.6094,563.32,117.6094,572.32,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link *start to CHeader--><path d="M26.0598,85.5 C43.2674,85.5 80.8327,85.5 125.282,85.5 " fill="none" id="*start-CHeader" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="130.451,85.5,121.451,81.5,125.451,85.5,121.451,89.5,130.451,85.5" style="stroke: #A80036; stroke-width: 1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="68" x="44.25" y="78.9951">COMMAND</text><!--link CHeader to Child--><path d="M625.789,85.5 C635.746,85.5 645.702,85.5 655.659,85.5 " fill="none" id="CHeader-Child" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="660.865,85.5,651.865,81.5,655.865,85.5,651.865,89.5,660.865,85.5" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Child to Child--><path d="M744.35,72.7712 C760.64,72.4105 774,76.6534 774,85.5 C774,95.2617 757.733,99.4181 739.211,97.9691 " fill="none" id="Child-Child" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="739.211,73.0309,748.4018,76.5706,744.2046,72.778,747.9972,68.5809,739.211,73.0309" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Child to *end--><path d="M739.047,85.5 C754.807,85.5 770.567,85.5 786.327,85.5 " fill="none" id="Child-*end" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="791.672,85.5,782.672,81.5,786.672,85.5,782.672,89.5,791.672,85.5" style="stroke: #A80036; stroke-width: 1.0;"/></g></svg>

`COMMAND` uses the same header structure that `TELEMETRY` does, shown above in
UML form.

The first token encountered must be the text `COMMAND`. It is then followed by
two words separated by whitespace. These must be bare words; they will not be
interpreted as quoted strings and all non-whitespace characters that appear will
be treated literally. The first token names the target to which the packet
belongs, and the second names the packet itself.

Following `COMMAND` and the two names, one of two tokens must appear:
`BIG_ENDIAN` or `LITTLE_ENDIAN`. These denote the default endianness of all
`Parameter` fields within the `COMMAND`.

If the next element after the endian token is a double-quoted string, it is
treated as the `COMMAND`’s description. If it is anything else, the header stops
and the parser moves on to attempting to parse child elements.

### Decorators

`COMMAND` blocks may have various decorations applied to them to add metadata
beyond that contained in the header.

Valid tokens here are:

- `SELECT_PARAMETER`

    This is valid to appear within a `COMMAND` but will generally be found on
    `SELECT_COMMAND`. It modifies a `PARAMETER` element also attached to this
    `COMMAND`. The `Parameter` element it targets need not appear above it in
    the source text, and need not even be present *in this block*.
    `SELECT_COMMAND` blocks may add new `Parameter` blocks which
    `SELECT_PARAMETER` can target.

    Abusing the permissiveness of non-locality is a good way to break things.

- `DISABLED`

    This decorator informs the application that this packet can never be sent
    over the network. The application *may* choose to display the packet in its
    UI, but should mark that it will not be sent, and any attempt by the user to
    send it *must* log an error message and refuse to transmit.

- `DISABLE_MESSAGES`

    This decorator suppresses the display of raw command packets in the UI when
    the command is sent, but does **not** disable the command entirely. It is
    solely a UI mute.

- `HAZARDOUS`

    `COMMAND` is one of two elements in the COSMOS grammar that can accept this
    decorator. The `HAZARDOUS` decorator marks the entire commadn as hazardous
    to transmit, and the application *must* request second confirmation from the
    user when this command is requested for transmit.

    `HAZARDOUS` *may* carry a double-quoted text message, which the application
    *must* render to the user in the warning displayed before the request for
    confirmation. The message *must not* be a bare word or a single-quoted text
    string. The former will attempt to begin a new element parse, and the latter
    is an error.

- `HIDDEN`

    This decorator hides the command from the UI, but does not prevent its use
    in the application.

- `META`

    Stores metadata about the current command. See the `META` documentation for
    more detailed information.

Note that the decorator tokens listed above may be interspersed with `PARAMETER`
blocks. I have listed them as separate elements to denote their semantic
differences, but there is not a required ordering between them.

## Behavior

`Command` collects `Parameter` elements into a list ordered by chronological
appearance in the source text. This list **cannot** be reordered, because
`PARAMETER` syntax can depend on previously-encountered `Parameter` elements.

`Command` implements `Deref` to `[Parameter]`, and also provides `Index` by
integer and text keys that look up `Parameter`s by position or name,
respectively. In addition to the trait methods, the inherent methods
`.parameters` and `.param(&str)` provide more explicit (and for `.param`,
non-panicking) access to the `Parameter` set.

## Serialization

`Command`'s implementation of `Display` is a normalized set of COSMOS text. This
can be written out to COSMOS files. Transformation to and from binary messages
is not implemented in this library.
