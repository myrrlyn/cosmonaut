# `PARAMETER` Blocks

`PARAMETER` blocks describe individual data fields within a `COMMAND` block.
These fields carry named, typed, data within the `COMMAND` message and describe
the binary layout of the packet as well as transformation between program-usable
data structures and the network transport packet.

Each `COMMAND` *must* have at least one `PARAMETER` object. The parser will
raise an error if it cannot find one before the block ends.

## Grammar

The `PARAMETER` grammar is the most complex part of the language, due to the
number of possible states. The diagram below shows the `PARAMETER` syntax rules.

<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" contentScriptType="application/ecmascript" contentStyleType="text/css" height="1236px" preserveAspectRatio="xMidyMid" style="width:100%;height:auto;" version="1.1" viewBox="0 0 727 1236" width="727px" zoomAndPan="magnify"><defs><filter height="300%" id="fox63j5ht7viq" width="300%" x="-1" y="-1"><feGaussianBlur result="blurOut" stdDeviation="2.0"/><feColorMatrix in="blurOut" result="blurOut2" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .4 0"/><feOffset dx="4.0" dy="4.0" in="blurOut2" result="blurOut3"/><feBlend in="SourceGraphic" in2="blurOut3" mode="normal"/></filter></defs><g><ellipse cx="268" cy="18" fill="#000000" filter="url(#fox63j5ht7viq)" rx="10" ry="10" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="56" x="6" y="122"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="6" x2="62" y1="149.6094" y2="149.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="36" x="16" y="141.5332">Name</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="11" y="167.0664">word</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="113" x="96.5" y="205"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="96.5" x2="209.5" y1="232.6094" y2="232.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="93" x="106.5" y="224.5332">Append_Name</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="101.5" y="250.0664">word</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="58" x="24" y="288"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="24" x2="82" y1="315.6094" y2="315.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="38" x="34" y="307.5332">Offset</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="13" x="29" y="333.0664">int</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="56" x="103" y="402"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="103" x2="159" y1="429.6094" y2="429.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="36" x="113" y="421.5332">Width</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="20" x="108" y="447.0664">uint</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="97.9844" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="73" x="245.5" y="531"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="245.5" x2="318.5" y1="558.6094" y2="558.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="53" x="255.5" y="550.5332">Primitive</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="19" x="250.5" y="576.0664">INT</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="28" x="250.5" y="591.1602">UINT</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="37" x="250.5" y="606.2539">FLOAT</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="53" x="250.5" y="621.3477">DERIVED</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="67.7969" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="87" x="80.5" y="546"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="80.5" x2="167.5" y1="573.6094" y2="573.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="67" x="90.5" y="565.5332">Composite</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="45" x="85.5" y="591.0664">STRING</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="41" x="85.5" y="606.1602">BLOCK</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="65" x="77.5" y="705"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="77.5" x2="142.5" y1="732.6094" y2="732.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="45" x="87.5" y="724.5332">Default</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="29" x="82.5" y="750.0664">value</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="75" x="291.5" y="705"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="291.5" x2="366.5" y1="732.6094" y2="732.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="55" x="301.5" y="724.5332">Minimum</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="29" x="296.5" y="750.0664">value</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="78" x="178" y="705"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="178" x2="256" y1="732.6094" y2="732.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="58" x="188" y="724.5332">Maximum</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="29" x="183" y="750.0664">value</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="98" x="329" y="122"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="329" x2="427" y1="149.6094" y2="149.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="78" x="339" y="141.5332">Array_Name</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="334" y="167.0664">word</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="155" x="451.5" y="205"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="451.5" x2="606.5" y1="232.6094" y2="232.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="135" x="461.5" y="224.5332">Append_Array_Name</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="456.5" y="250.0664">word</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="100" x="371" y="288"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="371" x2="471" y1="315.6094" y2="315.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="80" x="381" y="307.5332">Array_Offset</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="13" x="376" y="333.0664">int</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="50" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="115" x="393.5" y="403.5"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="393.5" x2="508.5" y1="431.1094" y2="431.1094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="95" x="403.5" y="423.0332">Element_Width</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="128.1719" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="111" x="395.5" y="516"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="395.5" x2="506.5" y1="543.6094" y2="543.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="91" x="405.5" y="535.5332">Element_Type</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="19" x="400.5" y="561.0664">INT</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="28" x="400.5" y="576.1602">UINT</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="37" x="400.5" y="591.2539">FLOAT</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="53" x="400.5" y="606.3477">DERIVED</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="45" x="400.5" y="621.4414">STRING</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="41" x="400.5" y="636.5352">BLOCK</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="98" x="402" y="705"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="402" x2="500" y1="732.6094" y2="732.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="78" x="412" y="724.5332">Array_Width</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="20" x="407" y="750.0664">uint</text><ellipse cx="413" cy="1215" fill="none" filter="url(#fox63j5ht7viq)" rx="10" ry="10" style="stroke: #000000; stroke-width: 1.0;"/><ellipse cx="413.5" cy="1215.5" fill="#000000" rx="6" ry="6" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="50" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="70" x="445" y="956.5"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="445" x2="515" y1="984.1094" y2="984.1094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="50" x="455" y="976.0332">Modifier</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="324.6094" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="183" x="226.5" y="819"/><rect fill="#FFFFFF" height="291" rx="12.5" ry="12.5" style="stroke: #FFFFFF; stroke-width: 1.0;" width="177" x="229.5" y="849.6094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="226.5" x2="409.5" y1="846.6094" y2="846.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="34" x="301" y="838.5332">Suffix</text><ellipse cx="310.6767" cy="869.6094" fill="#000000" filter="url(#fox63j5ht7viq)" rx="10" ry="10" style="stroke: none; stroke-width: 1.0;"/><ellipse cx="278.6767" cy="1118.6094" fill="none" filter="url(#fox63j5ht7viq)" rx="10" ry="10" style="stroke: #000000; stroke-width: 1.0;"/><ellipse cx="279.1767" cy="1119.1094" fill="#000000" rx="6" ry="6" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="91" x="265.1767" y="915.6094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="265.1767" x2="356.1767" y1="943.2188" y2="943.2188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="71" x="275.1767" y="935.1426">Description</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="32" x="270.1767" y="960.6758">" text "</text><rect fill="#FEFECE" filter="url(#fox63j5ht7viq)" height="67.7969" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="111" x="265.1767" y="1004.6094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="265.1767" x2="376.1767" y1="1032.2188" y2="1032.2188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="44" x="298.6767" y="1024.1426">Endian</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="72" x="270.1767" y="1049.6758">BIG_ENDIAN</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="91" x="270.1767" y="1064.7695">LITTLE_ENDIAN</text><!--link *start*Suffix to *end*Suffix--><path d="M301.9569,874.6294 C289.0344,881.1344 265.2922,895.3184 255.6767,915.6094 C225.7957,978.6654 239.3447,1004.7694 255.6767,1072.6094 C258.5529,1084.5569 265.1667,1096.9012 270.5672,1105.6318 " fill="none" id="*start*Suffix-*end*Suffix" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="273.4113,1110.0849,271.9378,1100.3469,270.7199,1105.871,265.1957,1104.6531,273.4113,1110.0849" style="stroke: #A80036; stroke-width: 1.0;"/><!--link *start*Suffix to Description--><path d="M310.6767,879.8264 C310.6767,887.6584 310.6767,899.3664 310.6767,910.4564 " fill="none" id="*start*Suffix-Description" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="310.6767,915.4924,314.6767,906.4924,310.6767,910.4924,306.6767,906.4924,310.6767,915.4924" style="stroke: #A80036; stroke-width: 1.0;"/><!--link *start*Suffix to Endian--><path d="M319.3964,874.6294 C332.3189,881.1344 356.061,895.3184 365.677,915.6094 C378.737,943.1694 365.482,975.6154 350.048,1000.1164 " fill="none" id="*start*Suffix-Endian" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="347.147,1004.5964,355.3958,999.2151,349.8642,1000.3991,348.6802,994.8676,347.147,1004.5964" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Description to Endian--><path d="M313.4092,968.9324 C314.3941,978.2394 315.5279,988.9534 316.6026,999.1094 " fill="none" id="Description-Endian" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="317.1581,1004.3584,320.1896,994.9877,316.6324,999.3861,312.234,995.8288,317.1581,1004.3584" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Endian to *end*Suffix--><path d="M302.8649,1072.6884 C296.7529,1084.0393 290.2719,1096.0753 285.5585,1104.8288 " fill="none" id="Endian-*end*Suffix" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="283.1354,1109.329,290.9243,1103.3013,285.506,1104.9267,283.8806,1099.5084,283.1354,1109.329" style="stroke: #A80036; stroke-width: 1.0;"/><!--link *start to Name--><path d="M257.784,18.95 C215.275,18.98 53.1697,21.5 22,58 C8.2975,74.05 12.3204,97.96 19.2178,117.03 " fill="none" id="*start-Name" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="21.0243,121.77,21.5472,111.935,19.2391,117.0996,14.0745,114.7914,21.0243,121.77" style="stroke: #A80036; stroke-width: 1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="81" x="32.5" y="72.4951">PARAMETER</text><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="100" x="23" y="88.8467">ID_PARAMETER</text><!--link *start to Append_Name--><path d="M257.809,19.18 C235.703,20.18 183.449,26 160,58 C129.774,99.25 136.727,162.13 144.728,199.49 " fill="none" id="*start-Append_Name" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="145.912,204.82,147.862,195.1661,144.8261,199.9393,140.0529,196.9035,145.912,204.82" style="stroke: #A80036; stroke-width: 1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="142" x="170.5" y="72.4951">APPEND_PARAMETER</text><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="161" x="161" y="88.8467">APPEND_ID_PARAMETER</text><!--link Name to Offset--><path d="M36.9688,175.12 C40.3331,204.16 45.8157,251.488 49.4157,282.562 " fill="none" id="Name-Offset" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="50.0208,287.785,52.9579,278.3843,49.4451,282.8183,45.0111,279.3054,50.0208,287.785" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Offset to Width--><path d="M70.8948,341.195 C82.5874,357.984 97.902,379.975 110.146,397.556 " fill="none" id="Offset-Width" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="113.188,401.925,111.325,392.2539,110.3297,397.8225,104.7611,396.8272,113.188,401.925" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Append_Name to Width--><path d="M150.079,258.394 C146.04,294.187 138.765,358.674 134.46,396.833 " fill="none" id="Append_Name-Width" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="133.898,401.809,138.8832,393.315,134.4594,396.8406,130.9338,392.4168,133.898,401.809" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Width to Primitive--><path d="M156.958,455.2 C180.28,478.29 214.894,512.561 241.795,539.194 " fill="none" id="Width-Primitive" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="245.37,542.733,241.7899,533.5579,241.8174,539.2146,236.1606,539.2421,245.37,542.733" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Width to Composite--><path d="M129.797,455.2 C128.697,478.68 127.057,513.721 125.801,540.536 " fill="none" id="Width-Composite" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="125.555,545.783,129.9723,536.9803,125.7893,540.7885,121.9811,536.6054,125.555,545.783" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Composite to Default--><path d="M120.893,614.179 C118.501,639.718 115.221,674.749 112.886,699.678 " fill="none" id="Composite-Default" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="112.403,704.837,117.2238,696.2487,112.8687,699.8587,109.2586,695.5036,112.403,704.837" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Primitive to Minimum--><path d="M297.13,629.126 C304.403,652.261 312.907,679.309 319.3,699.645 " fill="none" id="Primitive-Minimum" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="320.878,704.664,321.9966,694.8789,319.3794,699.8939,314.3644,697.2767,320.878,704.664" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Maximum to Minimum--><path d="M261.555,731.5 C271.459,731.5 281.364,731.5 291.269,731.5 " fill="none" id="Maximum-Minimum" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="256.375,731.5,265.375,735.5,261.375,731.5,265.375,727.5,256.375,731.5" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Default to Maximum--><path d="M147.821,731.5 C157.803,731.5 167.785,731.5 177.766,731.5 " fill="none" id="Default-Maximum" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="142.602,731.5,151.602,735.5,147.602,731.5,151.602,727.5,142.602,731.5" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Default to Suffix--><path d="M131.476,758.106 C153.346,784.182 188.805,826.46 222.964,867.188 " fill="none" id="Default-Suffix" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="226.266,871.124,223.5464,861.6581,223.0526,867.2933,217.4173,866.7995,226.266,871.124" style="stroke: #A80036; stroke-width: 1.0;"/><!--link *start to Array_Name--><path d="M277.187,22.79 C290.482,28.66 315.217,41.06 331,58 C346.881,75.05 358.93,98.48 366.851,117.09 " fill="none" id="*start-Array_Name" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="368.775,121.71,369.022,111.8642,366.8595,117.0914,361.6323,114.929,368.775,121.71" style="stroke: #A80036; stroke-width: 1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="133" x="356" y="80.4951">ARRAY_PARAMETER</text><!--link *start to Append_Array_Name--><path d="M277.939,19.22 C317.896,20.32 466.199,26.51 498,58 C535.453,95.09 536.414,160.96 533.12,199.69 " fill="none" id="*start-Append_Array_Name" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="532.654,204.74,537.4602,196.1435,533.1112,199.7609,529.4937,195.412,532.654,204.74" style="stroke: #A80036; stroke-width: 1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="194" x="521" y="80.4951">APPEND_ARRAY_PARAMETER</text><!--link Array_Name to Array_Offset--><path d="M384.719,175.12 C392.364,204.28 404.843,251.88 412.989,282.946 " fill="none" id="Array_Name-Array_Offset" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="414.258,287.785,415.8434,278.0646,412.9893,282.9486,408.1052,280.0946,414.258,287.785" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Array_Offset to Element_Width--><path d="M427.883,341.195 C432.468,358.314 438.502,380.841 443.255,398.587 " fill="none" id="Array_Offset-Element_Width" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="444.56,403.457,446.0968,393.7288,443.2672,398.627,438.3689,395.7973,444.56,403.457" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Append_Array_Name to Element_Width--><path d="M519.697,258.05 C511.58,280.113 499.356,312.794 488,341 C480.194,360.389 470.946,382.009 463.649,398.783 " fill="none" id="Append_Array_Name-Element_Width" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="461.646,403.38,468.9077,396.7266,463.643,398.7961,461.5734,393.5314,461.646,403.38" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Element_Width to Element_Type--><path d="M451,453.52 C451,469.042 451,490.091 451,510.512 " fill="none" id="Element_Width-Element_Type" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="451,515.687,455,506.687,451,510.687,447,506.687,451,515.687" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Element_Type to Array_Width--><path d="M451,644.401 C451,663.42 451,683.443 451,699.388 " fill="none" id="Element_Type-Array_Width" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="451,704.632,455,695.632,451,699.632,447,695.632,451,704.632" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Array_Width to Suffix--><path d="M437.268,758.106 C429.429,772.722 418.861,792.428 407.24,814.097 " fill="none" id="Array_Width-Suffix" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="404.745,818.75,412.5237,812.7092,407.1082,814.3437,405.4737,808.9281,404.745,818.75" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Suffix to *end--><path d="M384.368,1144.2284 C393.9,1167.4556 402.221,1187.7334 407.4,1200.3544 " fill="none" id="Suffix-*end" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="409.422,1205.2809,409.7057,1195.4361,407.5237,1200.6553,402.3046,1198.4733,409.422,1205.2809" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Suffix to Modifier--><path d="M409.758,981.5 C419.727,981.5 429.696,981.5 439.666,981.5 " fill="none" id="Suffix-Modifier" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="444.879,981.5,435.879,977.5,439.879,981.5,435.879,985.5,444.879,981.5" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Modifier to Modifier--><path d="M502.799,952.994 C525.259,937.048 550,946.55 550,981.5 C550,1018.625 522.084,1027.036 498.638,1006.733 " fill="none" id="Modifier-Modifier" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="498.638,956.267,508.1848,953.8465,502.5679,953.1757,503.2387,947.5587,498.638,956.267" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Modifier to *end--><path d="M472.956,1006.837 C458.995,1055.079 428.199,1161.4825 417.094,1199.8543 " fill="none" id="Modifier-*end" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="415.603,1205.0071,421.9476,1197.4741,416.9931,1200.2042,414.263,1195.2498,415.603,1205.0071" style="stroke: #A80036; stroke-width: 1.0;"/></g></svg>

Other than the `Modifier` elements, the `Parameter` is typically written on one
line.

### Keyword

`Parameter` blocks begin with one of six keywords:

- `PARAMETER` – an ordinary scalar value at a known offset
- `ID_PARAMETER` – a signature scalar value at a known offset
- `ARRAY_PARAMETER` – an array of one or more scalar values, at a known offset
- `APPEND_PARAMETER` – an ordinary scalar value, placed immediately after the
    end of the preceding parameter (or 0 if it is the first entry)
- `APPEND_ID_PARAMETER` – a signature scalar value, placed immediately after the
    end of the preceding parameter (or 0 if it is the first entry)
- `APPEND_ARRAY_PARAMETER` – an array of one or more scalar values, placed
    immediately after the end of the preceding parameter (or 0 if it is the
    first entry)

The keyword determines the syntax rules for the following items, as shown.
Appended parameters can elide the offset value, and scalar parameters must
specify their valid range (for numeric types) while arrays do not.

Ordinary and signature parameters have the same grammar; the only distinction is
that the “default value” state is also the signature value used for
identification, and the minimum and maximum values *should* be identical to the
default.

The parser will warn, but not error, when this occurs. It is the responsibility
of the application to handle this warning in a useful manner.

### Name

All parameters must have a name, which is one word.

### Offset

Non-appended parameters must specify their offset within the overall packet, in
bits. This must be a signed integer, and is allowed to be zero or negative. A
zero or positive offset count from the front end of the packet, and a negative
offset counts from the back end of the packet. For example,
`PARAMETER Foo -8 8 …` names the last byte in the packet.

### Width

After the offset (non-appended) or name (appended) component, the parameter must
specify the width in bits of its scalar value. For ordinary and signature
parameters, this is the only width component; for array parameters, this is the
width of one member of the array and is *not* the width of the entire parameter.

It must be an unsigned integer. A width of 0 is only valid for `DERIVED`,
`STRING`, and `BLOCK` types. `STRING` and `BLOCK` types use a 0 width as a
synonym for unlimited, and will have their entire in-memory value written into
the packet. A non-zero width will truncate values too long, and zero-fill values
too short.

Any named parameters appearing after a 0-width `STRING` or `BLOCK` parameter
*must* have negative offsets. `APPEND_` parameters are permissible behind a
variable-length parameter; their widths will calculate their offsets. It is not
permissible to have multiple 0-width parameters in the same command packet.

### Type

The next component for all parameter paths is the data type keyword. This must
be one of:

- `INT` – signed integer, from <math><msup><mn>-2</mn><mi><mi>n</mi><mo>-</mo>
    <mn>1</mn></mi></msup></math> through <math><msup><mn>2</mn><mi><mi>n</mi>
    <mo>-</mo><mn>1</mn></mi></msup> <mo>-</mo><mn>1</mn></math> where <math>
    <mi>n</mi></math> is the width specified above.
- `UINT` – unsigned integer, from <math><mn>0</mn></math> through <math><msup>
    <mn>2</mn><mi>n</mi></msup><mo>-</mo><mn>1</mn></math>.
- `FLOAT` – a floating point number in some encoding. COSMOS does not mandate
    the floating-point encoding or width, but IEEE754 32- and 64- bit are the
    most likely to be used.
- `DERIVED` – a value that is not present in the binary-serialized packet, but
    is named here and calculated later. It *should* have a size and offset of
    zero, and the parser will warn but not error on non-zero width or offset
    values. `DERIVED` parameters will *not* be counted when computing binary
    serialization, regardless of width and offset values.
- `STRING` – a run of bytes that may be interpreted as text. COSMOS does not
    mandate the encoding, but as a Rust library this parser will interpret all
    `STRING` parameters as UTF-8, which is a superset of 7-bit ASCII.
    Applications using incompatible encodings should attach a `META` attribute
    to the parameter. The attribute `META ENCODING IDENT`, where `IDENT` is some
    recognizable value, will instruct the binary serializer to use custom
    encodings. Known identifiers are `UTF-16BE`, `UTF-16LE`, `UTF32-BE`, or.
    `UTF32-LE`. Other identifiers will require a registered callback to
    transform between raw bytes and text.
- `BLOCK` – a run of bytes. It is the responsibility of the application to
    interpret and process these.

### Domain Bounds

For ordinary and signature parameters, the next one or three elements must be
text that is valid to parse as the indicated type.

For `INT`, `UINT`, `FLOAT`, and `DERIVED`, there must be three items: the
minimum allowed value, the maximum allowed value, and the default value.

> For signature parameters (`ID_PARAMETER` and `APPEND_ID_PARAMETER`), the
> default value is the identifying fingerprint used during pattern matching, and
> the minimum and maximum values *should* be the same value.

For `STRING` and `BLOCK`, there must only be one item: the default value.

- `STRING` values are `"`, text, `"`.
- `BLOCK` values are `'0x`, one or more hexadecimal digits (upper or lower
    case), `'`.

### Array Width

Arrays do not set their minimum, maximum, or default values. Their last required
parameter is the width, in bits, of the total array. This *should*, but is not
required to, be an even multiple of the element width. The parser will warn on
an uneven array width, but will not error.

Array parameters may use the `DEFAULT` decorator to provide a default value that
will be used for all members.

### Description

Following the default value may be an optional double-quote-delimited run of
text that serves as the human-readable description of the parameter.

Note that double-quote strings support escape sequences, including `\"`, for
embedding otherwise untypeable characters.

### Endianness

After the description may be an optional endianness token. This forces the
parameter to be interpreted as the given endianness, regardless of the
endianness of the owning `COMMAND`.

### Decorators

A `Parameter` element may be optionally followed by modifier items. These are
traditionally added, one per line, indented one step further than the
`Parameter` declaration. With few exceptions, whitespace is not significant.

- `DESCRIPTION`

    Takes a double-quoted string, and overrides the description set in the
    primary line.

- `DEFAULT_VALUE`

    Takes a value, and overrides the default value set in the primary line.

- `MAXIMUM_VALUE`

    Takes a value, and overrides the maximum value set in the primary line.

- `MINIMUM_VALUE`

    Takes a value, and overrides the minimum value set in the primary line.

- `META`

    Provides meta information about the parameter. This can be used to specify
    `STRING` encoding or `BLOCK` data type, for example. The first word after
    `META` is the attribute’s name, and all words after it *on the same line*
    are values. The `META` parsing halts at end of line.

- `POLY_WRITE_CONVERSION`
- `SEG_POLY_WRITE_CONVERSION`

    These two decorators define polynomial factors that are applied to field
    values between user entry and binary serialization. `SEG_` is a segmented
    version of `POLY_WRITE_CONVERSION`, and can store multiple different
    polynomials for parts of the domain. The segmented conversion takes a lower
    bound, and its upper bound is defined by the next segmented conversion whose
    lower bound is higher.

    ```cosmos
    POLY_WRITE_CONVERSION C0 C1 C2 … Cn
    SEG_POLY_WRITE_CONVERSION BOUND C0 C1 C2 … Cn
    ```

    All arguments to the conversion decorator must be parsable as numbers. The
    conversion parser reads to the end of the line, halting when it cannot parse
    a number. For a set of coefficients `C0 C1 C2 … Cn`, the resulting
    polynomial is <math><mi>y</mi><mo>=</mo><msub><mi>C</mi><mi>n</mi></msub>
    <mo>⋅</mo><msup><mi>x</mi><mi>n</mi></msup><mo>+</mo>…<mo>+</mo><msub>
    <mi>C</mi><mn>2</mn></msub><mo>⋅</mo><msup><mi>x</mi><mn>2</mn></msup>
    <mo>+</mo><msub><mi>C</mi><mn>1</mn></msub><mo>⋅</mo><mi>x</mi>
    <mo>+</mo><msub><mi>C</mi><mn>0</mn></msub></math>, where <math><mi>x</mi>
    </math> is the value of the parameter field in application memory, and
    <math><mi>y</mi></math> is the output value that will be written into the
    binary message.

- `OVERFLOW`

    Sets behavior when an integer (signed or unsigned) value is wider than its
    parameter width. Settings that do not raise an error, log a warning.

    - `ERROR` forces the application to raise an error and reject the parameter
        as invalid.
    - `ERROR_ALLOW_HEX` forces the application to raise an error on decimal
        literals but truncate (by dropping the high bits) hexadecimal literals.
        This permits, for example, `0xFF` to be written to an 8-bit `INT` slot.
    - `TRUNCATE` truncates (by dropping the high bits) the number to fit.
    - `SATURATE` clamps the field to the closest `INTn_MIN` or `INTn_MAX` value
        for the width and sign.

- `REQUIRED`

    This decorator takes no arguments. If applied to a parameter block, the
    parameter loses its default constructor and *must* be explicitly set by the
    client when building the owning command.

- `STATE`

    The `STATE` token defines a named value, which can take an optional hazard
    warning. `STATE` has its own documentation describing its grammar and use.

- `UNITS`

    Provides a unit of measurement to be displayed with the value. Useful for
    contextualizing raw numbers.
