# `TELEMETRY` Blocks

`TELEMETRY` blocks describe data messages emitted by nodes, generally not human
operated, to convey information to other nodes in the network. These can be
autonomous messages exchanged to keep a network informed of each others’ state,
as well as to inform human operators. These blocks represent one of COSMOS’s two
root-level elements, and are one of the four root-level text structures in
message definition files.

Each `TELEMETRY` block must have a block header carrying information about the
telemetry message itself, followed by one or more `Item` blocks that describe
the semantic and binary construction of the resulting message packet.
`TELEMETRY` blocks may also have zero or more decorator elements.

## `TELEMETRY` Grammar

<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" contentScriptType="application/ecmascript" contentStyleType="text/css" height="173px" preserveAspectRatio="xMidyMid" style="width:100%;height:auto;" version="1.1" viewBox="0 0 817 173" width="817px" zoomAndPan="magnify"><defs><filter height="300%" id="f14sbgylrpabmi" width="300%" x="-1" y="-1"><feGaussianBlur result="blurOut" stdDeviation="2.0"/><feColorMatrix in="blurOut" result="blurOut2" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .4 0"/><feOffset dx="4.0" dy="4.0" in="blurOut2" result="blurOut3"/><feBlend in="SourceGraphic" in2="blurOut3" mode="normal"/></filter></defs><g><ellipse cx="16" cy="85.5" fill="#000000" filter="url(#f14sbgylrpabmi)" rx="10" ry="10" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f14sbgylrpabmi)" height="67.7969" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="63" x="669.5" y="51.5"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="669.5" x2="732.5" y1="79.1094" y2="79.1094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="32" x="685" y="71.0332">Child</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="24" x="674.5" y="96.5664">Item</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="43" x="674.5" y="111.6602">Modifier</text><ellipse cx="796" cy="85.5" fill="none" filter="url(#f14sbgylrpabmi)" rx="10" ry="10" style="stroke: #000000; stroke-width: 1.0;"/><ellipse cx="796.5" cy="86" fill="#000000" rx="6" ry="6" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f14sbgylrpabmi)" height="154.6094" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="495" x="139.5" y="8"/><rect fill="#FFFFFF" height="121" rx="12.5" ry="12.5" style="stroke: #FFFFFF; stroke-width: 1.0;" width="489" x="142.5" y="38.6094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="139.5" x2="634.5" y1="35.6094" y2="35.6094"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="46" x="364" y="27.5332">Header</text><ellipse cx="160.5" cy="113.6094" fill="#000000" filter="url(#f14sbgylrpabmi)" rx="10" ry="10" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f14sbgylrpabmi)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="62" x="188.5" y="87.1094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="188.5" x2="250.5" y1="114.7188" y2="114.7188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="42" x="198.5" y="106.6426">Target</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="193.5" y="132.1758">word</text><rect fill="#FEFECE" filter="url(#f14sbgylrpabmi)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="56" x="268.5" y="87.1094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="268.5" x2="324.5" y1="114.7188" y2="114.7188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="36" x="278.5" y="106.6426">Name</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="27" x="273.5" y="132.1758">word</text><rect fill="#FEFECE" filter="url(#f14sbgylrpabmi)" height="67.7969" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="111" x="343" y="79.6094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="343" x2="454" y1="107.2188" y2="107.2188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="44" x="376.5" y="99.1426">Endian</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="72" x="348" y="124.6758">BIG_ENDIAN</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="91" x="348" y="139.7695">LITTLE_ENDIAN</text><ellipse cx="591.5" cy="113.6094" fill="none" filter="url(#f14sbgylrpabmi)" rx="10" ry="10" style="stroke: #000000; stroke-width: 1.0;"/><ellipse cx="592" cy="114.1094" fill="#000000" rx="6" ry="6" style="stroke: none; stroke-width: 1.0;"/><rect fill="#FEFECE" filter="url(#f14sbgylrpabmi)" height="52.7031" rx="12.5" ry="12.5" style="stroke: #A80036; stroke-width: 1.5;" width="91" x="472" y="87.1094"/><line style="stroke: #A80036; stroke-width: 1.5;" x1="472" x2="563" y1="114.7188" y2="114.7188"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="71" x="482" y="106.6426">Description</text><text fill="#000000" font-family="sans-serif" font-size="12" lengthAdjust="spacingAndGlyphs" textLength="32" x="477" y="132.1758">" text "</text><!--link *start*THeader to Target--><path d="M170.6406,113.6094 C174.756,113.6094 178.8713,113.6094 182.9867,113.6094 " fill="none" id="*start*THeader-Target" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="188.1995,113.6094,179.1995,109.6094,183.1995,113.6094,179.1995,117.6094,188.1995,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Target to Name--><path d="M250.781,113.6094 C254.883,113.6094 258.985,113.6094 263.087,113.6094 " fill="none" id="Target-Name" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="268.283,113.6094,259.283,109.6094,263.283,113.6094,259.283,117.6094,268.283,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Name to Endian--><path d="M324.789,113.6094 C329.135,113.6094 333.481,113.6094 337.827,113.6094 " fill="none" id="Name-Endian" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="342.929,113.6094,333.929,109.6094,337.929,113.6094,333.929,117.6094,342.929,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Endian to *end*THeader--><path d="M437.572,79.5774 C448.219,72.2218 460.224,65.4914 472.5,61.6094 C511.062,49.4153 528.778,40.8704 563.5,61.6094 C576.748,69.5224 584.096,86.1034 587.903,98.4259 " fill="none" id="Endian-*end*THeader" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="589.325,103.4086,590.7024,93.6565,587.9533,98.6004,583.0093,95.8513,589.325,103.4086" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Endian to Description--><path d="M454.281,113.6094 C458.333,113.6094 462.384,113.6094 466.436,113.6094 " fill="none" id="Endian-Description" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="471.568,113.6094,462.568,109.6094,466.568,113.6094,462.568,117.6094,471.568,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Description to *end*THeader--><path d="M563.172,113.6094 C567.52,113.6094 571.868,113.6094 576.216,113.6094 " fill="none" id="Description-*end*THeader" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="581.32,113.6094,572.32,109.6094,576.32,113.6094,572.32,117.6094,581.32,113.6094" style="stroke: #A80036; stroke-width: 1.0;"/><!--link *start to THeader--><path d="M26.3099,85.5 C44.7151,85.5 85.845,85.5 134.003,85.5 " fill="none" id="*start-THeader" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="139.294,85.5,130.294,81.5,134.294,85.5,130.294,89.5,139.294,85.5" style="stroke: #A80036; stroke-width: 1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacingAndGlyphs" textLength="77" x="44.25" y="78.9951">TELEMETRY</text><!--link THeader to Child--><path d="M634.766,85.5 C644.607,85.5 654.448,85.5 664.289,85.5 " fill="none" id="THeader-Child" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="669.435,85.5,660.435,81.5,664.435,85.5,660.435,89.5,669.435,85.5" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Child to Child--><path d="M737.833,72.9087 C753.806,71.8545 767.5,76.0516 767.5,85.5 C767.5,95.9258 750.826,99.9576 732.816,97.5955 " fill="none" id="Child-Child" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="732.816,73.4045,742.1656,76.5006,737.7918,72.9131,741.3793,68.5393,732.816,73.4045" style="stroke: #A80036; stroke-width: 1.0;"/><!--link Child to *end--><path d="M732.543,85.5 C748.429,85.5 764.315,85.5 780.202,85.5 " fill="none" id="Child-*end" style="stroke: #A80036; stroke-width: 1.0;"/><polygon fill="#A80036" points="785.589,85.5,776.589,81.5,780.589,85.5,776.589,89.5,785.589,85.5" style="stroke: #A80036; stroke-width: 1.0;"/></g></svg>

`TELEMETRY` uses the same header structure that `COMMAND` does, shown above in
UML form.

The first token encountered must be the text `TELEMETRY`. It is then followed by
two words separated by whitespace. These must be bare words; they will not be
interpreted as quoted strings and all non-whitespace characters that appear will
be treated literally. The first token names the target to which the packet
belongs, and the second names the packet itself.

Following `TELEMETRY` and the two names, one of two tokens must appear:
`BIG_ENDIAN` or `LITTLE_ENDIAN`. These denote the default endianness of all
`Item` feilds within the `TELEMETRY`.

If the next element after the endian token is a double-quoted string, it is
treated as the `TELEMETRY`’s description. If it is anything else, the header
stops and the parser moves on to attempting to parse child elements.

### Decorators

`TELEMETRY` blocks may have various decorations applied to them to add metadata
beyond that contained in the header.

Valid tokens here are:

- `SELECT_ITEM`

    This is valid to appear within a `TELEMETRY` but will generally be found on
    `SELECT_TELEMETRY`. It modifies an `Item` structure attached to this
    `Telemetry`. The `Item` element it targets need not appear above it in the
    source text, and need not even be present *in this block*.
    `SELECT_TELEMETRY` blocks may add new `Item` blocks which `SELECT_ITEM` can
    target.

    Abusing the permissiveness of non-locality is a good way to break things.

- `META`

    Stores metadata about the current telemetry. See the `META` documentation
    for more detailed information.

- `ALLOW_SHORT`

    Permits the application to process binary packets that match the identifying
    signature of this block but are insufficiently long. The application must
    zero-fill the bits that are not present in the packet received from the
    network when deserializing.

- `HIDDEN`

    This decorator hides the telemetry from the UI, but does not prevent its use
    in the application.

## Behavior

`Telemetry` collects `Item` elements into a list ordered by chronological
appearance in the source text. This list **cannot** be reordered, because `ITEM`
syntax can depend on previously-encountered `Item` elements.

`Telemetry` implements `Deref` to `[Item]`, and also provides `Index` by integer
and text keys that look up `Item`s by position or name, respectively. In
addition to the trait methods, the inherent methods `.items` and `item(&str)`
provide more explicit (and for `.item`, non-pancking) access to the `Item` set.

## Serialization

`Telemetry`’s implementation of `Display` is a normalized set of COSMOS text.
This can be written out to COSMOS files. Transformation to and from binary
messages is not implemented in this library.
