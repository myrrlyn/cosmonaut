# COSMOS Message Definition Language

The [COSMOS][cosmos] framework includes a declarative language used to create a
set of binary messages that all applications on a network can understand and
exchange. The language defines the binary layout and semantic meaning of the
message suite. It is intended to be used as a common source of truth for all
participants on the network.

This crate provides a parser for the language which produces a tree of Rust data
structures containing all of the described information, which clients can use to
understand the messages on a network.

The parser operates on fully-loaded texts, such as from a filesystem; it is not
currently capable of operating on streams such as `stdin` or a network socket.

## Notes on Terminology

Throughout this book, specific terms will be used to ensure as clear as possible
a delivery of the information and meaning.

When the words “may”, “must”, “should”, or “shall” appear in *italics*, they are
to be interpreted as per [RFC 2119][rfc_2119].

Tokens in the COSMOS grammar will be written in monospace font, fully
capitalized, like `THIS`. The parser is stricly case sensitive, and requires
that COSMOS tokens are capitalized.

When referring to COSMOS source text, the word “block” means a group of one or
more elements that combine to form a composite whole. The word “element” means a
single piece of text that corresponds to one structure in the parser API. A
block is composed of one parent and zero or more child elements, which may
themselves be blocks. The distinction is primarily used to distinguish between
the root element alone versus the composite structure at the root element.

Items from the crate API will be written in monospace font directly as their
name in the API, such as `TypeName` and `.method_name`. Functions will be
written with a leading `.` when they are methods on a type, and generally
without parentheses. The function signature will generally be elided, but may be
present as a list of types `(arguments) -> return`.

Some words will be found as COSMOS tokens in `MONOSPACE_ALL_CAPS`, API tokens in
`CamelCase` or `snake_case`, and as English words in standard text. Where these
refer to the same concept, the typesetting distinguishes between the COSMOS
text, the library type, and the abstract concept.

The parser is a library. The phrase “the application” will be used throughout
the book to denote behavior that is the responsibility of some arbitrary COSMOS
application using this parser.

## Diagrams

This book uses UML state diagrams to visualize the syntax of various elements of
the grammar. Keywords will appear in `SCREAMING_SNAKE_CASE`. They will attach to
the transition arrows when they do not need to name a state, and will be
enumerated inside a named state otherwise.

The named state blocks will contain token names (`word`, `int`, `value`),
keywords (`BIG_ENDIAN`, `FLOAT`), production rules (`" text "`, `' hex '`), or
generic names (`Modifier`) to denote valid items in this position.

[cosmos]: //cosmosrb.com
[rfc_2119]: https://tools.ietf.org/html/rfc2119
