# COSMOS Language

The COSMOS language is a plain-text description of binary message formats. It
provides the description of both construction and meaning of bitfields in a
message, the mechanisms of identifying messages from a stream, and the direction
of travel for a message to take (COSMOS is designed for a full-duplex, but not
necessarily symmetric, communications channel, akin to HTTP).

COSMOS message dictionaries are files that consist of one or more message
blocks. Each block declares a message’s name, bit endianness, and other
information, followed by one or more lines that define the components of the
message. The messages are built of bit sequences with specified type, values,
and width. Messages can index into their components from the front and back of
the packet, and can have unsized packets that are delimited by indexing from
either end, or by sentinel values.

## Detailed Specification

### Header

Each COSMOS block begins with a header line, described more fully
[here][header], which describe the block’s direction, endianness, namespace, and
purpose. The header is of the general form:

```text
TYPE TARGET NAME ENDIANNESS DESCRIPTION?
```

where `TYPE` is either `COMMAND` for control messages or `TELEMETRY` for data
messages, `TARGET` and `NAME` are two strings which define the scope of the
message’s application, `ENDIANNESS` details whether the message is laid out in
big- or little- endian byte- or bit- order, and an optional plain-text
description of what the block carries or means.

### Fields

After the header line, the block must have one or more lines indented once. The
COSMOS format is mostly whitespace insensitive, so the indent or dedent of lines
relative to their predecessor is determined only by whether the number of spaces
is greater than or less than the predecessor. A file with mixed tabs and spaces
counts tabs as four spaces wide by default, but this can be configured at the
command line or in a configuration file.

[header]: header
