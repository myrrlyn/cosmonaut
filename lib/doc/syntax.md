# Syntax Overview

The language is traditionally written with meaningful indentation, and does not
have scope-delimiting tokens. It is *largely* whitespace-insensitive, and can be
parsed in most cases without giving newlines or indentation state-specific
meaning. There are a few cases where newlines resolve ambiguity, and the
documentation highlights them.

This document describes the normative format of the source text; the parser in
this crate is more permissive than the format described here, but for ease of
reading and interoperability with the reference parser, the indentation and
newline format should be respected.

## Message Blocks

Each message begins with a header line at the left-most level of indentation.
This header begins with either the token `COMMAND` or the token `TELEMETRY`,
which denotes the semantic type of the message.

One or more lines must follow the header, which describe the fields within the
message or provide additional metadata. These lines should be indented one level
(the specific indentation schema is not relevant to this parser), and any
modifier lines on *them* should be indented once more.

## Field Blocks

The `COMMAND` and `TELEMETRY` blocks **must** contain at least one `PARAMETER`
or `ITEM` block (respectively). These blocks denote fields within the message,
and describe the position in the binary, the payload data type, and other
information about the field. A blank message is an error, and the parser will
raise a failure if it successfully parses a message header that is followed by
no valid field blocks before EOF or the next header.

## Modifier Elements

Message and field blocks both support modifier elements as children. The set of
elements supported varies per block type, and specific information is listed in
their API documentation and their chapters of this book.

## Structure

The language has a highly deterministic state sequence, and deviation from this
sequence results in an immediate error. The state sequence is denoted by
keywords and the tokens they expect to follow them.

Keywords are always fully capitalized. The parser is strictly case sensitive,
and performs no case unification among symbols.

## Sample

The following text describes two messages, each with one field where that field
has one special state. This is a minimal set of the grammar, meant only to show
the general layout. More complete samples can be found in the `assets/` folder
of the parser repository, in the specific chapters, or in the reference docs.

```cosmos
COMMAND TARGETNAME MESSAGENAME BIG_ENDIAN "Optional double-quoted description"
  PARAMETER FIELDNAME 0 8 UINT UINT8_MIN UINT8_MAX 42 "Optional description"
    STATE NAME VALUE HAZARDOUS "Optional reason"

TELEMETRY TARGETNAME MESSAGENAME LITTLE_ENDIAN "Optional description"
  ITEM FIELDNAME 0 32 INT INT32_MIN INT32_MAX 0 "Optional description"
    STATE NAME VALUE GREEN
```
