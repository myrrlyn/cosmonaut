# Language Overview

The message definition language is a plain-text syntax for the binary layout of
messages exchanged in a network. This language provides information about the
semantic meaning of messages, the types of data contained in them, and the
bit-level layout of fields in each message.

The COSMOS applicaiton framework is designed for use in a full-duplex and
asymmetric network, akin to the client/server distinction seen in HTTP.

The message dictionary for an application is built from a collection of files,
each of which contains one or more message blocks. Each block declares the
message’s meta information for use by the application, followed by declarations
of the data fields that make up the message. Each field has a data type, a width
in bits (COSMOS does not require that fields are full octets, or that they
respect octet boundaries), and an explicit or implicit starting offset in the
containing message.

The language is typically written with one element per line, with child elements
indented by one level. The grammar very rarely *requires* such distinction, but
it greatly aids human comprehension of the text.

## Structure

Message definitions consist of one of two message types, command or telemetry,
each of which contain one or more first-level field elements (parameter for
command, item for telemetry) that carry data. Elements may also have modifier
children to carry meta information and provide additional context for
applications interpreting the message.

In addition to the base command, telemetry, parameter, and item elements, the
grammar provides elements that can edit elements with additional or overriding
information.
