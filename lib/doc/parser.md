# Cosmonaut Parser

The parser engine builds up Cosmonaut message structures out of supplied input
text. It is not a streaming parser, and is not designed to accept an unbounded
supply of text. Rather, it is designed for use in a framework that will load a
collection of files, and pass fully-loaded text into the parser. It should not
be used in a pipeline program that accepts text on standard input and emits text
on standard output.

The primary parser API is provided by the `Dictionary` structure and its symbol
tree. A `Dictionary` instance holds the full parsed state for the COSMOS text in
an application, and so it should generally be used as a singleton. A client that
wishes to support multiple COSMOS applications in parallel may have multiple
`Dictionary` instances.

In addition to the `Dictionary` API, all of the parsing utilities and individual
types used to build up the parse tree are exposed for individual use by clients.

The basic types used by the parser are `nom`’s `CompleteStr` and the
`ParseResult` type, which is a partially-applied `nom::IResult` enum using
`CompleteStr` as the input type. All parsers in the crate consume `CompleteStr`
and emit `ParseResult`, and can be combined with each other or with other
`nom`-compatible parse tools as desired.

## Usage

Clients that only need to build up a parse tree, and have no interest in working
directly with the parse utilities, should instantiate a `Dictionary` and feed it
source files. Once parsing has begun, the `Dictionary` can be queried for the
structures it has built.

At present, the `Dictionary` can only accept UTF-8 encoded text slices, which
the client can build however they please. A recommended idiom is using
memory-mapped files that the client knows are in UTF-8, and building a slice out
of the map, or otherwise loading a file from disk into a UTF-8 `String`. Once
loaded, the `Dictionary.parse` method will build all the structures it can from
the text.

```rust
extern crate cosmonaut as cosmos;
use cosmos::Dictionary;

# fn main() {
let text = r#"
COMMAND TARGET_NAME MESSAGE_NAME BIG_ENDIAN "Sample command"
  PARAMETER HEADER 0 8 UINT 0 MAX_UINT8 42 "Sample parameter"
    STATE DEFAULT 42 HAZARDOUS "Example"
TELEMETRY TARGET_NAME MESSAGE_NAME BIG_ENDIAN "Sample telemetry"
  ITEM HEADER 0 8 UINT "Sample item"
    STATE DEFAULT 42 GREEN
"#;
let mut dict = Dictionary::new();
dict.parse(text).expect("This example can't fail");
# }
```

Since the sample text is well-formed, the parse succeeds and returns nothing. At
this point, `dict` is populated with one target, containg one command and one
telemetry. The populated dictionary can now be queried with safe accessors or
by a panicking-on-failure index.

We can query down the `Command` tree, through `Parameter` and `State`…

```rust
# extern crate cosmonaut as cosmos;
# use cosmos::Dictionary;
use cosmos::{
    Target,
    command::Command,
    parameter::Parameter,
    decorators::State,
};
# fn main() {
# let text = r#"
# COMMAND TARGET_NAME MESSAGE_NAME BIG_ENDIAN "Sample command"
#   PARAMETER HEADER 0 8 UINT 0 MAX_UINT8 42 "Sample parameter"
#     STATE DEFAULT 42 HAZARDOUS "Example"
# TELEMETRY TARGET_NAME MESSAGE_NAME BIG_ENDIAN "Sample telemetry"
#   ITEM HEADER 0 8 UINT "Sample item"
#     STATE DEFAULT 42 GREEN
# "#;
# let mut dict = Dictionary::new();
# dict.parse(text).expect("This example can't fail");

let tgt: &Target = &dict["TARGET_NAME"];
let cmd: &Command = &tgt.all_cmd()["MESSAGE_NAME"];
let param: &Parameter = &cmd["HEADER"];
let state: &State<Parameter> = &param["DEFAULT"];
assert!(state.has_extra());
# }
```

or through the `Telemetry` tree, through `Item` and `State`…

```rust
# extern crate cosmonaut as cosmos;
# use cosmos::Dictionary;
use cosmos::{
    Target,
    telemetry::Telemetry,
    item::Item,
    decorators::State,
    decorators::Color,
};
# fn main() {
# let text = r#"
# COMMAND TARGET_NAME MESSAGE_NAME BIG_ENDIAN "Sample command"
#   PARAMETER HEADER 0 8 UINT 0 MAX_UINT8 42 "Sample parameter"
#     STATE DEFAULT 42 HAZARDOUS "Example"
# TELEMETRY TARGET_NAME MESSAGE_NAME BIG_ENDIAN "Sample telemetry"
#   ITEM HEADER 0 8 UINT "Sample item"
#     STATE DEFAULT 42 GREEN
# "#;
# let mut dict = Dictionary::new();
# dict.parse(text).expect("This example can't fail");

let tgt: &Target = &dict["TARGET_NAME"];
let tlm: &Telemetry = &tgt.all_tlm()["MESSAGE_NAME"];
let item: &Item = &tlm["HEADER"];
let state: &State<Item> = &item["DEFAULT"];

if let Color::Green = *state.extra().unwrap() {}
else {
    panic!("TARGET_NAME/tlm/MESSAGE_NAME/HEADER/DEFAULT is definitely GREEN");
}
# }
```

The examples above show a limited set of the query behavior available on the
dictionary tree or on the individual elements.
